<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="ttl-epWrap">
    <div class="container">
      <h1 class="ttl-ep">お知らせ</h1>
    </div>
  </div>
  <div class="breadcrumb">
  <div class="breadcrumb-inner">
    <ul>
      <li><a href="/"><span class="icon-home"></span></a></li>
      <li>お知らせ</li>
    </ul>
  </div>
</div><!-- ./breadcrumb -->
  <div class="p-end p-news">
    <div class="container">
      <div class="p-news--cnt">
        <ul class="p-news--list end">
          <li class="p-news--item">
            <span class="date">2019.00.00</span>
            <span class="tag">カテゴリ</span>
            <a class="p-news--item-ttl link" href="/news/detail/">こちらに新着情報が入ります</a>
          </li>
          <li class="p-news--item">
            <span class="date">2019.00.00</span>
            <span class="tag">カテゴリ</span>
            <a class="p-news--item-ttl link" href="/news/detail/">こちらに新着情報が入ります</a>
          </li>
          <li class="p-news--item">
            <span class="date">2019.00.00</span>
            <span class="tag">カテゴリ</span>
            <a class="p-news--item-ttl link" href="/news/detail/">こちらに新着情報が入ります</a>
          </li>
          <li class="p-news--item">
            <span class="date">2019.00.00</span>
            <span class="tag">カテゴリ</span>
            <a class="p-news--item-ttl link" href="/news/detail/">こちらに新着情報が入ります</a>
          </li>
          <li class="p-news--item">
            <span class="date">2019.00.00</span>
            <span class="tag">カテゴリ</span>
            <a class="p-news--item-ttl link" href="/news/detail/">こちらに新着情報が入ります</a>
          </li>
          <li class="p-news--item">
            <span class="date">2019.00.00</span>
            <span class="tag">カテゴリ</span>
            <a class="p-news--item-ttl link" href="/news/detail/">こちらに新着情報が入ります</a>
          </li>
          <li class="p-news--item">
            <span class="date">2019.00.00</span>
            <span class="tag">カテゴリ</span>
            <a class="p-news--item-ttl link" href="/news/detail/">こちらに新着情報が入ります</a>
          </li>
          <li class="p-news--item">
            <span class="date">2019.00.00</span>
            <span class="tag">カテゴリ</span>
            <a class="p-news--item-ttl link" href="/news/detail/">こちらに新着情報が入ります</a>
          </li>
          <li class="p-news--item">
            <span class="date">2019.00.00</span>
            <span class="tag">カテゴリ</span>
            <a class="p-news--item-ttl link" href="/news/detail/">こちらに新着情報が入ります</a>
          </li>
        </ul>
        <div class="pagination">
          <div class="pagination-list">
            <a class="ctrl prev" href="">
              <svg xmlns="http://www.w3.org/2000/svg" width="6.121" height="9.414" viewBox="0 0 6.121 9.414">
                <g id="_1" data-name=" 1" transform="translate(1267.414 519.938) rotate(180)">
                  <path class="a" id="_1" data-name="&gt;" d="M814.332,1939.086l4,4-4,4" transform="translate(447.668 -1427.855)" fill="none" stroke="#3CAAE0" stroke-width="2" />
                </g>
              </svg>
            </a>
            <a href="">1</a>
            <a href="">2</a>
            <a class="active" href="">3</a>
            <a href="">4</a>
            <a href="">5</a>
            <a class="ctrl next" href="">
              <svg xmlns="http://www.w3.org/2000/svg" width="6.121" height="9.414" viewBox="0 0 6.121 9.414">
                <g id="シンボル_82" data-name="シンボル 82" transform="translate(-1259.293 -510.062)">
                  <path class="a" id="_" data-name="&gt;" d="M814.332,1939.086l4,4-4,4" transform="translate(445.668 -1428.316)" fill="none" stroke="#3CAAE0" stroke-width="2" />
                </g>
              </svg>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>