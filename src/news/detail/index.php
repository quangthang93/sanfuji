<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="ttl-epWrap">
    <div class="container">
      <h1 class="ttl-ep">お知らせ</h1>
    </div>
  </div>
  <div class="breadcrumb">
  <div class="breadcrumb-inner">
    <ul>
      <li><a href="/"><span class="icon-home"></span></a></li>
      <li><a href="/news">お知らせ</a></li>
      <li>第5回ライフプランセミナー神奈川「住宅購入前の準備とは？」</li>
    </ul>
  </div>
</div><!-- ./breadcrumb -->
  <div class="p-end p-news">
    <div class="container">
      <h2 class="p-news--ttl">第5回ライフプランセミナー神奈川「住宅購入前の準備とは？」</h2>
        <div class="dateWrap">
          <span class="date">2021.04.20</span>
          <span class="tag type3">イベント</span>
        </div>
        <div class="no-reset">
          <br>
          <p>これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。このダミーテキストは自由に改変することが出来ます。ダミーテキストはダミー文書やダミー文章とも呼ばれることがあります。これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。このダミーテキストは自由に改変することが出来ます。ダミーテキストはダミー文書やダミー文章とも呼ばれることがあります。これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。</p>

          <div class="col-2">
            <div>
              <h3>こちらに中見出しが入ります</h3>
              <p>これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。このダミーテキストは自由に改変することが出来ます。ダミーテキストはダミー文書やダミー文章とも呼ばれることがあります。これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。このダミーテキストは自由に改変することが出来ます。ダミーテキストはダミー文書やダミー文章とも呼ばれることがあります。これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。</p>
              <br>
              <a href="" class="link-icon arrow">テキストリンク</a>
              <br>
              <a href="" class="link-icon blank">外部リンク</a>
              <br>
              <a href="" class="link-icon pdf">PDFリンク</a>
            </div>
            <div style="padding-top: 20px;">
              <img src="<?php echo $PATH;?>/assets/images/news/news02.jpg" alt="">
            </div>
          </div>
          <br>
          <br>
          <br>
          <br>
          <p class="align-center"><img src="<?php echo $PATH;?>/assets/images/news/news01.png" alt=""></p>
        </div><!-- ./p-news-content -->
        <p class="view-moreWrap"><a href="/news" class="view-more">お知らせ一覧を見る</a></p>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>