<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="ttl-epWrap">
    <div class="container">
      <h1 class="ttl-ep">404 NOT FOUND</h1>
    </div>
  </div>
  <div class="breadcrumb">
  <div class="breadcrumb-inner">
    <ul>
      <li><a href="/"><span class="icon-home"></span></a></li>
      <li>404 NOT FOUND</li>
    </ul>
  </div>
</div><!-- ./breadcrumb -->
  <div class="p-end p-booking">
    <div class="container">
      <h2 class="title-lv2">お探しのページは見つかりませんでした。</h2>
      <p class="desc3">申し訳ございません。お客様がアクセスしようとしたページは、 掲載期間が終了し削除されたか、別の場所に移動した可能性があります。 <br>メニューから引き続き当社ホームページをご覧くださいませ。</p>
      <div class="align-center mgt-80">
        <a href="/" class="link2">トップページ</a>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>