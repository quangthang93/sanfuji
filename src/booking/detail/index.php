<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="ttl-epWrap">
    <div class="container">
      <h1 class="ttl-ep">モデルハウス見学予約</h1>
    </div>
  </div>
  <div class="breadcrumb">
  <div class="breadcrumb-inner">
    <ul>
      <li><a href="/"><span class="icon-home"></span></a></li>
      <li>モデルハウス見学予約</li>
    </ul>
  </div>
</div><!-- ./breadcrumb -->
  <div class="p-end p-booking">
    <div class="container">
      <h2 class="title-lv2">見学するモデルハウスを選ぶ</h2>
      <p class="desc2">モデルハウス見学をご希望の方はご見学希望日時をご入力ください。ご予約は本日より３日後以降からの受付となります。 ご希望日時を調整の上お申し込みのモデルハウスよりご連絡をさせていただきます。</p>
      <div class="p-choice">
        <h3 class="title-lv3">見学をご希望のモデルハウスを3棟までお選びください。</h3>
        <div class="p-choice--head">
          <div class="p-choice--head-ttl">モデルハウス一覧:&nbsp;&nbsp;<a href="" class="link2 type2">Tvkハウジングプラザ新百合ヶ丘</a></div>
          <div class="p-choice--head-btn">
            <a href="" class="btn-change">来場する展示場の変更</a>
          </div>
        </div><!-- ./p-choice--head -->
        <ul class="p-choice--list">
          <li class="p-choice--item js-itemChoice">
            <div class="p-choice--item-infor">
              <div class="p-choice--item-infor-thumb">
                <img src="<?php echo $PATH;?>/assets/images/booking/booking01.png" alt="">
              </div>
              <div class="p-choice--item-infor-cnt">
                <p class="p-choice--item-infor-cat">三井ホーム</p>
                <h4 class="p-choice--item-infor-ttl">LANGLEY</h4>
                <p class="p-choice--item-infor-des">#システムキッチンがある #安心のセキュリティ #二重窓 #防音・断熱・通気性 #IoT対応 #アイランドキッチン #木のぬくもり</p>
              </div>
            </div>
            <div class="p-choice--item-form">
              <form action="">
                <div class="p-choice--item-form-fields">
                  <div class="p-event--search-fields-item type2 calendar"><span><input readonly="readonly" class="js-datepicker01" type="text" value="見学希望日を選択"></span></div>
                  <div class="p-choice--item-form-timeWrap">
                    <div class="p-choice--item-form-time">
                      <div class="p-choice--item-form-input">
                        <span>
                          <input readonly="readonly" class="js-hour" type="number" value="10" min="0" max="17" step="1">
                          <span class="btn-plus js-btnHour"></span>
                        </span>        
                      </div>
                      <div class="p-choice--item-form-label">時</div>
                    </div>
                    <div class="p-choice--item-form-time">
                      <div class="p-choice--item-form-input">
                        <span>
                          <input readonly="readonly" class="js-min" type="number" value="00" min="0" max="59" step="1">
                          <span class="btn-plus js-btnMin"></span>
                        </span>        
                      </div>
                      <div class="p-choice--item-form-label">分</div>
                    </div>
                  </div>
                </div>
                <p class="p-choice--item-form-alert">同じ日時が選択されています</p>
              </form>
              <div class="p-choice--item-btn">
                <a class="btn-add js-btnAddItem" href="javascript:void(0)">追加</a>
              </div>
            </div>
          </li>
          <li class="p-choice--item js-itemChoice">
            <div class="p-choice--item-infor">
              <div class="p-choice--item-infor-thumb">
                <img src="<?php echo $PATH;?>/assets/images/booking/booking02.png" alt="">
              </div>
              <div class="p-choice--item-infor-cnt">
                <p class="p-choice--item-infor-cat">積水ハウス(シャーウッド)</p>
                <h4 class="p-choice--item-infor-ttl">ザ・グラヴィス</h4>
                <p class="p-choice--item-infor-des">#システムキッチンがある #安心のセキュリティ #二重窓 #防音・断熱・通気性 #アイランドキッチン</p>
              </div>
            </div>
            <div class="p-choice--item-form">
              <form action="">
                <div class="p-choice--item-form-fields">
                  <div class="p-event--search-fields-item type2 calendar"><span><input readonly="readonly" class="js-datepicker01" type="text" value="見学希望日を選択"></span></div>
                  <div class="p-choice--item-form-timeWrap">
                    <div class="p-choice--item-form-time">
                      <div class="p-choice--item-form-input">
                        <span>
                          <input readonly="readonly" class="js-hour" type="number" value="10" min="0" max="17" step="1">
                          <span class="btn-plus js-btnHour"></span>
                        </span>        
                      </div>
                      <div class="p-choice--item-form-label">時</div>
                    </div>
                    <div class="p-choice--item-form-time">
                      <div class="p-choice--item-form-input">
                        <span>
                          <input readonly="readonly" class="js-min" type="number" value="00" min="0" max="59" step="1">
                          <span class="btn-plus js-btnMin"></span>
                        </span>        
                      </div>
                      <div class="p-choice--item-form-label">分</div>
                    </div>
                  </div>
                </div>
              </form>
              <div class="p-choice--item-btn">
                <a class="btn-add js-btnAddItem" href="javascript:void(0)">追加</a>
              </div>
            </div>
          </li>
          <li class="p-choice--item js-itemChoice">
            <div class="p-choice--item-infor">
              <div class="p-choice--item-infor-thumb">
                <img src="<?php echo $PATH;?>/assets/images/booking/booking03.png" alt="">
              </div>
              <div class="p-choice--item-infor-cnt">
                <p class="p-choice--item-infor-cat">住友林業</p>
                <h4 class="p-choice--item-infor-ttl">PROUDIO</h4>
                <p class="p-choice--item-infor-des">#システムキッチンがある #安心のセキュリティ #二重窓 #防音・断熱・通気性 #IoT対応 #アイランドキッチン #木のぬくもり</p>
              </div>
            </div>
            <div class="p-choice--item-form">
              <form action="">
                <div class="p-choice--item-form-fields">
                  <div class="p-event--search-fields-item type2 calendar"><span><input readonly="readonly" class="js-datepicker01" type="text" value="見学希望日を選択"></span></div>
                  <div class="p-choice--item-form-timeWrap">
                    <div class="p-choice--item-form-time">
                      <div class="p-choice--item-form-input">
                        <span>
                          <input readonly="readonly" class="js-hour" type="number" value="10" min="0" max="17" step="1">
                          <span class="btn-plus js-btnHour"></span>
                        </span>        
                      </div>
                      <div class="p-choice--item-form-label">時</div>
                    </div>
                    <div class="p-choice--item-form-time">
                      <div class="p-choice--item-form-input">
                        <span>
                          <input readonly="readonly" class="js-min" type="number" value="00" min="0" max="59" step="1">
                          <span class="btn-plus js-btnMin"></span>
                        </span>        
                      </div>
                      <div class="p-choice--item-form-label">分</div>
                    </div>
                  </div>
                </div>
              </form>
              <div class="p-choice--item-btn">
                <a class="btn-add js-btnAddItem" href="javascript:void(0)">追加</a>
              </div>
            </div>
          </li>
          <li class="p-choice--item js-itemChoice">
            <div class="p-choice--item-infor">
              <div class="p-choice--item-infor-thumb">
                <img src="<?php echo $PATH;?>/assets/images/booking/booking04.png" alt="">
              </div>
              <div class="p-choice--item-infor-cnt">
                <p class="p-choice--item-infor-cat">ダイワハウス</p>
                <h4 class="p-choice--item-infor-ttl">xevoΣ PREMIUM</h4>
                <p class="p-choice--item-infor-des">#システムキッチンがある #安心のセキュリティ #二重窓 #防音・断熱・通気性 #IoT対応 #アイランドキッチン #木のぬくもり</p>
              </div>
            </div>
            <div class="p-choice--item-form">
              <form action="">
                <div class="p-choice--item-form-fields">
                  <div class="p-event--search-fields-item type2 calendar"><span><input readonly="readonly" class="js-datepicker01" type="text" value="見学希望日を選択"></span></div>
                  <div class="p-choice--item-form-timeWrap">
                    <div class="p-choice--item-form-time">
                      <div class="p-choice--item-form-input">
                        <span>
                          <input readonly="readonly" class="js-hour" type="number" value="10" min="0" max="17" step="1">
                          <span class="btn-plus js-btnHour"></span>
                        </span>        
                      </div>
                      <div class="p-choice--item-form-label">時</div>
                    </div>
                    <div class="p-choice--item-form-time">
                      <div class="p-choice--item-form-input">
                        <span>
                          <input readonly="readonly" class="js-min" type="number" value="00" min="0" max="59" step="1">
                          <span class="btn-plus js-btnMin"></span>
                        </span>        
                      </div>
                      <div class="p-choice--item-form-label">分</div>
                    </div>
                  </div>
                </div>
              </form>
              <div class="p-choice--item-btn">
                <a class="btn-add js-btnAddItem" href="javascript:void(0)">追加</a>
              </div>
            </div>
          </li>
          <li class="p-choice--item js-itemChoice">
            <div class="p-choice--item-infor">
              <div class="p-choice--item-infor-thumb">
                <img src="<?php echo $PATH;?>/assets/images/booking/booking01.png" alt="">
              </div>
              <div class="p-choice--item-infor-cnt">
                <p class="p-choice--item-infor-cat">三井ホーム</p>
                <h4 class="p-choice--item-infor-ttl">LANGLEY</h4>
                <p class="p-choice--item-infor-des">#システムキッチンがある #安心のセキュリティ #二重窓 #防音・断熱・通気性 #IoT対応 #アイランドキッチン #木のぬくもり</p>
              </div>
            </div>
            <div class="p-choice--item-form">
              <form action="">
                <div class="p-choice--item-form-fields">
                  <div class="p-event--search-fields-item type2 calendar"><span><input readonly="readonly" class="js-datepicker01" type="text" value="見学希望日を選択"></span></div>
                  <div class="p-choice--item-form-timeWrap">
                    <div class="p-choice--item-form-time">
                      <div class="p-choice--item-form-input">
                        <span>
                          <input readonly="readonly" class="js-hour" type="number" value="10" min="0" max="17" step="1">
                          <span class="btn-plus js-btnHour"></span>
                        </span>        
                      </div>
                      <div class="p-choice--item-form-label">時</div>
                    </div>
                    <div class="p-choice--item-form-time">
                      <div class="p-choice--item-form-input">
                        <span>
                          <input readonly="readonly" class="js-min" type="number" value="00" min="0" max="59" step="1">
                          <span class="btn-plus js-btnMin"></span>
                        </span>        
                      </div>
                      <div class="p-choice--item-form-label">分</div>
                    </div>
                  </div>
                </div>
              </form>
              <div class="p-choice--item-btn">
                <a class="btn-add js-btnAddItem" href="javascript:void(0)">追加</a>
              </div>
            </div>
          </li>
          <li class="p-choice--item js-itemChoice">
            <div class="p-choice--item-infor">
              <div class="p-choice--item-infor-thumb">
                <img src="<?php echo $PATH;?>/assets/images/booking/booking02.png" alt="">
              </div>
              <div class="p-choice--item-infor-cnt">
                <p class="p-choice--item-infor-cat">積水ハウス(シャーウッド)</p>
                <h4 class="p-choice--item-infor-ttl">ザ・グラヴィス</h4>
                <p class="p-choice--item-infor-des">#システムキッチンがある #安心のセキュリティ #二重窓 #防音・断熱・通気性 #アイランドキッチン</p>
              </div>
            </div>
            <div class="p-choice--item-form">
              <form action="">
                <div class="p-choice--item-form-fields">
                  <div class="p-event--search-fields-item type2 calendar"><span><input readonly="readonly" class="js-datepicker01" type="text" value="見学希望日を選択"></span></div>
                  <div class="p-choice--item-form-timeWrap">
                    <div class="p-choice--item-form-time">
                      <div class="p-choice--item-form-input">
                        <span>
                          <input readonly="readonly" class="js-hour" type="number" value="10" min="0" max="17" step="1">
                          <span class="btn-plus js-btnHour"></span>
                        </span>        
                      </div>
                      <div class="p-choice--item-form-label">時</div>
                    </div>
                    <div class="p-choice--item-form-time">
                      <div class="p-choice--item-form-input">
                        <span>
                          <input readonly="readonly" class="js-min" type="number" value="00" min="0" max="59" step="1">
                          <span class="btn-plus js-btnMin"></span>
                        </span>        
                      </div>
                      <div class="p-choice--item-form-label">分</div>
                    </div>
                  </div>
                </div>
              </form>
              <div class="p-choice--item-btn">
                <a class="btn-add js-btnAddItem" href="javascript:void(0)">追加</a>
              </div>
            </div>
          </li>
          <li class="p-choice--item js-itemChoice">
            <div class="p-choice--item-infor">
              <div class="p-choice--item-infor-thumb">
                <img src="<?php echo $PATH;?>/assets/images/booking/booking03.png" alt="">
              </div>
              <div class="p-choice--item-infor-cnt">
                <p class="p-choice--item-infor-cat">住友林業</p>
                <h4 class="p-choice--item-infor-ttl">PROUDIO</h4>
                <p class="p-choice--item-infor-des">#システムキッチンがある #安心のセキュリティ #二重窓 #防音・断熱・通気性 #IoT対応 #アイランドキッチン #木のぬくもり</p>
              </div>
            </div>
            <div class="p-choice--item-form">
              <form action="">
                <div class="p-choice--item-form-fields">
                  <div class="p-event--search-fields-item type2 calendar"><span><input readonly="readonly" class="js-datepicker01" type="text" value="見学希望日を選択"></span></div>
                  <div class="p-choice--item-form-timeWrap">
                    <div class="p-choice--item-form-time">
                      <div class="p-choice--item-form-input">
                        <span>
                          <input readonly="readonly" class="js-hour" type="number" value="10" min="0" max="17" step="1">
                          <span class="btn-plus js-btnHour"></span>
                        </span>        
                      </div>
                      <div class="p-choice--item-form-label">時</div>
                    </div>
                    <div class="p-choice--item-form-time">
                      <div class="p-choice--item-form-input">
                        <span>
                          <input readonly="readonly" class="js-min" type="number" value="00" min="0" max="59" step="1">
                          <span class="btn-plus js-btnMin"></span>
                        </span>        
                      </div>
                      <div class="p-choice--item-form-label">分</div>
                    </div>
                  </div>
                </div>
              </form>
              <div class="p-choice--item-btn">
                <a class="btn-add js-btnAddItem" href="javascript:void(0)">追加</a>
              </div>
            </div>
          </li>
          <li class="p-choice--item js-itemChoice">
            <div class="p-choice--item-infor">
              <div class="p-choice--item-infor-thumb">
                <img src="<?php echo $PATH;?>/assets/images/booking/booking04.png" alt="">
              </div>
              <div class="p-choice--item-infor-cnt">
                <p class="p-choice--item-infor-cat">ダイワハウス</p>
                <h4 class="p-choice--item-infor-ttl">xevoΣ PREMIUM</h4>
                <p class="p-choice--item-infor-des">#システムキッチンがある #安心のセキュリティ #二重窓 #防音・断熱・通気性 #IoT対応 #アイランドキッチン #木のぬくもり</p>
              </div>
            </div>
            <div class="p-choice--item-form">
              <form action="">
                <div class="p-choice--item-form-fields">
                  <div class="p-event--search-fields-item type2 calendar"><span><input readonly="readonly" class="js-datepicker01" type="text" value="見学希望日を選択"></span></div>
                  <div class="p-choice--item-form-timeWrap">
                    <div class="p-choice--item-form-time">
                      <div class="p-choice--item-form-input">
                        <span>
                          <input readonly="readonly" class="js-hour" type="number" value="10" min="0" max="17" step="1">
                          <span class="btn-plus js-btnHour"></span>
                        </span>        
                      </div>
                      <div class="p-choice--item-form-label">時</div>
                    </div>
                    <div class="p-choice--item-form-time">
                      <div class="p-choice--item-form-input">
                        <span>
                          <input readonly="readonly" class="js-min" type="number" value="00" min="0" max="59" step="1">
                          <span class="btn-plus js-btnMin"></span>
                        </span>        
                      </div>
                      <div class="p-choice--item-form-label">分</div>
                    </div>
                  </div>
                </div>
              </form>
              <div class="p-choice--item-btn">
                <a class="btn-add js-btnAddItem" href="javascript:void(0)">追加</a>
              </div>
            </div>
          </li>
        </ul><!-- ./p-choice--list -->

        <div class="p-choice--direct">
          <a href="/booking/form/" class="btn-blue">見学予約フォーム</a>
        </div>

      </div><!-- ./p-choice -->
    </div>
  </div>
  <div class="p-exhibition--sectionTel">
        <div class="p-exhibition--sectionTel-inner">
          <h3 class="p-exhibition--sectionTel-ttl">お電話でも来店予約を受け付けております！</h3>
          <div class="p-exhibition--sectionTel-numberWrap">
            <a href="tel:045-716-8466" class="p-exhibition--sectionTel-btn sp-only">お電話での受付はこちら</a>
            <a class="p-exhibition--sectionTel-number" href="tel:045-716-8466">045-716-8466</a>
            <p class="desc2 pc-only">[受付時間] 11:00-16:00</p>
          </div>
          <p class="desc2 txt-bold">※電話申し込みは、お電話で直接内容等を確認させていただいた時点で完了いたします。</p>
          <p class="desc2">※担当者が不在の場合もございます。留守番電話での対応はいたしておりませんのであらかじめご了承ください。</p>
        </div>
      </div><!-- ./p-exhibition--tel -->
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>