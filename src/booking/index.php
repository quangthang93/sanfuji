<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="ttl-epWrap">
    <div class="container">
      <h1 class="ttl-ep">モデルハウス見学予約</h1>
    </div>
  </div>
  <div class="breadcrumb">
  <div class="breadcrumb-inner">
    <ul>
      <li><a href="/"><span class="icon-home"></span></a></li>
      <li>モデルハウス見学予約</li>
    </ul>
  </div>
</div><!-- ./breadcrumb -->
  <div class="p-end p-booking">
    <div class="container">
      <h2 class="title-lv2">来場する展示場を選ぶ</h2>
      <p class="desc2">モデルハウス見学をご希望の方はご来場いただく展示場をご選択後、予約フォームまでご入力ください。ご希望日時を調整の上お申し込みのモデルハウスよりご連絡をさせていただきます。</p>
      <div class="p-booking--choice">
        <p class="title-lv4">エリアを選択して絞り込み</p>
        <div class="p-booking--choice-list">
          <ul>
            <li><a class="js-choice active" href="javascript:void(0)"><span>横浜北部・川崎エリア</span></a></li>
            <li><a class="js-choice" href="javascript:void(0)"><span>横浜南部エリア</span></a></li>
            <li><a class="js-choice" href="javascript:void(0)"><span>湘南・西湘エリア</span></a></li>
            <li><a class="js-choice" href="javascript:void(0)"><span>相模原・県央エリア</span></a></li>
          </ul>
        </div>
      </div><!-- ./p-booking--choice -->
      <div class="p-booking--results">
        <h3 class="title-lv3">来場をご希望の展示場をお選びください。</h3>
        <p class="p-booking--results-counter">該当の展示場: <span class="num">85</span><span class="unit">件</span></p>
        <ul class="p-booking--results-list">
          <li class="p-booking--results-item">
            <a class="link" href="/booking/detail">
              <div class="p-booking--results-thumb">
                <img src="<?php echo $PATH;?>/assets/images/booking/booking01.png" alt="">
              </div>
              <div class="p-booking--results-cnt">
                <h4 class="p-booking--results-cnt-ttl">Tvkハウジングプラザ新百合ヶ丘</h4>
                <p class="p-booking--results-cnt-address">神奈川県川崎市麻生区万福寺4-3-1</p>
                <p class="p-booking--results-cnt-count">展示棟数: 全4棟</p>
                <div class="p-booking--results-view">
                  <a href="" class="btn-view">この展示場で探す</a>
                </div>
              </div>
            </a>
          </li>
          <li class="p-booking--results-item">
            <a class="link" href="/booking/detail">
              <div class="p-booking--results-thumb">
                <img src="<?php echo $PATH;?>/assets/images/booking/booking02.png" alt="">
              </div>
              <div class="p-booking--results-cnt">
                <h4 class="p-booking--results-cnt-ttl">Tvkハウジングプラザ新百合ヶ丘</h4>
                <p class="p-booking--results-cnt-address">神奈川県川崎市麻生区万福寺4-3-1</p>
                <p class="p-booking--results-cnt-count">展示棟数: 全4棟</p>
                <div class="p-booking--results-view">
                  <a href="" class="btn-view">この展示場で探す</a>
                </div>
              </div>
            </a>
          </li>
          <li class="p-booking--results-item">
            <a class="link" href="/booking/detail">
              <div class="p-booking--results-thumb">
                <img src="<?php echo $PATH;?>/assets/images/booking/booking03.png" alt="">
              </div>
              <div class="p-booking--results-cnt">
                <h4 class="p-booking--results-cnt-ttl">Tvkハウジングプラザ新百合ヶ丘</h4>
                <p class="p-booking--results-cnt-address">神奈川県川崎市麻生区万福寺4-3-1</p>
                <p class="p-booking--results-cnt-count">展示棟数: 全4棟</p>
                <div class="p-booking--results-view">
                  <a href="" class="btn-view">この展示場で探す</a>
                </div>
              </div>
            </a>
          </li>
          <li class="p-booking--results-item">
            <a class="link" href="/booking/detail">
              <div class="p-booking--results-thumb">
                <img src="<?php echo $PATH;?>/assets/images/booking/booking04.png" alt="">
              </div>
              <div class="p-booking--results-cnt">
                <h4 class="p-booking--results-cnt-ttl">Tvkハウジングプラザ新百合ヶ丘</h4>
                <p class="p-booking--results-cnt-address">神奈川県川崎市麻生区万福寺4-3-1</p>
                <p class="p-booking--results-cnt-count">展示棟数: 全4棟</p>
                <div class="p-booking--results-view">
                  <a href="" class="btn-view">この展示場で探す</a>
                </div>
              </div>
            </a>
          </li>
          <li class="p-booking--results-item">
            <a class="link" href="/booking/detail">
              <div class="p-booking--results-thumb">
                <img src="<?php echo $PATH;?>/assets/images/booking/booking05.png" alt="">
              </div>
              <div class="p-booking--results-cnt">
                <h4 class="p-booking--results-cnt-ttl">Tvkハウジングプラザ新百合ヶ丘</h4>
                <p class="p-booking--results-cnt-address">神奈川県川崎市麻生区万福寺4-3-1</p>
                <p class="p-booking--results-cnt-count">展示棟数: 全4棟</p>
                <div class="p-booking--results-view">
                  <a href="" class="btn-view">この展示場で探す</a>
                </div>
              </div>
            </a>
          </li>
          <li class="p-booking--results-item">
            <a class="link" href="/booking/detail">
              <div class="p-booking--results-thumb">
                <img src="<?php echo $PATH;?>/assets/images/booking/booking06.png" alt="">
              </div>
              <div class="p-booking--results-cnt">
                <h4 class="p-booking--results-cnt-ttl">Tvkハウジングプラザ新百合ヶ丘</h4>
                <p class="p-booking--results-cnt-address">神奈川県川崎市麻生区万福寺4-3-1</p>
                <p class="p-booking--results-cnt-count">展示棟数: 全4棟</p>
                <div class="p-booking--results-view">
                  <a href="" class="btn-view">この展示場で探す</a>
                </div>
              </div>
            </a>
          </li>
          <li class="p-booking--results-item">
            <a class="link" href="/booking/detail">
              <div class="p-booking--results-thumb">
                <img src="<?php echo $PATH;?>/assets/images/booking/booking01.png" alt="">
              </div>
              <div class="p-booking--results-cnt">
                <h4 class="p-booking--results-cnt-ttl">Tvkハウジングプラザ新百合ヶ丘</h4>
                <p class="p-booking--results-cnt-address">神奈川県川崎市麻生区万福寺4-3-1</p>
                <p class="p-booking--results-cnt-count">展示棟数: 全4棟</p>
                <div class="p-booking--results-view">
                  <a href="" class="btn-view">この展示場で探す</a>
                </div>
              </div>
            </a>
          </li>
          <li class="p-booking--results-item">
            <a class="link" href="/booking/detail">
              <div class="p-booking--results-thumb">
                <img src="<?php echo $PATH;?>/assets/images/booking/booking02.png" alt="">
              </div>
              <div class="p-booking--results-cnt">
                <h4 class="p-booking--results-cnt-ttl">Tvkハウジングプラザ新百合ヶ丘</h4>
                <p class="p-booking--results-cnt-address">神奈川県川崎市麻生区万福寺4-3-1</p>
                <p class="p-booking--results-cnt-count">展示棟数: 全4棟</p>
                <div class="p-booking--results-view">
                  <a href="" class="btn-view">この展示場で探す</a>
                </div>
              </div>
            </a>
          </li>
          <li class="p-booking--results-item">
            <a class="link" href="/booking/detail">
              <div class="p-booking--results-thumb">
                <img src="<?php echo $PATH;?>/assets/images/booking/booking03.png" alt="">
              </div>
              <div class="p-booking--results-cnt">
                <h4 class="p-booking--results-cnt-ttl">Tvkハウジングプラザ新百合ヶ丘</h4>
                <p class="p-booking--results-cnt-address">神奈川県川崎市麻生区万福寺4-3-1</p>
                <p class="p-booking--results-cnt-count">展示棟数: 全4棟</p>
                <div class="p-booking--results-view">
                  <a href="" class="btn-view">この展示場で探す</a>
                </div>
              </div>
            </a>
          </li>
          <li class="p-booking--results-item">
            <a class="link" href="/booking/detail">
              <div class="p-booking--results-thumb">
                <img src="<?php echo $PATH;?>/assets/images/booking/booking04.png" alt="">
              </div>
              <div class="p-booking--results-cnt">
                <h4 class="p-booking--results-cnt-ttl">Tvkハウジングプラザ新百合ヶ丘</h4>
                <p class="p-booking--results-cnt-address">神奈川県川崎市麻生区万福寺4-3-1</p>
                <p class="p-booking--results-cnt-count">展示棟数: 全4棟</p>
                <div class="p-booking--results-view">
                  <a href="" class="btn-view">この展示場で探す</a>
                </div>
              </div>
            </a>
          </li>
          <li class="p-booking--results-item">
            <a class="link" href="/booking/detail">
              <div class="p-booking--results-thumb">
                <img src="<?php echo $PATH;?>/assets/images/booking/booking05.png" alt="">
              </div>
              <div class="p-booking--results-cnt">
                <h4 class="p-booking--results-cnt-ttl">Tvkハウジングプラザ新百合ヶ丘</h4>
                <p class="p-booking--results-cnt-address">神奈川県川崎市麻生区万福寺4-3-1</p>
                <p class="p-booking--results-cnt-count">展示棟数: 全4棟</p>
                <div class="p-booking--results-view">
                  <a href="" class="btn-view">この展示場で探す</a>
                </div>
              </div>
            </a>
          </li>
          <li class="p-booking--results-item">
            <a class="link" href="/booking/detail">
              <div class="p-booking--results-thumb">
                <img src="<?php echo $PATH;?>/assets/images/booking/booking06.png" alt="">
              </div>
              <div class="p-booking--results-cnt">
                <h4 class="p-booking--results-cnt-ttl">Tvkハウジングプラザ新百合ヶ丘</h4>
                <p class="p-booking--results-cnt-address">神奈川県川崎市麻生区万福寺4-3-1</p>
                <p class="p-booking--results-cnt-count">展示棟数: 全4棟</p>
                <div class="p-booking--results-view">
                  <a href="" class="btn-view">この展示場で探す</a>
                </div>
              </div>
            </a>
          </li>
        </ul><!-- ./p-booking--results-list -->
        <div class="p-booking--results-viewmore">
          <a class="btn-view2" href="">もっと見る</a>
        </div>
      </div><!-- ./p-booking--results -->
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>