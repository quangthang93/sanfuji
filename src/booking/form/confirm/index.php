<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="ttl-epWrap">
    <div class="container">
      <h1 class="ttl-ep">モデルハウス見学予約</h1>
    </div>
  </div>
  <div class="breadcrumb">
  <div class="breadcrumb-inner">
    <ul>
      <li><a href="/"><span class="icon-home"></span></a></li>
      <li>モデルハウス見学予約</li>
    </ul>
  </div>
</div><!-- ./breadcrumb -->
  <div class="p-end p-booking">
    <div class="container">
      <h2 class="title-lv2">予約内容のご入力</h2>
      <div class="p-form">
        <h4 class="title-lv4">注意事項</h4>
        <ul class="p-form--list">
          <li>お申し込みフォームは、お申し込みの代行サービスであり、ご予約の完了ではありません。</li>
          <li>1回のお申し込みでモデルハウス3棟までお申し込みいただけます。</li>
          <li>ご予約にあたり、ご見学希望日時をご入力ください。ご予約は本日より3日後以降からの受付となります。</li>
          <li>ご希望日時を調整の上お申し込みのモデルハウスよりご連絡をさせていただきます。また、内容によりご希望に沿えない場合がございます。予めご了承ください。</li>
          <li>お申し込み時のご連絡先に誤記や虚偽がある場合は申し込みを無効とさせていただきます。</li>
          <li>特典はご見学された日時に提供されているサービスとなります。期間中1家族1回とさせていただきます。予めご了承ください。</li>
        </ul>
      </div><!-- ./p-form -->
      <div class="contact-content">
        <div class="v-contact">
          <div class="u-layout-smaller type2">
            <div class="c-steps">
              <div class="c-steps__col">
                <span class="c-steps__col__number u-font-rajdhani">1</span>
                <p></p>
                <p class="c-steps__col__label">予約内容のご入力</p>
                <p></p>
              </div>
              <div class="c-steps__col is-active">
                <span class="c-steps__col__number u-font-rajdhani">2</span>
                <p></p>
                <p class="c-steps__col__label">予約内容のご確認</p>
                <p></p>
              </div>
              <div class="c-steps__col">
                <span class="c-steps__col__number u-font-rajdhani">3</span>
                <p></p>
                <p class="c-steps__col__label">お申し込み完了</p>
                <p></p>
              </div>
            </div>

            <p class="title-lv3 mgb-35">注意事項をご確認のうえお客様情報をご入力ください。</p>
            <div id="mw_wp_form_mw-wp-form-215" class="mw_wp_form mw_wp_form_input">
              <form method="post" action="" enctype="multipart/form-data">
                <div class="c-form">
                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">来場希望の展示場</span>
                    </label>
                    <div class="c-form__row__field">
                      Tvkハウジングプラザ新百合ヶ丘 <input type="hidden" name="fullname" value="Tvkハウジングプラザ新百合ヶ丘">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">見学希望のモデルハウス</span>
                    </label>
                    <div class="c-form__row__field">
                      <ul class="p-form--list type2">
                        <li>LANGLEY - 三井ホーム<span class="verticalLine"></span>2021年2月27日(土) 10時30分</li>
                        <li>ザ・グラヴィス - 積水ハウス(シャーウッド)<span class="verticalLine"></span>2021年2月27日(土) 10時30分</li>
                        <li>PROUDIO - 住友林業<span class="verticalLine"></span>2021年2月27日(土) 10時30分</li>
                      </ul>
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">お名前</span>
                    </label>
                    <div class="c-form__row__field">
                      山田太郎 <input type="hidden" name="fullname" value="山田太郎">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">フリガナ</span>
                    </label>
                    <div class="c-form__row__field">
                      ヤマダタロウ <input type="hidden" name="fullname" value="ヤマダタロウ">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">電話番号</span>
                    </label>
                    <div class="c-form__row__field">
                      0310001000 <input type="hidden" name="fullname" value="0310001000">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">メールアドレス</span>
                    </label>
                    <div class="c-form__row__field">
                      example@xxxxxx.co.jp <input type="hidden" name="fullname" value="example@xxxxxx.co.jp">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">ご来場予定人数</span>
                    </label>
                    <div class="c-form__row__field">
                      1名 <input type="hidden" name="fullname" value="1名">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">ご希望の相談内容</span>
                    </label>
                    <div class="c-form__row__field">
                      <ul class="p-form--list type2">
                        <li>お見積もりについて</li>
                        <li>住宅ローンについて</li>
                      </ul>
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">ご来場予定人数</span>
                    </label>
                    <div class="c-form__row__field">
                      こちらに入力内容が入ります。これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。このダミーテキストは自由に改変することが出来ます。ダミーテキストはダミー文書やダミー文章とも呼ばれることがあります。これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。 <input type="hidden" name="fullname" value="こちらに入力内容が入ります。これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。このダミーテキストは自由に改変することが出来ます。ダミーテキストはダミー文書やダミー文章とも呼ばれることがあります。これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。">
                    </div>
                  </div>
                </div>
                <ul class="c-contact__action">
                  <li><input type="submit" name="submitConfirm" value="予約内容の変更" class="c-contact__action__button c-button is-gray">
                  </li>
                  <li><input type="submit" name="submitConfirm" value="見学予約する" class="c-contact__action__button c-button is-yellow">
              </li>
                </ul>
                <input type="hidden" id="mw_wp_form_token" name="mw_wp_form_token" value="5f44e21288"><input type="hidden" name="_wp_http_referer" value="/contact/"><input type="hidden" name="mw-wp-form-form-id" value="215"><input type="hidden" name="mw-wp-form-form-verify-token" value="5568ba116390563b279f106b7571c3e8be9dee21">
              </form>
              <!-- end .mw_wp_form -->
            </div>
          </div>
        </div><!-- ./v-contact -->
      </div><!-- ./contact-content -->
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>