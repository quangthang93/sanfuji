<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="ttl-epWrap">
    <div class="container">
      <h1 class="ttl-ep">モデルハウス見学予約</h1>
    </div>
  </div>
  <div class="breadcrumb">
  <div class="breadcrumb-inner">
    <ul>
      <li><a href="/"><span class="icon-home"></span></a></li>
      <li>モデルハウス見学予約</li>
    </ul>
  </div>
</div><!-- ./breadcrumb -->
  <div class="p-end p-booking">
    <div class="container">
      <h2 class="title-lv2">予約内容のご入力</h2>
      <div class="p-form">
        <h4 class="title-lv4">注意事項</h4>
        <ul class="p-form--list">
          <li>お申し込みフォームは、お申し込みの代行サービスであり、ご予約の完了ではありません。</li>
          <li>1回のお申し込みでモデルハウス3棟までお申し込みいただけます。</li>
          <li>ご予約にあたり、ご見学希望日時をご入力ください。ご予約は本日より3日後以降からの受付となります。</li>
          <li>ご希望日時を調整の上お申し込みのモデルハウスよりご連絡をさせていただきます。また、内容によりご希望に沿えない場合がございます。予めご了承ください。</li>
          <li>お申し込み時のご連絡先に誤記や虚偽がある場合は申し込みを無効とさせていただきます。</li>
          <li>特典はご見学された日時に提供されているサービスとなります。期間中1家族1回とさせていただきます。予めご了承ください。</li>
        </ul>
      </div><!-- ./p-form -->
      <div class="contact-content">
    <div class="v-contact">
      <div class="u-layout-smaller type2">
        <div class="c-steps">
          <div class="c-steps__col is-active">
            <span class="c-steps__col__number u-font-rajdhani">1</span>
            <p></p>
            <p class="c-steps__col__label">予約内容のご入力</p>
            <p></p>
          </div>
          <div class="c-steps__col">
            <span class="c-steps__col__number u-font-rajdhani">2</span>
            <p></p>
            <p class="c-steps__col__label">予約内容のご確認</p>
            <p></p>
          </div>
          <div class="c-steps__col">
            <span class="c-steps__col__number u-font-rajdhani">3</span>
            <p></p>
            <p class="c-steps__col__label">お申し込み完了</p>
            <p></p>
          </div>
        </div>

        <!-- edit block -->
        <div class="p-form--infor">
          <p class="title-lv3 mgb-30">注意事項をご確認のうえお客様情報をご入力ください。</p>
          <div class="p-form--infor-table">
            <table class="table type3">
              <tr>
                <th>来場希望の展示場</th>
                <td>Tvkハウジングプラザ新百合ヶ丘</td>
              </tr>
              <tr>
                <th>見学希望のモデルハウス</th>
                <td>
                  <ul>
                    <li>LANGLEY - 三井ホーム<span class="verticalLine"></span>2021年2月27日(土) 10時30分</li>
                    <li>ザ・グラヴィス - 積水ハウス(シャーウッド)<span class="verticalLine"></span>2021年2月27日(土) 10時30分</li>
                    <li>PROUDIO - 住友林業<span class="verticalLine"></span>2021年2月27日(土) 10時30分</li>
                  </ul>
                </td>
              </tr>
            </table>
          </div>
          <div class="p-form--infor-btn">
            <a href="" class="btn-change">予約するモデルハウスの変更</a>
          </div>
        </div>
        <!-- ./ edit block -->
        
        <p class="title-lv3 mgb-20">お客様情報</p>
        <div id="mw_wp_form_mw-wp-form-215" class="mw_wp_form mw_wp_form_input">
          <form method="post" action="" enctype="multipart/form-data">
            <div class="c-form">

              <div class="c-form__row"><label for="name" class="c-form__row__label"><span class="c-form__row__label__text">お名前</span> <span class="c-form__required">必須</span> </label>
                <p></p>
                <div class="c-form__row__field"><input type="text" name="name" id="name" class="c-form__input" size="60" value="" placeholder="山田太郎">
                </div>
              </div>
              <div class="c-form__row"><label for="phonetic" class="c-form__row__label"><span class="c-form__row__label__text">フリガナ</span></label>
                <p></p>
                <div class="c-form__row__field"><input type="text" name="phonetic" id="phonetic" class="c-form__input" size="60" value="" placeholder="ヤマダタロウ">
                </div>
              </div>
              <div class="c-form__row"><label for="phone" class="c-form__row__label"><span class="c-form__row__label__text">電話番号</span> <span class="c-form__required">必須</span> </label>
                <p></p>
                <div class="c-form__row__field"><input type="text" name="phone" id="phone" class="c-form__input" size="60" value="" placeholder="例) 0310001000 ※半角数字、ハイフン(-)不要">
                </div>
              </div>
              <div class="c-form__row"><label for="email" class="c-form__row__label"><span class="c-form__row__label__text">メールアドレス</span> <span class="c-form__required">必須</span> </label>
                <p></p>
                <div class="c-form__row__field"><input type="email" name="email" id="email" class="c-form__input" size="60" value="" placeholder="例) example@xxxxxx.co.jp" data-conv-half-alphanumeric="true">
                </div>
              </div>

              <div class="c-form__row"><label for="email" class="c-form__row__label"><span class="c-form__row__label__text">ご来場予定人数</span> <span class="c-form__required">必須</span> </label>
                <p></p>
                <div class="c-form__row__field">
                  <div class="input-peopleWrap">
                    <input type="number" name="email" id="email" class="c-form__input input-people js-inputPeople" min="1" value="1">
                    <span class="input-plus js-inputPlus"></span>
                  </div>
                  <span class="input-unit">名</span>
                </div>
              </div>


              <div class="c-form__row"><label for="email" class="c-form__row__label"><span class="c-form__row__label__text">ご希望の相談内容</span> <span class="c-form__required">必須</span> </label>
                <p></p>
                <div class="c-form__row__field">
                  <!-- <ul class="c-list-cbox">
                    <li class="c-form__checkbox">
                      <span class="mwform-checkbox-field horizontal-item">
                        <label for="kibou-1">
                          <input type="checkbox" name="kibou" value="kibou" id="kibou-1" class="c-form__checkbox__field">
                          <span class="mwform-checkbox-field-text">設計・プランについて</span>
                        </label>
                      </span>
                    </li>
                    <li class="c-form__checkbox">
                      <span class="mwform-checkbox-field horizontal-item">
                        <label for="kibou-2">
                          <input type="checkbox" name="kibou" value="kibou" id="kibou-2" class="c-form__checkbox__field">
                          <span class="mwform-checkbox-field-text">予算・価格帯について</span>
                        </label>
                      </span>
                    </li>
                    <li class="c-form__checkbox">
                      <span class="mwform-checkbox-field horizontal-item">
                        <label for="kibou-3">
                          <input type="checkbox" name="kibou" value="kibou" id="kibou-3" class="c-form__checkbox__field">
                          <span class="mwform-checkbox-field-text">土地・不動産について</span>
                        </label>
                      </span>
                    </li>
                    <li class="c-form__checkbox">
                      <span class="mwform-checkbox-field horizontal-item">
                        <label for="kibou-4">
                          <input type="checkbox" name="kibou" value="kibou" id="kibou-4" class="c-form__checkbox__field">
                          <span class="mwform-checkbox-field-text">資金計画について</span>
                        </label>
                      </span>
                    </li>
                    <li class="c-form__checkbox">
                      <span class="mwform-checkbox-field horizontal-item">
                        <label for="kibou-5">
                          <input type="checkbox" name="kibou" value="kibou" id="kibou-5" class="c-form__checkbox__field">
                          <span class="mwform-checkbox-field-text">リフォームについて</span>
                        </label>
                      </span>
                    </li>
                    <li class="c-form__checkbox">
                      <span class="mwform-checkbox-field horizontal-item">
                        <label for="kibou-6">
                          <input type="checkbox" name="kibou" value="kibou" id="kibou-6" class="c-form__checkbox__field">
                          <span class="mwform-checkbox-field-text">その他全般</span>
                        </label>
                      </span>
                    </li>
                  </ul> -->
                  <ul class="c-list-cbox">
                    <span class="mwform-checkbox-field horizontal-item">
                      <label>
                        <input type="checkbox" name="soudan[data][]" value="設計・プランについて">
                        <span class="mwform-checkbox-field-text">設計・プランについて</span>
                      </label>
                    </span>
                    <span class="mwform-checkbox-field horizontal-item">
                      <label>
                        <input type="checkbox" name="soudan[data][]" value="予算・価格帯について">
                        <span class="mwform-checkbox-field-text">予算・価格帯について</span>
                      </label>
                    </span>
                    <span class="mwform-checkbox-field horizontal-item">
                      <label>
                        <input type="checkbox" name="soudan[data][]" value="土地・不動産について">
                        <span class="mwform-checkbox-field-text">土地・不動産について</span>
                      </label>
                    </span>
                    <span class="mwform-checkbox-field horizontal-item">
                      <label>
                        <input type="checkbox" name="soudan[data][]" value="資金計画について">
                        <span class="mwform-checkbox-field-text">資金計画について</span>
                      </label>
                    </span>
                    <span class="mwform-checkbox-field horizontal-item">
                      <label>
                        <input type="checkbox" name="soudan[data][]" value="リフォームについて">
                        <span class="mwform-checkbox-field-text">リフォームについて</span>
                      </label>
                    </span>
                    <span class="mwform-checkbox-field horizontal-item">
                      <label>
                        <input type="checkbox" name="soudan[data][]" value="その他全般">
                        <span class="mwform-checkbox-field-text">その他全般</span>
                      </label>
                    </span>
                  </ul>
                </div>
              </div>


              <div class="c-form__row is-vertical-top"><label for="content" class="c-form__row__label"><span class="c-form__row__label__text">ご意見・ご要望など</span> <span class="c-form__required">必須</span> </label>
                <p></p>
                <div class="c-form__row__field"><textarea name="content" id="content" class="c-form__textarea" cols="50" rows="5" placeholder="入力してください"></textarea>
                </div>
              </div>
            </div>
            <div class="c-contact__privacy is-hide-confirm">
              <div class="c-contact__privacy__inner">
                <p class="c-contact__privacy__text">当社は、お客様個人を識別できる情報（以下「個人情報」といいます。）を適切に保護する為に、以下の取り組みを実施いたします。</p>
                <dl class="c-contact__privacy__list">
                  <dt class="c-contact__privacy__list__title">1.法令の遵守</dt>
                  <dd class="c-contact__privacy__list__data">当社は、個人情報保護に関する関係法令、国が定める指針等及び社内規程を遵守致します。</dd>
                  <dt class="c-contact__privacy__list__title">2.個人情報の取得</dt>
                  <dd class="c-contact__privacy__list__data">当社は、個人情報を取得する際には、その利用目的を明示し、お客様の同意の範囲内で、適正かつ公正な手段によって取得いたします。</dd>
                  <dt class="c-contact__privacy__list__title">3.利用目的</dt>
                  <dd class="c-contact__privacy__list__data">当社は、お客様からご提供いただいた個人情報を、お客様とのご契約上の責務を果たすため、およびお客様に有用な情報をご提供するために利用いたします。</dd>
                  <dt class="c-contact__privacy__list__title">4.第三者提供</dt>
                  <dd class="c-contact__privacy__list__data">当社は、お客様の個人情報をあらかじめお客様の同意をいただいている場合および法令等で定められた場合、または当社と機密保持契約を締結している業務委託先に利用目的の達成に必要な範囲内で開示する場合を除き、第三者へ開示いたしません。</dd>
                  <dt class="c-contact__privacy__list__title">5.管理体制</dt>
                  <dd class="c-contact__privacy__list__data">当社は、お客様の個人情報保護のため、情報管理責任者や個人情報を取り扱う部門毎に部門別情報管理者を置き、個人情報の適切な管理に努めます。 また、業務に従事する者に対して適切な教育を実施いたします。</dd>
                  <dt class="c-contact__privacy__list__title">6.安全管理措置</dt>
                  <dd class="c-contact__privacy__list__data">当社は、お客様からご提供いただいた個人情報に対して、不正アクセス・紛失・漏洩などを防止するためのセキュリティ対策を実施いたします。</dd>
                </dl>
              </div>
            </div>
            <div class="c-contact__privacy-check is-hide-confirm form-checkBox">
              <div class="c-form__checkbox">
                <span class="c-form__required">必須</span>
                <span class="mwform-checkbox-field horizontal-item">
                  <label for="privacy-1">
                    <input type="checkbox" name="privacy" value="privacy" id="privacy-1" class="c-form__checkbox__field">
                    <span class="mwform-checkbox-field-text"><a class="link-cbox" href="/privacy/" target="_blank">プライバシーポリシー</a>に同意します。</span>
                  </label>
                </span>
              </div>
              <p></p>
            </div>
            <ul class="c-contact__action">
              <li><input type="submit" name="submitConfirm" value="入力内容を確認する" class="c-contact__action__button c-button is-yellow">
              </li>
              <!-- <li><input type="submit" name="submitConfirm" value="入力内容を確認する" class="c-contact__action__button c-button is-yellow">
              </li> -->
            </ul>
            <input type="hidden" id="mw_wp_form_token" name="mw_wp_form_token" value="5f44e21288"><input type="hidden" name="_wp_http_referer" value="/contact/"><input type="hidden" name="mw-wp-form-form-id" value="215"><input type="hidden" name="mw-wp-form-form-verify-token" value="5568ba116390563b279f106b7571c3e8be9dee21">
          </form>
          <!-- end .mw_wp_form -->
        </div>
      </div>
    </div><!-- ./v-contact -->
  </div><!-- ./contact-content -->
    </div>
  </div>
</main><!-- ./main -->

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>