$(function() {

  /* --------------------
  GLOBAL VARIABLE
  --------------------- */
  // Selector
  // var $pageTop = $('.js-pageTop');

  // Init Value
  var breakpointSP = 767,
    breakpointTB = 1050,
    wWindow = $(window).outerWidth();


  /* --------------------
  FUNCTION COMMON
  --------------------- */
  // Menu trigger
  var triggerMenu = function() {
    $('.js-menuTrigger').click(function() {
      $('body').toggleClass('fixed');
      $(this).toggleClass('open');
      $('.js-menu').toggleClass('open');
      $('.js-menu ul li a').toggleClass('open');
    });
  }

  // Setting anchor link
  var anchorLink = function() {
    // Scroll to section
    $('a[href^="#"]').not("a[class*='carousel-control-']").click(function() {
      var href = $(this).attr("href");
      var hash = href == "#" || href == "" ? 'html' : href;
      var position = $(hash).offset().top - 100;
      $('body,html').stop().animate({ scrollTop: position }, 1000);
      return false;
    });
  }

  // Trigger Accordion
  var triggerAccordion = function() {
    var $accorLabel = $('.js-modelHouseAc'),
        $accorCnt = $('js-modelHouseAcCnt');
    $accorLabel.click(function () {
      $(this).toggleClass('active').siblings($accorCnt).slideToggle();

      var $_slider = $(this).siblings($accorCnt).find('.js-subSlidersEP');
      if ($_slider.length) {
        $_slider.get(0).slick.setPosition();
      }
    });   
  } 

  // Slider init
  var initSlider = function() {
    var $slide01 = $('.js-sliderEvent');
    var $slide02 = $('.js-sliderTop');
    var $slide03 = $('.js-slidersEP');
    var $slide03Nav = $('.js-slidersEP-nav');
    var $slider04 = $('.js-subSlidersEP');
    var $slider05 = $('.js-sectionInforSlider');
    var $slide06 = $('.js-slidersEP2');
    var $slide06Nav = $('.js-slidersEP2-nav');
    var $slider07 = $('.js-notice-slider');

    // Slider 01
    if ($slide01.length) {
      // Tabs slider
      $slide01.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        swipeToSlide: true,
        centerMode: true,
        variableWidth: true,
        prevArrow: '<a class="slick-prev" href="javscript:void(0)"><span></span></a>',
        nextArrow: '<a class="slick-next" href="javscript:void(0)"><span></span></a>'
      });
    }

    // Slider 02
    if ($slide02.length) {
      // Tabs slider
      $slide02.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        swipeToSlide: true,
        // fade: true,
        centerMode: true,
        variableWidth: true,
        dots: true,
        prevArrow: '<a class="slick-prev type2 top" href="javscript:void(0)"><span></span></a>',
        nextArrow: '<a class="slick-next type2 top" href="javscript:void(0)"><span></span></a>',
      });
    }


    // Slider 03
    if ($slide03.length) {
      // Tabs slider
      $slide03.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        swipeToSlide: true,
        centerMode: true,
        variableWidth: true,
        asNavFor: $slide03Nav,
        prevArrow: '<a class="slick-prev" href="javscript:void(0)"><span></span></a>',
        nextArrow: '<a class="slick-next" href="javscript:void(0)"><span></span></a>',
        responsive: [
          {
            breakpoint: 1050,
            settings: {
              slidesToShow: 2
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 1
            }
          }
        ]
      });
    }

    // Slider03 Nav
    if ($slide03.length && $slide03Nav.length) {
      $slide03Nav.slick({
        slidesToShow: 8,
        slidesToScroll: 1,
        swipeToSlide: true,
        // centerMode: true,
        asNavFor: $slide03,
        // arrows: true,
        dots: false,
        focusOnSelect: true,
        prevArrow: '<a class="slick-prev type3" href="javscript:void(0)"><span></span></a>',
        nextArrow: '<a class="slick-next type3" href="javscript:void(0)"><span></span></a>',
        responsive: [
          {
            breakpoint: 1050,
            settings: {
              slidesToShow: 4
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 2
            }
          }
        ]
      });
    }

    // Slider 04
    if ($slider04.length) {
      // Tabs slider
      $slider04.slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        swipeToSlide: true,
        // fade: true,
        dots: true,
        arrows: false
      });
    }

    // Slider 05
    if ($slider05.length) {
      if (wWindow >= 768) {
        if ($slider05.children().length > 3) {
          // Tabs slider
          $slider05.slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            swipeToSlide: true,
            centerMode: true,
            variableWidth: true,
            dots: false,
            prevArrow: '<a class="slick-prev" href="javscript:void(0)"><span></span></a>',
            nextArrow: '<a class="slick-next" href="javscript:void(0)"><span></span></a>',
            responsive: [{
              breakpoint: 768,
              settings: {
                slidesToShow: 1
              }
            }]
          });
        } else if ($slider05.children().length == 3) {
          $slider05.addClass('container slider-type3').children().addClass('slider-item-type2');
        } else {
          $slider05.addClass('container slider-type2').children().addClass('slider-item-type2');
        }
      } else {
        $slider05.slick({
          slidesToShow: 3,
          slidesToScroll: 1,
          autoplay: true,
          autoplaySpeed: 5000,
          swipeToSlide: true,
          centerMode: true,
          variableWidth: true,
          dots: false,
          prevArrow: '<a class="slick-prev" href="javscript:void(0)"><span></span></a>',
          nextArrow: '<a class="slick-next" href="javscript:void(0)"><span></span></a>',
          responsive: [{
            breakpoint: 768,
            settings: {
              slidesToShow: 1
            }
          }]
        });
      }

    }

    // Slider 06
    if ($slide06.length) {
      // Tabs slider
      $slide06.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        swipeToSlide: true,
        centerMode: true,
        variableWidth: true,
        asNavFor: $slide06Nav,
        prevArrow: '<a class="slick-prev" href="javscript:void(0)"><span></span></a>',
        nextArrow: '<a class="slick-next" href="javscript:void(0)"><span></span></a>',
        responsive: [
          {
            breakpoint: 1050,
            settings: {
              slidesToShow: 2
            }
          },
          {
            breakpoint: 768,
            settings: {
              dots: true,
              slidesToShow: 1
            }
          }
        ]
      });
    }

    // Slider06 Nav
    if ($slide06.length && $slide06Nav.length) {
      $slide06Nav.slick({
        slidesToShow: 8,
        slidesToScroll: 1,
        swipeToSlide: true,
        // centerMode: true,
        asNavFor: $slide06,
        // arrows: true,
        dots: false,
        focusOnSelect: true,
        prevArrow: '<a class="slick-prev type3" href="javscript:void(0)"><span></span></a>',
        nextArrow: '<a class="slick-next type3" href="javscript:void(0)"><span></span></a>',
        responsive: [
          {
            breakpoint: 1050,
            settings: {
              slidesToShow: 4
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 2
            }
          }
        ]
      });
    }

    // Slider 07
    if ($slider07.length) {
      if (wWindow >= 768) {
        if ($slider07.children().length > 3) {
          // Tabs slider
          $slider07.slick({
            arrows: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            swipeToSlide: true,
            centerMode: true,
            variableWidth: true,
            dots: false,
            responsive: [{
              breakpoint: 767,
              settings: {
                arrows: true,
                slidesToShow: 1,
                centerMode: false,
                variableWidth: false,
                prevArrow: '<a class="slick-prev type3" href="javscript:void(0)"><span></span></a>',
                nextArrow: '<a class="slick-next type3" href="javscript:void(0)"><span></span></a>',
              }
            }]
          });
        } else if ($slider07.children().length == 3) {
          $slider07.addClass('container slider-type3').children().addClass('slider-item-type2');
        } else {
          $slider07.addClass('container slider-type2').children().addClass('slider-item-type2');
        }
      } else {
        $slider07.slick({
            arrows: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            swipeToSlide: true,
            centerMode: true,
            variableWidth: true,
            dots: false,
            responsive: [{
              breakpoint: 767,
              settings: {
                arrows: true,
                slidesToShow: 1,
                centerMode: false,
                variableWidth: false,
                prevArrow: '<a class="slick-prev type3" href="javscript:void(0)"><span></span></a>',
                nextArrow: '<a class="slick-next type3" href="javscript:void(0)"><span></span></a>',
              }
            }]
          });
      }
    }

  }

  // Date picker init
  var initDatePicker = function() {
    $.datepicker.setDefaults($.datepicker.regional["ja"]);
    var $datePicker01 = $('.js-datepicker01');
    if ($datePicker01.length) {
      $datePicker01.datepicker({
        firstDay: 1,
        dateFormat: 'yy年mm月dd日'
      });
    }
    var $datePicker02 = $('.js-datepicker02');
    if ($datePicker02.length) {
      $datePicker02.datepicker({
        firstDay: 1,
        minDate:3
      });
    }
  }

  // Trigger modal search
  var triggerModalSearch = function() {
    $('.js-openModalSearch').click(function() {
      $('.js-modalSearch').show(200);
    });
    $('.js-closeModalSearch').click(function() {
      $('.js-modalSearch').hide();
    });
  }

  // Trigger modal house
  var triggerModalHouse = function() {
    if ($('.js-modalHouse').length) {
      $('.js-modalHouse').remodal();
    }
  }

  // choose area - booking page
  var chooseArea = function() {
    $('.js-choice').click(function() {
      $('.js-choice').removeClass('active');
      $(this).addClass('active');
    });
  }

  // add minute and second - booking choice page
  var addTime = function() {
    $('.js-btnHour').click(function() {
      var $min = $(this).closest('.p-choice--item-form-time').siblings('.p-choice--item-form-time').find('.js-min');
      var tmpValMin = parseInt($min.val());

      var $hour = $(this).siblings('.js-hour');
      var tmpVal = parseInt($hour.val());
      if (tmpVal == 17 || (tmpVal == 16 && tmpValMin == 30)) {
        $hour.val("10");
        check_form();
        return;
      }
      tmpVal = (tmpVal < 17) ? tmpVal + 1 : tmpVal;

      if (tmpVal >= 10) {
        $hour.val(tmpVal);
      } else {
        $hour.val("0" + tmpVal);
      }

      check_form();
    });

    $('.js-btnMin').click(function() {
      var $hour = $(this).closest('.p-choice--item-form-time').siblings('.p-choice--item-form-time').find('.js-hour');
      var tmpValHour = parseInt($hour.val());

      var $min = $(this).siblings('.js-min');
      var tmpVal = parseInt($min.val());

      if (tmpValHour == 17) {
        $hour.val("10");
        $min.val("00");
        check_form();
        return;
      }

      if (tmpVal === 30) {

        if (tmpValHour >= 9) {
          $hour.val(tmpValHour + 1);
          $min.val("00");
        } else {
          $hour.val("0" + (tmpValHour + 1));
          $min.val("00");
        }
        check_form();
        return;
      }

      tmpVal = (tmpVal < 30) ? tmpVal + 30 : tmpVal;

      if (tmpVal >= 10) {
        $min.val(tmpVal);
      } else {
        $min.val("0" + tmpVal);
      }

      check_form();

    });
  }

  // select item - booing choice page 
  var selectItem = function() {
    var $btnAdd = $('.js-btnAddItem');
    // var $itemChoiceActive = $('.js-itemChoice.active');
    $btnAdd.click(function(e) {
      // if (e.target !== this) return;
      var num = $('.js-itemChoice.active').length;
      var text = $(this).html();

      if (text === '追加' && num >= 3) {
        return;
      }

      if (text === '追加') {
        text = '選択解除';
      } else {
        num -= 1;
        text = '追加';
      }
      // text = () ? '選択解除' : '追加';
      $(this).toggleClass('active').html(text);
      $(this).closest('.p-choice--item').toggleClass('active');

      check_form();
    });
  }

  // add people - booking form page
  var addPeople = function() {
    $('.js-inputPlus').click(function() {
      var $people = $(this).siblings('.js-inputPeople');
      var tmpVal = parseInt($people.val());
      $people.val(tmpVal + 1);
    });
  }

  var matchHeight = function() {
    var $elem01 = $('.p-event--infor-item-ttl');
    var $elem02 = $('.p-top--main-posts-ttl');
    var $elem03 = $('.js-equaDesc');
    var $elem04 = $('.p-event--slider-item-ttl');
    var $elem05 = $('.p-event--slider-item');
    var $elem06 = $('.p-event--slider-item-tagWrap');
    var $elem07 = $('.notice-slider--item');
    if ($elem01.length) {
      $elem01.matchHeight();
    }
    if ($elem02.length) {
      $elem02.matchHeight();
    }
    if ($elem03.length) {
      $elem03.matchHeight();
    }
    if ($elem04.length) {
      $elem04.matchHeight();
    }
    if ($elem05.length) {
      $elem05.matchHeight();
    }
    if ($elem06.length) {
      $elem06.matchHeight();
    }
    if ($elem07.length) {
      $elem07.matchHeight();
    }
  }

  // Tabs Control
  var tabsControl = function() {
    var $tabsNav = $('.js-tabsNav'),
        $tabsItem = $('.js-tabsItem'),
        $tabsCnt = $('.js-tabsCnt'),
        $tabsPanel = $('.js-tabsPanel');

    // Setting first view
    $tabsPanel.hide();
    $tabsCnt.each(function () {
        $(this).find($tabsPanel).eq(0).show();
    });
    $tabsNav.each(function () {
        $(this).find($tabsItem).eq(0).addClass('active');
    });

    // Click event
    $tabsItem.on('click', function () {
      var tMenu = $(this).parents($tabsNav).find($tabsItem);
      var tCont = $(this).parents($tabsNav).next($tabsCnt).find($tabsPanel);
      var index = tMenu.index(this);
      tMenu.removeClass('active');
      $(this).addClass('active');
      tCont.hide();
      tCont.eq(index).show();
    });
  } 

  // Setting anchor link
  var anchorLink = function() {
    // Scroll to section
    $('a[href^="#"]').not("a[class*='carousel-control-']").click(function() {
      var href= $(this).attr("href");
      var hash = href == "#" || href == "" ? 'html' : href;
      var position = $(hash).offset().top - 100;
      $('body,html').stop().animate({scrollTop:position}, 1000);
      return false;
    });
  }

  // exhibition
  var exhibitionModal = function() {
    $('.js-exhibitionModalCtrl').click(function() {
      $('.js-exhibitionModal').toggleClass('active');
    });
  }

  /* --------------------
  INIT (WINDOW ON LOAD)
  --------------------- */
  // Run all script when DOM has loaded
  var init = function() {
    objectFitImages();
    triggerMenu();
    anchorLink();
    initSlider();
    initDatePicker();
    triggerModalSearch();
    chooseArea();
    addTime();
    addPeople();
    matchHeight();
    tabsControl();
    anchorLink();
    exhibitionModal();
    triggerModalHouse();
    selectItem();
    triggerAccordion();
  }

  init();


  /* --------------------
  WINDOW ON RESIZE
  --------------------- */
  $(window).resize(function() {
    wWindow = $(window).outerWidth();
  });


  /* --------------------
  WINDOW ON SCROLL
  --------------------- */
  $(window).scroll(function() {
    var scroll = $(this).scrollTop();
    $('.fadeup, .fadein').each(function() {
      var elemPos = $(this).offset().top;
      var windowHeight = $(window).height();
      if (scroll > elemPos - windowHeight + 100) {
        $(this).addClass('in');
      }
    });

  });

  // checked label 20210531

  $(".checkedAll").click(function() {
    $(".checkSingle").prop("checked", $(this).prop("checked"));
  });

  $(".checkSingle").click(function() {
    if (!$(this).prop("checked")) {
      $(".checkedAll").prop("checked", false);
    }
  });

  $(".checkedAll02").click(function() {
    $(".checkSingle02").prop("checked", $(this).prop("checked"));
  });

  $(".checkSingle02").click(function() {
    if (!$(this).prop("checked")) {
      $(".checkedAll02").prop("checked", false);
    }
  });


if(window.innerWidth < 769){

  var contHe = $(document).height() - $(window).height() + 50
  $(window).scroll(function(){
    var scroll = $(this).scrollTop() ;
     if(scroll < contHe){
           $('.new__homeAdd').fadeIn("slow");
     } else {
           $('.new__homeAdd').fadeOut("slow");
     }
  });
}

  

});