$(function() {
	var $btnArrow = $('.js-blogSlider').children().find('.slick-arrow');
	$btnArrow.attr("href","javascript:void(0)");

	// Trigger Accordion
  var triggerMenuCategory = function() {
    $('.js-blogNavCtrl').click(function() {
      $('body').toggleClass('fixed');
      $('.js-overlay, .js-blogNavDirect, .js-blogNavClose').toggleClass('active');
    });

    $('.js-blogNavClose').click(function() {
      $('body').removeClass('fixed');
      $('.js-overlay, .js-blogNavDirect, .js-blogNavClose').removeClass('active');
    });
  } 

  // Trigger Accordion
  var triggerFormSearch = function(e) {
    $('.js-btnBlogSearch').click(function() {
      $('.js-formBlogSearch, .blog-nav').toggleClass('active');
    });
  }

  triggerFormSearch();
  triggerMenuCategory();


  // popup 
  $('.js-blogPopupCtrl').click(function () {
    $('.js-blogOverlay, .js-blogPopup').hide();
  });
});