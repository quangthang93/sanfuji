<footer class="footer">
  <div class="footer-inner">
    <div class="footer-top">
      <div class="footer-logo">
        <a class="link" href="/"><img src="<?php echo $PATH;?>/assets/images/common/footer-logo.svg" alt=""></a>
      </div><!-- ./footer-logo -->
      <div class="footer-infor">
        <div class="footer-nav">
          <ul>
            <li><a class="book link" href="">Live-rary(コラム)</a></li>
            <li><a class="link" href="">展示場を探す</a></li>
            <li><a class="link" href="">モデルハウスを探す</a></li>
            <li><a class="speaker link" href="/event">イベント・相談会情報</a></li>
            <li><a class="news link" href="/news">お知らせ</a></li>
          </ul>
        </div><!-- ./footer-nav -->
        <div class="footer-ctrl">
          <!-- <div class="footer-ctrl--event link"><a href="">イベント申込み</a></div> -->
          <div class="footer-ctrl--study link"><a href="">モデルハウス見学</a></div>
          <a href="/privacy" class="footer-ctrl--link link">プライバシーポリシー</a>
        </div><!-- ./footer-ctrl -->
      </div><!-- ./footer-infor -->
    </div><!-- ./footer-top -->
    <div class="footer-copy">
      <p class="footer-copy--infor">
        Copyright © 神奈川県総合住宅展示場ナビ運営事務局
      </p>
    </div><!-- ./footer-copy -->
  </div><!-- ./footer-inner -->
</footer><!-- ./footer -->
</div>
  </div> <!-- .main-wrap -->
  <div class="mobile-menu-container off-canvas" id="mobile-menu">
    <a href="#" class="close"><i class="tsi tsi-times"></i></a>
    <div class="logo">
    </div>
    <ul class="mobile-menu"></ul>
  </div>
  <div class="search-modal-wrap">
    <div class="search-modal-box" role="dialog" aria-modal="true">
      <form method="get" class="search-form" action="http://sanfuji-media.f-demo.com/">
        <input type="search" class="search-field" name="s" placeholder="Search..." value="" required />
        <button type="submit" class="search-submit visuallyhidden">Submit</button>
        <p class="message">
          Type above and press <em>Enter</em> to search. Press <em>Esc</em> to cancel. </p>
      </form>
    </div>
  </div>
  <a href="https://www.pinterest.com/pin/create/bookmarklet/?url=%url%&media=%media%&description=%desc%" class="pinit-btn" target="_blank" title="Pin It" data-show-on="single" data-heading="">
    <i class="tsi tsi-pinterest-p"></i>
  </a>
  <script type='text/javascript' id='contact-form-7-js-extra'>
  /* <![CDATA[ */
  var wpcf7 = { "apiSettings": { "root": "http:\/\/sanfuji-media.f-demo.com\/index.php?rest_route=\/contact-form-7\/v1", "namespace": "contact-form-7\/v1" } };
  /* ]]> */
  </script>
  <script type='text/javascript' src='http://sanfuji-media.f-demo.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2' id='contact-form-7-js'></script>
  <script type='text/javascript' src='http://sanfuji-media.f-demo.com/wp-content/plugins/table-of-contents-plus/front.min.js?ver=2002' id='toc-front-js'></script>
  <script type='text/javascript' id='cheerup-theme-js-extra'>
  /* <![CDATA[ */
  var Bunyad = { "custom_ajax_url": "\/" };
  /* ]]> */
  </script>
  <script type='text/javascript' src='http://sanfuji-media.f-demo.com/wp-content/themes/cheerup/js/theme.js?ver=7.0.4' id='cheerup-theme-js'></script>
  <script type='text/javascript' src='http://sanfuji-media.f-demo.com/wp-content/themes/cheerup/js/lazysizes.js?ver=7.0.4' id='lazysizes-js'></script>
  <script type='text/javascript' src='http://sanfuji-media.f-demo.com/wp-content/themes/cheerup/js/jquery.mfp-lightbox.js?ver=7.0.4' id='magnific-popup-js'></script>
  <script type='text/javascript' src='http://sanfuji-media.f-demo.com/wp-content/themes/cheerup/js/jquery.fitvids.js?ver=7.0.4' id='jquery-fitvids-js'></script>
  <script type='text/javascript' src='http://sanfuji-media.f-demo.com/wp-includes/js/imagesloaded.min.js?ver=4.1.4' id='imagesloaded-js'></script>
  <script type='text/javascript' src='http://sanfuji-media.f-demo.com/wp-content/themes/cheerup/js/object-fit-images.js?ver=7.0.4' id='object-fit-images-js'></script>
  <script type='text/javascript' src='http://sanfuji-media.f-demo.com/wp-content/themes/cheerup/js/jquery.sticky-sidebar.js?ver=7.0.4' id='theia-sticky-sidebar-js'></script>
  <script type='text/javascript' src='http://sanfuji-media.f-demo.com/wp-content/themes/cheerup/js/jquery.slick.js?ver=7.0.4' id='jquery-slick-js'></script>
  <script type='text/javascript' src='http://sanfuji-media.f-demo.com/wp-content/themes/cheerup/js/jarallax.js?ver=7.0.4' id='jarallax-js'></script>
  <script type='text/javascript' src='http://sanfuji-media.f-demo.com/wp-includes/js/wp-embed.min.js?ver=5.6.1' id='wp-embed-js'></script>

  <script src="<?php echo $PATH;?>/assets/js/libs/jquery-3.5.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/ofi.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/datepicker-ja.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/slick.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/common.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/blog.js"></script>
</body>

</html>