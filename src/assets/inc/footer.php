<footer class="footer">
  <div class="footer-inner">
    <div class="footer-top">
      <div class="footer-logo">
        <a class="link" href="/"><img src="<?php echo $PATH;?>/assets/images/common/footer-logo.svg" alt=""></a>
      </div><!-- ./footer-logo -->
      <div class="footer-infor">
        <div class="footer-nav">
          <ul>
            <li><a class="book link" href="">Live-rary(コラム)</a></li>
            <li><a class="link" href="">展示場を探す</a></li>
            <li><a class="link" href="">モデルハウスを探す</a></li>
            <li><a class="speaker link" href="/event">イベント・相談会情報</a></li>
            <li><a class="news link" href="/news">お知らせ</a></li>
          </ul>
        </div><!-- ./footer-nav -->
        <div class="footer-ctrl">
          <!-- <div class="footer-ctrl--event link"><a href="">イベント申込み</a></div> -->
          <div class="footer-ctrl--study link"><a href="">モデルハウス見学</a></div>
          <a href="/privacy" class="footer-ctrl--link link">プライバシーポリシー</a>
        </div><!-- ./footer-ctrl -->
      </div><!-- ./footer-infor -->
    </div><!-- ./footer-top -->
    <div class="footer-copy">
      <p class="footer-copy--infor">
        Copyright © 神奈川県総合住宅展示場ナビ運営事務局
      </p>
    </div><!-- ./footer-copy -->
  </div><!-- ./footer-inner -->
</footer><!-- ./footer -->
</div>
<script src="<?php echo $PATH;?>/assets/js/libs/jquery-3.5.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="<?php echo $PATH;?>/assets/js/libs/datepicker-ja.js"></script>
<script src="<?php echo $PATH;?>/assets/js/libs/slick.min.js"></script>
<script src="<?php echo $PATH;?>/assets/js/libs/jquery.matchHeight-min.js"></script>
<script src="<?php echo $PATH;?>/assets/js/libs/ofi.min.js"></script>
<script src="<?php echo $PATH;?>/assets/js/libs/remodal.min.js"></script>
<script src="<?php echo $PATH;?>/assets/js/common.js"></script>
</body>

</html>