<!DOCTYPE html>
<html lang="ja" prefix="og: https://ogp.me/ns#">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link rel="pingback" href="http://sanfuji-media.f-demo.com/xmlrpc.php" />
  <link rel="profile" href="http://gmpg.org/xfn/11" />
  <title>sanfuji media - Just another WordPress site</title>
  <!-- All in One SEO 4.0.15 -->
  <meta name="description" content="Just another WordPress site" />
  <link rel="canonical" href="http://sanfuji-media.f-demo.com/" />
  <link rel="next" href="http://sanfuji-media.f-demo.com/?paged=2" />
  <meta property="og:site_name" content="sanfuji media - Just another WordPress site" />
  <meta property="og:type" content="website" />
  <meta property="og:title" content="sanfuji media - Just another WordPress site" />
  <meta property="og:description" content="Just another WordPress site" />
  <meta property="og:url" content="http://sanfuji-media.f-demo.com/" />
  <meta property="twitter:card" content="summary" />
  <meta property="twitter:domain" content="sanfuji-media.f-demo.com" />
  <meta property="twitter:title" content="sanfuji media - Just another WordPress site" />
  <meta property="twitter:description" content="Just another WordPress site" />
  <script type="application/ld+json" class="aioseo-schema">
  { "@context": "https:\/\/schema.org", "@graph": [{ "@type": "WebSite", "@id": "http:\/\/sanfuji-media.f-demo.com\/#website", "url": "http:\/\/sanfuji-media.f-demo.com\/", "name": "sanfuji media", "description": "Just another WordPress site", "publisher": { "@id": "http:\/\/sanfuji-media.f-demo.com\/#organization" }, "potentialAction": { "@type": "SearchAction", "target": "http:\/\/sanfuji-media.f-demo.com\/?s={search_term_string}", "query-input": "required name=search_term_string" } }, { "@type": "Organization", "@id": "http:\/\/sanfuji-media.f-demo.com\/#organization", "name": "sanfuji media", "url": "http:\/\/sanfuji-media.f-demo.com\/" }, { "@type": "BreadcrumbList", "@id": "http:\/\/sanfuji-media.f-demo.com\/#breadcrumblist", "itemListElement": [{ "@type": "ListItem", "@id": "http:\/\/sanfuji-media.f-demo.com\/#listItem", "position": "1", "item": { "@id": "http:\/\/sanfuji-media.f-demo.com\/#item", "name": "\u30db\u30fc\u30e0", "description": "Just another WordPress site", "url": "http:\/\/sanfuji-media.f-demo.com\/" } }] }, { "@type": "CollectionPage", "@id": "http:\/\/sanfuji-media.f-demo.com\/#collectionpage", "url": "http:\/\/sanfuji-media.f-demo.com\/", "name": "sanfuji media - Just another WordPress site", "description": "Just another WordPress site", "inLanguage": "ja", "isPartOf": { "@id": "http:\/\/sanfuji-media.f-demo.com\/#website" }, "breadcrumb": { "@id": "http:\/\/sanfuji-media.f-demo.com\/#breadcrumblist" }, "about": { "@id": "http:\/\/sanfuji-media.f-demo.com\/#organization" } }] }
  </script>
  <!-- All in One SEO -->
  <link rel='dns-prefetch' href='//fonts.googleapis.com' />
  <link rel='dns-prefetch' href='//s.w.org' />
  <link rel="alternate" type="application/rss+xml" title="sanfuji media &raquo; フィード" href="http://sanfuji-media.f-demo.com/?feed=rss2" />
  <link rel="alternate" type="application/rss+xml" title="sanfuji media &raquo; コメントフィード" href="http://sanfuji-media.f-demo.com/?feed=comments-rss2" />
  <script type="text/javascript">
  window._wpemojiSettings = { "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/", "ext": ".png", "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/", "svgExt": ".svg", "source": { "concatemoji": "http:\/\/sanfuji-media.f-demo.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6.1" } };
  ! function(e, a, t) { var n, r, o, i = a.createElement("canvas"),
      p = i.getContext && i.getContext("2d");

    function s(e, t) { var a = String.fromCharCode;
      p.clearRect(0, 0, i.width, i.height), p.fillText(a.apply(this, e), 0, 0);
      e = i.toDataURL(); return p.clearRect(0, 0, i.width, i.height), p.fillText(a.apply(this, t), 0, 0), e === i.toDataURL() }

    function c(e) { var t = a.createElement("script");
      t.src = e, t.defer = t.type = "text/javascript", a.getElementsByTagName("head")[0].appendChild(t) } for (o = Array("flag", "emoji"), t.supports = { everything: !0, everythingExceptFlag: !0 }, r = 0; r < o.length; r++) t.supports[o[r]] = function(e) { if (!p || !p.fillText) return !1; switch (p.textBaseline = "top", p.font = "600 32px Arial", e) {
        case "flag":
          return s([127987, 65039, 8205, 9895, 65039], [127987, 65039, 8203, 9895, 65039]) ? !1 : !s([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819]) && !s([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]);
        case "emoji":
          return !s([55357, 56424, 8205, 55356, 57212], [55357, 56424, 8203, 55356, 57212]) } return !1 }(o[r]), t.supports.everything = t.supports.everything && t.supports[o[r]], "flag" !== o[r] && (t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && t.supports[o[r]]);
    t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && !t.supports.flag, t.DOMReady = !1, t.readyCallback = function() { t.DOMReady = !0 }, t.supports.everything || (n = function() { t.readyCallback() }, a.addEventListener ? (a.addEventListener("DOMContentLoaded", n, !1), e.addEventListener("load", n, !1)) : (e.attachEvent("onload", n), a.attachEvent("onreadystatechange", function() { "complete" === a.readyState && t.readyCallback() })), (n = t.source || {}).concatemoji ? c(n.concatemoji) : n.wpemoji && n.twemoji && (c(n.twemoji), c(n.wpemoji))) }(window, document, window._wpemojiSettings);
  </script>
  <style type="text/css">
  img.wp-smiley,
  img.emoji {
    display: inline !important;
    border: none !important;
    box-shadow: none !important;
    height: 1em !important;
    width: 1em !important;
    margin: 0 .07em !important;
    vertical-align: -0.1em !important;
    background: none !important;
    padding: 0 !important;
  }
  </style>
  <link rel='stylesheet' id='cheerup-core-css' href='http://sanfuji-media.f-demo.com/wp-content/themes/cheerup/style.css?ver=7.0.4' type='text/css' media='all' />
  <link rel='stylesheet' id='wp-block-library-css' href='http://sanfuji-media.f-demo.com/wp-includes/css/dist/block-library/style.min.css?ver=5.6.1' type='text/css' media='all' />
  <link rel='stylesheet' id='contact-form-7-css' href='http://sanfuji-media.f-demo.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.2' type='text/css' media='all' />
  <link rel='stylesheet' id='toc-screen-css' href='http://sanfuji-media.f-demo.com/wp-content/plugins/table-of-contents-plus/screen.min.css?ver=2002' type='text/css' media='all' />
  <link rel='stylesheet' id='wordpress-popular-posts-css-css' href='http://sanfuji-media.f-demo.com/wp-content/plugins/wordpress-popular-posts/assets/css/wpp.css?ver=5.2.4' type='text/css' media='all' />
  <link rel='stylesheet' id='cheerup-fonts-css' href='https://fonts.googleapis.com/css?family=IBM+Plex+Sans%3A400%2C500%2C600%2C700%7CMerriweather%3A300%2C300i%7CLora%3A400%2C400i' type='text/css' media='all' />
  <link rel='stylesheet' id='magnific-popup-css' href='http://sanfuji-media.f-demo.com/wp-content/themes/cheerup/css/lightbox.css?ver=7.0.4' type='text/css' media='all' />
  <link rel='stylesheet' id='cheerup-icons-css' href='http://sanfuji-media.f-demo.com/wp-content/themes/cheerup/css/icons/icons.css?ver=7.0.4' type='text/css' media='all' />
  <link rel='stylesheet' id='cheerup-child-css' href='http://sanfuji-media.f-demo.com/wp-content/themes/cheerup-child/style.css?ver=5.6.1' type='text/css' media='all' />
  <style id='cheerup-child-inline-css' type='text/css'>
  .grid-post,
  .grid-post .post-excerpt {
    text-align: left;
  }

  @media (min-width: 1200px) {
    .mid-footer {
      --mf-insta-cols: 6;
    }
  }
  </style>
  <script type='text/javascript' id='jquery-core-js-extra'>
  /* <![CDATA[ */
  var Sphere_Plugin = { "ajaxurl": "http:\/\/sanfuji-media.f-demo.com\/wp-admin\/admin-ajax.php" };
  /* ]]> */
  </script>
  <script type='text/javascript' src='http://sanfuji-media.f-demo.com/wp-includes/js/jquery/jquery.min.js?ver=3.5.1' id='jquery-core-js'></script>
  <script type='text/javascript' src='http://sanfuji-media.f-demo.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2' id='jquery-migrate-js'></script>
  <script type='application/json' id='wpp-json'>
  { "sampling_active": 0, "sampling_rate": 100, "ajax_url": "http:\/\/sanfuji-media.f-demo.com\/index.php?rest_route=\/wordpress-popular-posts\/v1\/popular-posts", "ID": 0, "token": "528973cd82", "lang": 0, "debug": 0 }
  </script>
  <script type='text/javascript' src='http://sanfuji-media.f-demo.com/wp-content/plugins/wordpress-popular-posts/assets/js/wpp.min.js?ver=5.2.4' id='wpp-js-js'></script>
  <script nomodule type='text/javascript' src='http://sanfuji-media.f-demo.com/wp-content/themes/cheerup/js/ie-polyfills.js?ver=7.0.4' id='cheerup-ie-polyfills-js'></script>
  <link rel="https://api.w.org/" href="http://sanfuji-media.f-demo.com/index.php?rest_route=/" />
  <link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://sanfuji-media.f-demo.com/xmlrpc.php?rsd" />
  <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://sanfuji-media.f-demo.com/wp-includes/wlwmanifest.xml" />
  <meta name="generator" content="WordPress 5.6.1" />
  <noscript>
    <style> .wpb_animate_when_almost_visible { opacity: 1; }</style>
  </noscript>

  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@400;500;600;700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.min.css">
  <link rel="stylesheet" href="<?php echo $PATH;?>/assets/css/index.css">
  <link rel="stylesheet" href="<?php echo $PATH;?>/assets/css/icons.css">
  <link rel="stylesheet" href="<?php echo $PATH;?>/assets/css/blog.css">
</head>

<body class="home blog right-sidebar  lazy-normal has-lb has-lb-s has-slider has-slider-default home-loop-grid wpb-js-composer js-comp-ver-6.1 vc_responsive">
  <div class="main-wrap">
    <div class="wrapper blog">
    <header class="header pc-only">
      <h1 class="header-logo link">
        <a href="/"><img src="<?php echo $PATH;?>/assets/images/common/header-logo-blog.svg" alt="神奈川県の住宅展示場なら 家ーる[yell]"></a>
      </h1>
      <div class="header-menu js-menu">
        <ul>
          <li><a class="link book" href="/blog/">Live-rary(コラム)</a></li>
          <li class="sp-only"><a class="link top" href="/">トップページ</a></li>
          <li><a class="link" href="/exhibition/">展示場を探す</a></li>
          <li><a class="link" href="/model-house/">モデルハウスを探す</a></li>
          <li><a class="link speaker" href="/event">イベント・相談会情報</a></li>
          <li><a class="link news" href="/news">お知らせ</a></li>
        </ul>
        <div class="header-menu--btn">
          <!-- <div class="header-menu--event link"><a href="">イベント申込み</a></div> -->
          <div class="header-menu--study link"><a href="/booking/detail/">モデルハウス見学予約</a></div>
        </div>
      </div>
      <a class="header-ctrl js-menuTrigger" href="javascript:void(0);">
        <span></span>
        <span></span>
        <span></span>
      </a>
    </header><!-- ./header -->