<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/variables.php'; ?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>サンフジ企画</title>
  <meta name="description" content="">
  <meta property="og:site_name" content="">
  <meta property="og:title" content="">
  <meta property="og:description" content="">
  <meta property="og:type" content="website">
  <meta property="og:url" content="">
  <meta property="og:image" content="">
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="">
  <meta name="twitter:image" content="">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@400;500;600;700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.min.css">
  <link rel="stylesheet" href="<?php echo $PATH;?>/assets/css/index.css">
  <link rel="stylesheet" href="<?php echo $PATH;?>/assets/css/blog.css">
</head>

<body>
  <div class="wrapper">
    <header class="header">
      <h1 class="header-logo link">
        <a href="/"><img src="<?php echo $PATH;?>/assets/images/common/header-logo-blog.svg" alt="神奈川県の住宅展示場なら 家ーる[yell]"></a>
      </h1>
      <div class="header-menu js-menu">
        <ul>
          <li><a class="link book" href="/blog/">Live-rary(コラム)</a></li>
          <li class="sp-only"><a class="link top" href="/">トップページ</a></li>
          <li><a class="link" href="/exhibition/">展示場を探す</a></li>
          <li><a class="link" href="/model-house/">モデルハウスを探す</a></li>
          <li><a class="link speaker" href="/event">イベント・相談会情報</a></li>
          <li><a class="link news" href="/news">お知らせ</a></li>
        </ul>
        <div class="header-menu--btn">
          <!-- <div class="header-menu--event link"><a href="">イベント申込み</a></div> -->
          <div class="header-menu--study link"><a href="/booking/detail/">モデルハウス見学予約</a></div>
        </div>
      </div>
      <a class="header-ctrl js-menuTrigger" href="javascript:void(0);">
        <span></span>
        <span></span>
        <span></span>
      </a>
    </header><!-- ./header -->

