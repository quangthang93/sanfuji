<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="ttl-epWrap">
    <div class="container">
      <h1 class="ttl-ep">電話・オンライン相談</h1>
    </div>
  </div>
  <div class="breadcrumb">
  <div class="breadcrumb-inner">
    <ul>
      <li><a href="/"><span class="icon-home"></span></a></li>
      <li>電話・オンライン相談</li>
    </ul>
  </div>
</div><!-- ./breadcrumb -->
  <div class="p-end p-booking">
    <div class="container">
      <h2 class="title-lv2">ご入力内容のご確認</h2>
      <div class="contact-content">
        <div class="v-contact">
          <div class="u-layout-smaller type2">
            <div class="c-steps">
              <div class="c-steps__col">
                <span class="c-steps__col__number u-font-rajdhani">1</span>
                <p></p>
                <p class="c-steps__col__label">お客様情報を入力</p>
                <p></p>
              </div>
              <div class="c-steps__col is-active">
                <span class="c-steps__col__number u-font-rajdhani">2</span>
                <p></p>
                <p class="c-steps__col__label">入力内容のご確認</p>
                <p></p>
              </div>
              <div class="c-steps__col">
                <span class="c-steps__col__number u-font-rajdhani">3</span>
                <p></p>
                <p class="c-steps__col__label">お申し込み完了</p>
                <p></p>
              </div>
            </div>

            <p class="title-lv3 mgb-35">お客様情報</p>
            <div id="mw_wp_form_mw-wp-form-215" class="mw_wp_form mw_wp_form_input">
              <form method="post" action="" enctype="multipart/form-data">
                <div class="c-form">
                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">相談希望のモデルハウス</span>
                    </label>
                    <div class="c-form__row__field">
                      アイ工務店 <input type="hidden" name="fullname" value="アイ工務店">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">ご希望の相談日時</span>
                    </label>
                    <div class="c-form__row__field">
                      2021年4月5日 &nbsp; 10:00 <input type="hidden" name="fullname" value="2021年4月5日 &nbsp; 10:00">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">ご希望の相談方法</span>
                    </label>
                    <div class="c-form__row__field">
                      オンライン(zoom) <input type="hidden" name="fullname" value="オンライン(zoom)">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">ご希望の相談内容</span>
                    </label>
                    <div class="c-form__row__field">
                      <ul class="p-form--list type2">
                        <li>設計・プランについて</li>
                        <li>予算・価格帯について</li>
                      </ul>
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">お名前</span>
                    </label>
                    <div class="c-form__row__field">
                      山田太郎 <input type="hidden" name="fullname" value="山田太郎">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">フリガナ</span>
                    </label>
                    <div class="c-form__row__field">
                      ヤマダタロウ <input type="hidden" name="fullname" value="ヤマダタロウ">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">電話番号</span>
                    </label>
                    <div class="c-form__row__field">
                      0310001000 <input type="hidden" name="fullname" value="0310001000">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">メールアドレス</span>
                    </label>
                    <div class="c-form__row__field">
                      example@xxxxxx.co.jp <input type="hidden" name="fullname" value="example@xxxxxx.co.jp">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">ご職業</span>
                    </label>
                    <div class="c-form__row__field">
                      会社員 <input type="hidden" name="fullname" value="会社員">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">郵便番号</span>
                    </label>
                    <div class="c-form__row__field">
                      1234567 <input type="hidden" name="fullname" value="1234567">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">性別</span>
                    </label>
                    <div class="c-form__row__field">
                      男性 <input type="hidden" name="fullname" value="男性">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">年代</span>
                    </label>
                    <div class="c-form__row__field">
                      40代 <input type="hidden" name="fullname" value="40代">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">ご意見・ご要望など</span>
                    </label>
                    <div class="c-form__row__field">
                      こちらに入力内容が入ります。これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。このダミーテキストは自由に改変することが出来ます。ダミーテキストはダミー文書やダミー文章とも呼ばれることがあります。これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。 <input type="hidden" name="fullname" value="こちらに入力内容が入ります。これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。このダミーテキストは自由に改変することが出来ます。ダミーテキストはダミー文書やダミー文章とも呼ばれることがあります。これは正式な文章の代わりに入れて使うダミーテキストです。この組見本は自由に複製したり頒布することができます。">
                    </div>
                  </div>
                </div>
                <ul class="c-contact__action">
                  <li><input type="submit" name="submitConfirm" value="入力内容の変更" class="c-contact__action__button c-button is-gray">
                  </li>
                  <li><input type="submit" name="submitConfirm" value="電話・オンライン相談のお申し込み" class="c-contact__action__button c-button is-yellow">
              </li>
                </ul>
                <input type="hidden" id="mw_wp_form_token" name="mw_wp_form_token" value="5f44e21288"><input type="hidden" name="_wp_http_referer" value="/contact/"><input type="hidden" name="mw-wp-form-form-id" value="215"><input type="hidden" name="mw-wp-form-form-verify-token" value="5568ba116390563b279f106b7571c3e8be9dee21">
              </form>
              <!-- end .mw_wp_form -->
            </div>
          </div>
        </div><!-- ./v-contact -->
      </div><!-- ./contact-content -->
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>