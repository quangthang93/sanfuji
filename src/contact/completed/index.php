<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="ttl-epWrap">
    <div class="container">
      <h1 class="ttl-ep">電話・オンライン相談</h1>
    </div>
  </div>
  <div class="breadcrumb">
  <div class="breadcrumb-inner">
    <ul>
      <li><a href="/"><span class="icon-home"></span></a></li>
      <li>電話・オンライン相談</li>
    </ul>
  </div>
</div><!-- ./breadcrumb -->
  <div class="p-end p-booking">
    <div class="container">
      <h2 class="title-lv2">お申し込み完了</h2>
      <div class="contact-content">
        <div class="v-contact">
          <div class="u-layout-smaller type2">
            <div class="c-steps">
              <div class="c-steps__col">
                <span class="c-steps__col__number u-font-rajdhani">1</span>
                <p></p>
                <p class="c-steps__col__label">お客様情報を入力</p>
                <p></p>
              </div>
              <div class="c-steps__col">
                <span class="c-steps__col__number u-font-rajdhani">2</span>
                <p></p>
                <p class="c-steps__col__label">入力内容のご確認</p>
                <p></p>
              </div>
              <div class="c-steps__col is-active">
                <span class="c-steps__col__number u-font-rajdhani">3</span>
                <p></p>
                <p class="c-steps__col__label">お申し込み完了</p>
                <p></p>
              </div>
            </div>

            <p class="title-lv3 mgb-35">電話・オンライン相談をお申込みいただきありがとうございます。</p>
            <p class="desc2">お送りいただいた内容はスタッフが確認し、ご希望日時を調整の上、お申し込みのモデルハウスよりご連絡をさせていただきます。また、内容によりご希望に沿えない場合がございます。予めご了承ください。<br>お申し込みのモデルハウスからはメールにてご連絡いたしますので、メール受信できるよう、お客様ご自身で設定変更をお願いします。</p>
            <div class="align-center mgt-80">
              <a href="/" class="link2">トップページ</a>
            </div>
          </div>
        </div><!-- ./v-contact -->
      </div><!-- ./contact-content -->
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>