<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="ttl-epWrap">
    <div class="container">
      <h1 class="ttl-ep">電話・オンライン相談</h1>
    </div>
  </div>
  <div class="breadcrumb">
    <div class="breadcrumb-inner">
      <ul>
        <li><a href="/"><span class="icon-home"></span></a></li>
        <li>電話・オンライン相談</li>
      </ul>
    </div>
  </div><!-- ./breadcrumb -->
  <div class="p-end p-booking">
    <div class="container">
      <h2 class="title-lv2">お客様情報をご入力</h2>
      <p class="desc mgb-15">Tvkハウジングプラザ新百合ヶ丘の電話・オンライン相談のお申込みに際して注意事項をご確認いただき、お客様情報をご入力ください。</p>
      <div class="p-form">
        <h4 class="title-lv4">注意事項</h4>
        <ul class="p-form--list">
          <li>下記お申込みフォームは、お申込みの代行サービスです。</li>
          <li>後日、お申込みの住宅メーカーより連絡させていただきます。</li>
          <li>住宅メーカーによっては、折り返しの連絡に日数がかかる場合あります。また、オンライン相談は実施せず電話相談のみのメーカーもあります。ご了承ください。</li>
          <li>当社規定により、ご希望に添えない場合もございます。</li>
          <li>お申込み内容確認のため「@housing-messe.com」からのメールが受信できるよう設定をお願い致します。</li>
          <li>30分たっても返信メールが届かない場合、<a class="link-contact" href="https://www.housing-messe.com/inquiry.php" target="_blank">お問い合わせフォーム</a>よりお問い合わせください。</li>
          <li>本ウェブサイトにご入力頂きましたお客様の個人情報は、お客様が相談を希望する住宅メーカーに対してのみお伝えします。尚、住宅メーカーへ伝達したお客様の個人情報に関しては住宅メーカーが責任を持って管理いたしますので直接お問い合わせください。</li>
          <li>入力された個人情報は、ご相談についての必要事項のご連絡に利用させて頂き、法令等で開示を求められた場合を除き、本人の許可なしに第三者に提供する事はありません。詳しくは<a class="link-contact" href="/privacy/">個人情報保護方針</a>をご参照ください。</li>
        </ul>
      </div><!-- ./p-form -->
      <div class="contact-content">
        <div class="v-contact">
          <div class="u-layout-smaller type2">
            <div class="c-steps">
              <div class="c-steps__col is-active">
                <span class="c-steps__col__number u-font-rajdhani">1</span>
                <p></p>
                <p class="c-steps__col__label">お客様情報を入力</p>
                <p></p>
              </div>
              <div class="c-steps__col">
                <span class="c-steps__col__number u-font-rajdhani">2</span>
                <p></p>
                <p class="c-steps__col__label">入力内容のご確認</p>
                <p></p>
              </div>
              <div class="c-steps__col">
                <span class="c-steps__col__number u-font-rajdhani">3</span>
                <p></p>
                <p class="c-steps__col__label">お申し込み完了</p>
                <p></p>
              </div>
            </div>
            <p class="title-lv3 mgb-20">お客様情報</p>
            <div id="mw_wp_form_mw-wp-form-215" class="mw_wp_form mw_wp_form_input">
              <form method="post" action="" enctype="multipart/form-data">
                <div class="c-form">

                  <div class="c-form__row"><label for="product" class="c-form__row__label"><span class="c-form__row__label__text">相談希望のモデルハウス</span> <span class="c-form__required">必須</span> </label>
                    <div class="c-form__row__field">
                      <p class="desc2 mgb-10">Tvkハウジングプラザ新百合ヶ丘</p>
                      <div class="c-form__select"><select name="product" class="c-form__select__field">
                          <option value="" selected="selected">
                            ご相談希望のモデルハウスを選択して下さい </option>
                          <option value="その他">
                            その他 </option>
                        </select>
                        <span class="input-plus2 js-inputPlus"></span>
                      </div>
                    </div>
                  </div>


                  <div class="c-form__row"><label for="people" class="c-form__row__label"><span class="c-form__row__label__text">ご希望の相談日時</span> <span class="c-form__required">必須</span> </label>
                    <p></p>
                    <div class="c-form__row__field">
                      <div class="input-peopleWrap--box">
                        <div class="input-peopleWrap--box-item date">
                          <div class="input-peopleWrap">
                            <input type="text" name="people" id="people" class="c-form__input input-people type2 js-datepicker01" readonly="readonly" value="ご相談希望日を選択して下さい">
                            <span class="input-plus2 js-inputPlus"></span>
                          </div>
                        </div>
                        <div class="input-peopleWrap--box-item">
                          <div class="input-peopleWrap">
                            <input type="number" name="people" id="people" class="c-form__input input-people type2 js-hour" value="01" min="0" max="23" step="1">
                            <span class="input-plus js-btnHour"></span>
                          </div>
                          <span class="input-unit">時</span>
                        </div>
                        <div class="input-peopleWrap--box-item">
                          <div class="input-peopleWrap">
                            <input type="number" name="people" id="people" class="c-form__input input-people type2 js-min" value="00" min="0" max="59" step="1">
                            <span class="input-plus js-btnMin"></span>
                          </div>
                          <span class="input-unit">分</span>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="c-form__row"><label for="" class="c-form__row__label"><span class="c-form__row__label__text">ご希望の相談方法</span> <span class="c-form__required">必須</span> </label>
                    <div class="c-form__row__field">
                      <div class="c-form__radio"> <span class="mwform-radio-field horizontal-item">
                          <label>
                            <input type="radio" name="type" value="電話" class="horizontal-item">
                            <span class="mwform-radio-field-text">電話</span>
                          </label>
                        </span>
                        <span class="mwform-radio-field horizontal-item">
                          <label>
                            <input type="radio" name="type" value="オンライン(zoom)" class="horizontal-item">
                            <span class="mwform-radio-field-text">オンライン(zoom)</span>
                          </label>
                        </span>
                        <input type="hidden" name="__children[type][]" value="{&quot;\u88fd\u54c1\u306e\u3054\u76f8\u8ac7&quot;:&quot;\u88fd\u54c1\u306e\u3054\u76f8\u8ac7&quot;,&quot;\u8cc7\u6599\u8acb\u6c42&quot;:&quot;\u8cc7\u6599\u8acb\u6c42&quot;}">
                      </div>
                      <p class="desc3">※オンライン相談を実施していないモデルハウスもあります。</p>
                    </div>
                  </div>
                  <div class="c-form__row"><label for="email" class="c-form__row__label"><span class="c-form__row__label__text">ご希望の相談内容</span> <span class="c-form__required">必須</span> </label>
                    <p></p>
                    <div class="c-form__row__field">
                      <ul>
                        <li class="c-form__checkbox">
                          <span class="mwform-checkbox-field horizontal-item">
                            <label for="kibou-1">
                              <input type="checkbox" name="kibou" value="kibou" id="kibou-1" class="c-form__checkbox__field">
                              <span class="mwform-checkbox-field-text">設計・プランについて</span>
                            </label>
                          </span>
                        </li>
                        <li class="c-form__checkbox">
                          <span class="mwform-checkbox-field horizontal-item">
                            <label for="kibou-2">
                              <input type="checkbox" name="kibou" value="kibou" id="kibou-2" class="c-form__checkbox__field">
                              <span class="mwform-checkbox-field-text">予算・価格帯について</span>
                            </label>
                          </span>
                        </li>
                        <li class="c-form__checkbox">
                          <span class="mwform-checkbox-field horizontal-item">
                            <label for="kibou-3">
                              <input type="checkbox" name="kibou" value="kibou" id="kibou-3" class="c-form__checkbox__field">
                              <span class="mwform-checkbox-field-text">土地・不動産について</span>
                            </label>
                          </span>
                        </li>
                        <li class="c-form__checkbox">
                          <span class="mwform-checkbox-field horizontal-item">
                            <label for="kibou-4">
                              <input type="checkbox" name="kibou" value="kibou" id="kibou-4" class="c-form__checkbox__field">
                              <span class="mwform-checkbox-field-text">資金計画について</span>
                            </label>
                          </span>
                        </li>
                        <li class="c-form__checkbox">
                          <span class="mwform-checkbox-field horizontal-item">
                            <label for="kibou-5">
                              <input type="checkbox" name="kibou" value="kibou" id="kibou-5" class="c-form__checkbox__field">
                              <span class="mwform-checkbox-field-text">リフォームについて</span>
                            </label>
                          </span>
                        </li>
                        <li class="c-form__checkbox">
                          <span class="mwform-checkbox-field horizontal-item">
                            <label for="kibou-6">
                              <input type="checkbox" name="kibou" value="kibou" id="kibou-6" class="c-form__checkbox__field">
                              <span class="mwform-checkbox-field-text">その他全般</span>
                            </label>
                          </span>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="c-form__row"><label for="name" class="c-form__row__label"><span class="c-form__row__label__text">お名前</span> <span class="c-form__required">必須</span> </label>
                    <p></p>
                    <div class="c-form__row__field"><input type="text" name="name" id="name" class="c-form__input" size="60" value="" placeholder="山田太郎">
                    </div>
                  </div>
                  <div class="c-form__row"><label for="phonetic" class="c-form__row__label"><span class="c-form__row__label__text">フリガナ</span></label>
                    <p></p>
                    <div class="c-form__row__field"><input type="text" name="phonetic" id="phonetic" class="c-form__input" size="60" value="" placeholder="ヤマダタロウ">
                    </div>
                  </div>
                  <div class="c-form__row"><label for="phone" class="c-form__row__label"><span class="c-form__row__label__text">電話番号</span> <span class="c-form__required">必須</span> </label>
                    <p></p>
                    <div class="c-form__row__field"><input type="text" name="phone" id="phone" class="c-form__input" size="60" value="" placeholder="例) 0310001000 ※半角数字、ハイフン(-)不要">
                    </div>
                  </div>
                  <div class="c-form__row"><label for="email" class="c-form__row__label"><span class="c-form__row__label__text">メールアドレス</span> <span class="c-form__required">必須</span> </label>
                    <p></p>
                    <div class="c-form__row__field"><input type="email" name="email" id="email" class="c-form__input" size="60" value="" placeholder="例) example@xxxxxx.co.jp" data-conv-half-alphanumeric="true">
                    </div>
                  </div>
                  <div class="c-form__row"><label for="postal" class="c-form__row__label"><span class="c-form__row__label__text">郵便番号</span> <span class="c-form__required">必須</span> </label>
                    <p></p>
                    <div class="c-form__row__field"><input type="text" name="postal" id="postal" class="c-form__input" size="60" value="" placeholder="例) 1234567 ※半角数字、ハイフン(-)不要">
                    </div>
                  </div>
                  <div class="c-form__row"><label for="" class="c-form__row__label"><span class="c-form__row__label__text">ご職業</span>  </label>
                    <div class="c-form__row__field">
                      <div class="c-form__radio">
                       <span class="mwform-radio-field horizontal-item">
                          <label>
                            <input type="radio" name="type" value="会社員" class="horizontal-item">
                            <span class="mwform-radio-field-text">会社員</span>
                          </label>
                        </span>
                        <span class="mwform-radio-field horizontal-item">
                          <label>
                            <input type="radio" name="type" value="会社役員" class="horizontal-item">
                            <span class="mwform-radio-field-text">会社役員</span>
                          </label>
                        </span>
                        <span class="mwform-radio-field horizontal-item">
                          <label>
                            <input type="radio" name="type" value="公務員" class="horizontal-item">
                            <span class="mwform-radio-field-text">公務員</span>
                          </label>
                        </span>
                        <span class="mwform-radio-field horizontal-item">
                          <label>
                            <input type="radio" name="type" value="自営業" class="horizontal-item">
                            <span class="mwform-radio-field-text">自営業</span>
                          </label>
                        </span>
                        <span class="mwform-radio-field horizontal-item">
                          <label>
                            <input type="radio" name="type" value="自由業" class="horizontal-item">
                            <span class="mwform-radio-field-text">自由業</span>
                          </label>
                        </span>
                        <span class="mwform-radio-field horizontal-item">
                          <label>
                            <input type="radio" name="type" value="無職" class="horizontal-item">
                            <span class="mwform-radio-field-text">無職</span>
                          </label>
                        </span>
                      </div>
                      <div class="c-form__radio">
                        <span class="mwform-radio-field horizontal-item">
                          <label>
                            <input type="radio" name="type" value="その他" class="horizontal-item">
                            <span class="mwform-radio-field-text">その他</span>
                          </label>
                        </span>
                        <div class="c-form__row__field type2">
                          <input type="text" name="name" id="name" class="c-form__input" size="60" value="" placeholder="※その他の場合はご記入ください">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="c-form__row"><label for="" class="c-form__row__label"><span class="c-form__row__label__text">性別</span></label>
                    <div class="c-form__row__field">
                      <div class="c-form__radio"> <span class="mwform-radio-field horizontal-item">
                          <label>
                            <input type="radio" name="type" value="男性" class="horizontal-item">
                            <span class="mwform-radio-field-text">男性</span>
                          </label>
                        </span>
                        <span class="mwform-radio-field horizontal-item">
                          <label>
                            <input type="radio" name="type" value="女性" class="horizontal-item">
                            <span class="mwform-radio-field-text">女性</span>
                          </label>
                        </span>
                        <input type="hidden" name="__children[type][]" value="{&quot;\u88fd\u54c1\u306e\u3054\u76f8\u8ac7&quot;:&quot;\u88fd\u54c1\u306e\u3054\u76f8\u8ac7&quot;,&quot;\u8cc7\u6599\u8acb\u6c42&quot;:&quot;\u8cc7\u6599\u8acb\u6c42&quot;}">
                      </div>
                    </div>
                  </div>
                 <div class="c-form__row"><label for="product" class="c-form__row__label"><span class="c-form__row__label__text">年代</span></label>
                    <div class="c-form__row__field">
                      <div class="c-form__select"><select name="product" class="c-form__select__field">
                          <option value="" selected="selected">
                            年代を選択して下さい </option>
                          <option value="その他">
                            その他 </option>
                        </select>
                        <span class="input-plus2 js-inputPlus"></span>
                      </div>
                    </div>
                  </div>
                  <div class="c-form__row is-vertical-top"><label for="content" class="c-form__row__label"><span class="c-form__row__label__text">ご意見・ご要望など</span></label>
                    <p></p>
                    <div class="c-form__row__field"><textarea name="content" id="content" class="c-form__textarea" cols="50" rows="5" placeholder="入力してください"></textarea>
                    </div>
                  </div>
                </div>
                <div class="c-contact__privacy is-hide-confirm">
                  <div class="c-contact__privacy__inner">
                    <p class="c-contact__privacy__text">当社は、お客様個人を識別できる情報（以下「個人情報」といいます。）を適切に保護する為に、以下の取り組みを実施いたします。</p>
                    <dl class="c-contact__privacy__list">
                      <dt class="c-contact__privacy__list__title">1.法令の遵守</dt>
                      <dd class="c-contact__privacy__list__data">当社は、個人情報保護に関する関係法令、国が定める指針等及び社内規程を遵守致します。</dd>
                      <dt class="c-contact__privacy__list__title">2.個人情報の取得</dt>
                      <dd class="c-contact__privacy__list__data">当社は、個人情報を取得する際には、その利用目的を明示し、お客様の同意の範囲内で、適正かつ公正な手段によって取得いたします。</dd>
                      <dt class="c-contact__privacy__list__title">3.利用目的</dt>
                      <dd class="c-contact__privacy__list__data">当社は、お客様からご提供いただいた個人情報を、お客様とのご契約上の責務を果たすため、およびお客様に有用な情報をご提供するために利用いたします。</dd>
                      <dt class="c-contact__privacy__list__title">4.第三者提供</dt>
                      <dd class="c-contact__privacy__list__data">当社は、お客様の個人情報をあらかじめお客様の同意をいただいている場合および法令等で定められた場合、または当社と機密保持契約を締結している業務委託先に利用目的の達成に必要な範囲内で開示する場合を除き、第三者へ開示いたしません。</dd>
                      <dt class="c-contact__privacy__list__title">5.管理体制</dt>
                      <dd class="c-contact__privacy__list__data">当社は、お客様の個人情報保護のため、情報管理責任者や個人情報を取り扱う部門毎に部門別情報管理者を置き、個人情報の適切な管理に努めます。 また、業務に従事する者に対して適切な教育を実施いたします。</dd>
                      <dt class="c-contact__privacy__list__title">6.安全管理措置</dt>
                      <dd class="c-contact__privacy__list__data">当社は、お客様からご提供いただいた個人情報に対して、不正アクセス・紛失・漏洩などを防止するためのセキュリティ対策を実施いたします。</dd>
                    </dl>
                  </div>
                </div>
                <div class="c-contact__privacy-check is-hide-confirm form-checkBox">
                  <div class="c-form__checkbox">
                    <span class="c-form__required">必須</span>
                    <span class="mwform-checkbox-field horizontal-item">
                      <label for="privacy-1">
                        <input type="checkbox" name="privacy" value="privacy" id="privacy-1" class="c-form__checkbox__field">
                        <span class="mwform-checkbox-field-text"><a class="link-cbox" href="/privacy/" target="_blank">プライバシーポリシー</a>に同意します。</span>
                      </label>
                    </span>
                  </div>
                  <p></p>
                </div>
                <ul class="c-contact__action">
                  <li><input type="submit" name="submitConfirm" value="入力内容を確認する" class="c-contact__action__button c-button is-yellow">
                  </li>
                  <!-- <li><input type="submit" name="submitConfirm" value="入力内容を確認する" class="c-contact__action__button c-button is-yellow">
              </li> -->
                </ul>
                <input type="hidden" id="mw_wp_form_token" name="mw_wp_form_token" value="5f44e21288"><input type="hidden" name="_wp_http_referer" value="/contact/"><input type="hidden" name="mw-wp-form-form-id" value="215"><input type="hidden" name="mw-wp-form-form-verify-token" value="5568ba116390563b279f106b7571c3e8be9dee21">
              </form>
              <!-- end .mw_wp_form -->
            </div>
          </div>
        </div><!-- ./v-contact -->
      </div><!-- ./contact-content -->
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>