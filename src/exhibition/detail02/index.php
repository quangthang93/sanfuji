<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="ttl-ep2Wrap">
    <div class="container">
      <h1 class="ttl-ep2">Tvkハウジングプラザ新百合ヶ丘</h1>
    </div>
  </div>
  <div class="breadcrumb">
    <div class="breadcrumb-inner">
      <ul>
        <li><a href="/"><span class="icon-home"></span></a></li>
        <li><a href="/exhibition/">展示場を探す</a></li>
        <li>Tvkハウジングプラザ新百合ヶ丘</li>
      </ul>
    </div>
  </div><!-- ./breadcrumb -->
  <div class="slidersEP-wrapper">
    <div class="slidersEP-head">
      <div class="container">
        <p class="location">湘南・西湘エリア</p>
        <div class="slidersEP-head--sns">
          <a href="" class="btn-share"><span>SHARE</span></a>
          <a href="" class="btn-share twitter"><span>SHARE</span></a>
        </div>
      </div>
    </div><!-- ./slidersEP-head -->
    <div class="slidersEP js-slidersEP">
      <div class="slidersEP-item">
        <div class="slidersEP-item-thumb">
          <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/pict01.png" alt="">
        </div>
      </div><!-- ./slidersEP--item -->
      <div class="slidersEP-item">
        <div class="slidersEP-item-thumb">
          <img src="<?php echo $PATH;?>/assets/images/exhibition/pict02.png" alt="">
        </div>
      </div><!-- ./slidersEP--item -->
      <div class="slidersEP-item">
        <div class="slidersEP-item-thumb">
          <img src="<?php echo $PATH;?>/assets/images/exhibition/pict03.png" alt="">
        </div>
      </div><!-- ./slidersEP--item -->
      <div class="slidersEP-item">
        <div class="slidersEP-item-thumb">
          <img src="<?php echo $PATH;?>/assets/images/exhibition/slider01.png" alt="">
        </div>
      </div><!-- ./slidersEP--item -->
      <div class="slidersEP-item">
        <div class="slidersEP-item-thumb">
          <img src="<?php echo $PATH;?>/assets/images/exhibition/pict01.png" alt="">
        </div>
      </div><!-- ./slidersEP--item -->
      <div class="slidersEP-item">
        <div class="slidersEP-item-thumb">
          <img src="<?php echo $PATH;?>/assets/images/exhibition/pict02.png" alt="">
        </div>
      </div><!-- ./slidersEP--item -->
      <div class="slidersEP-item">
        <div class="slidersEP-item-thumb">
          <img src="<?php echo $PATH;?>/assets/images/exhibition/pict03.png" alt="">
        </div>
      </div><!-- ./slidersEP--item -->
      <div class="slidersEP-item">
        <div class="slidersEP-item-thumb">
          <img src="<?php echo $PATH;?>/assets/images/exhibition/pict01.png" alt="">
        </div>
      </div><!-- ./slidersEP--item -->
      <div class="slidersEP-item">
        <div class="slidersEP-item-thumb">
          <img src="<?php echo $PATH;?>/assets/images/exhibition/pict02.png" alt="">
        </div>
      </div><!-- ./slidersEP--item -->
      <div class="slidersEP-item">
        <div class="slidersEP-item-thumb">
          <img src="<?php echo $PATH;?>/assets/images/exhibition/pict03.png" alt="">
        </div>
      </div><!-- ./slidersEP--item -->
    </div><!-- ./slidersEP -->
    <div class="slidersEP-navWrap">
      <div class="container">
        <div class="slidersEP-nav js-slidersEP-nav">
          <div class="slidersEP-nav--item">
            <div class="slidersEP-nav--item-thumb">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/slider01.png" alt="">
            </div>
          </div><!-- ./slidersEP--item -->
          <div class="slidersEP-nav--item">
            <div class="slidersEP-nav--item-thumb">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/slider03.png" alt="">
            </div>
          </div><!-- ./slidersEP--item -->
          <div class="slidersEP-nav--item">
            <div class="slidersEP-nav--item-thumb">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/slider01.png" alt="">
            </div>
          </div><!-- ./slidersEP--item -->
          <div class="slidersEP-nav--item">
            <div class="slidersEP-nav--item-thumb">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/slider02.png" alt="">
            </div>
          </div><!-- ./slidersEP--item -->
          <div class="slidersEP-nav--item">
            <div class="slidersEP-nav--item-thumb">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/slider01.png" alt="">
            </div>
          </div><!-- ./slidersEP--item -->
          <div class="slidersEP-nav--item">
            <div class="slidersEP-nav--item-thumb">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/slider02.png" alt="">
            </div>
          </div><!-- ./slidersEP--item -->
          <div class="slidersEP-nav--item">
            <div class="slidersEP-nav--item-thumb">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/slider01.png" alt="">
            </div>
          </div><!-- ./slidersEP--item -->
          <div class="slidersEP-nav--item">
            <div class="slidersEP-nav--item-thumb">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/slider02.png" alt="">
            </div>
          </div><!-- ./slidersEP--item -->
          <div class="slidersEP-nav--item">
            <div class="slidersEP-nav--item-thumb">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/slider01.png" alt="">
            </div>
          </div><!-- ./slidersEP--item -->
          <div class="slidersEP-nav--item">
            <div class="slidersEP-nav--item-thumb">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/slider02.png" alt="">
            </div>
          </div><!-- ./slidersEP--item -->
        </div><!-- ./slidersEP -->
      </div>
    </div><!-- ./slidersEP-navWap -->
  </div><!-- ./slidersEP--wrapper -->
  <div class="p-end type2">
    <div class="p-exhibition">
      <div class="container">
        <div class="p-exhibition--overview">
          <h3 class="title-lv3">展示場概要</h3>
          <div class="col2-table">
            <div class="col2-table-item">
              <table class="table">
                <tr>
                  <th>所在地</th>
                  <td>〒215-0004<br>神奈川県川崎市麻生区万福寺4-3-1</td>
                </tr>
                <tr>
                  <th>電話番号</th>
                  <td>0120-666-658</td>
                </tr>
                <tr>
                  <th>営業時間</th>
                  <td>10:00～18:00</td>
                </tr>
              </table>
            </div>
            <div class="col2-table-item">
              <p class="desc2">神奈川県内でも特に取り扱い数の充実に自信のある総合住宅展示場です。都会の家づくりを知り尽くした住宅メーカーの魅力ある提案がいっぱいのモデルハウスは必見です。<br>常駐スタッフは住宅に関する豊富な知識を持っておりますので、お客様のどんなお悩みにも真摯に対応させていただきます。 ご夫婦、ご家族でのご来場もお待ちしております。</p>
            </div>
          </div>
        </div><!-- ./p-exhibition--overview -->
        <div class="p-exhibition--notice">
          <h3 class="title-lv3">展示場からのお知らせ</h3>
          <div class="p-exhibition--notice-cnt">
            <h4 class="title-lv4">新型コロナウィルス感染予防対策について</h4>
            <p class="desc2">新型コロナウイルス感染予防対策として、会場内におきましては、アルコール消毒や従業員のマスク着用などの予防対策を実施しております。また、お客様にはご来場の際はマスクの着用や咳エチケット、手洗いや消毒、ソーシャルディスタンスの確保にご協力をお願いいたします。開催予定のイベントやセミナーに関しまして、状況に応じて予告なしに内容の変更や縮小、中止の場合がございます。 各モデルハウスの営業に関しましては、直接モデルハウスにお問い合わせください。</p>
          </div>
        </div><!-- ./p-exhibition--notice -->
        <div class="p-exhibition--news">
          <h3 class="title-lv3 type2">INFOMATION</h3>
          <ul class="p-exhibition--news-list">
            <li>
              <span class="date">2019.00.00</span>
              <a href="" class="link2"><span class="link-icon blank">モデルハウス見学はネット予約で！ネット予約見学プレゼントキャンペーン実施中！</span></a>
              <span class="new-label">NEW</span>
            </li>
            <li>
              <span class="date">2019.00.00</span>
              <a href="" class="link2"><span class="link-icon blank">LINE限定クーポンやイベント情報を配信！「友だち追加」でもれなくプレゼント！</span></a>
            </li>
            <li>
              <span class="date">2019.00.00</span>
              <a href="" class="link2"><span class="link-icon blank">無料設計図プレゼント！ご応募はこちらから！</span></a>
            </li>
            <li>
              <span class="date">2019.00.00</span>
              <a href="" class="link2"><span class="link-icon blank">ご存じですか？実は今おトクなんです！4つの住宅支援策を活用できます </span></a>
            </li>
          </ul>
        </div><!-- ./p-exhibition--news -->
        <div class="p-exhibition--pickup">
          <h3 class="title-lv3 type2">PICKUP</h3>
          <div class="p-exhibition--pickup-col2">
            <div class="p-exhibition--pickup-col2-item">
              <a class="link" href="">
                <img src="<?php echo $PATH;?>/assets/images/exhibition/banner02.png" alt="">
              </a>
            </div>
            <div class="p-exhibition--pickup-col2-item">
              <a class="link" href="">
                <img src="<?php echo $PATH;?>/assets/images/exhibition/banner01.png" alt="">
              </a>
            </div>
          </div>
          <div class="p-exhibition--pickup-col2">
            <div class="p-exhibition--pickup-col2-item">
              <a class="link" href="">
                <img src="<?php echo $PATH;?>/assets/images/exhibition/banner04.png" alt="">
              </a>
            </div>
            <!-- <div class="p-exhibition--pickup-col2-item">
              <a class="link" href="">
                <img src="<?php echo $PATH;?>/assets/images/exhibition/banner01.png" alt="">
              </a>
            </div> -->
          </div>
          <br>
          <br>
          <div class="p-exhibition--pickup-col1">
            <a class="link" href="">
              <img src="<?php echo $PATH;?>/assets/images/exhibition/banner02_2x.png" alt="">
            </a>
          </div>
          <div class="p-exhibition--pickup-col1">
            <a class="link" href="">
              <img src="<?php echo $PATH;?>/assets/images/exhibition/banner05.png" alt="">
            </a>
          </div>
          <div class="p-exhibition--pickup-col1">
            <a class="link" href="">
              <img src="<?php echo $PATH;?>/assets/images/exhibition/banner06.png" alt="">
            </a>
          </div>
        </div><!-- ./p-exhibition--pickup -->
        <div class="p-exhibition--direct">
          <div class="p-exhibition--direct-btn"><a href="" class="blue"><span>無料カタログ請求はこちら</span></a></div>
          <div class="p-exhibition--direct-btn"><a href="" class="orange"><span>見学のご予約はこちら</span></a></div>
          <div class="p-exhibition--direct-btn"><a href="" class="green"><span>電話・ オンライン相談はこちら</span></a></div>
        </div><!-- ./p-exhibition--direct -->
      </div>
      <div class="p-exhibition--anchorWrap">
        <ul class="p-exhibition--anchor">
          <li><a class="link" href="#model_houses">モデルハウス一覧</a></li>
          <li><a class="link" href="#events">イベント・相談会情報</a></li>
          <li><a class="link" href="#access">交通アクセス</a></li>
          <li><a class="link" href="#guide">会場案内図</a></li>
        </ul>
      </div><!-- ./p-exhibition--anchor -->
      <div class="p-exhibition--sectionList">
        <div class="p-exhibition--sectionList-ttl" id="model_houses">
          <div class="container">
            <h3 class="title-lv3">この展示場のモデルハウス一覧</h3>
          </div>
        </div>
        <div class="p-exhibition--sectionList-cnt">
          <div class="container">
            <p class="content-result--counter">公開中のモデルハウス:<span class="count">5</span><span class="unit">件</span></p>
            <p class="desc2 mgb-60">※モデルハウスにより定休日および営業時間が異なります。詳しくは各モデルハウスへお問い合わせ下さい。</p>
            <div class="p-exhibition--sectionList-item">
              <h4 class="p-exhibition--sectionList-item-label js-modelHouseAc active">
                <span class="jp">三井ホーム</span>
                LANGLEY
              </h4>
              <div class="p-exhibition--sectionList-item-inner js-modelHouseAcCnt">
                <div class="col2">
                  <div class="col2-item">
                    <p class="desc2 pc-only3">どこにいても家族を感じられる、自分らしく、心からリラックスできる、自然がもたらす心地よさを感じられる、そんな暮らしを実現しました。</p>
                    <p class="ttl-check">ポイント</p>
                    <ul class="list">
                      <li>温もりと上質を兼ねそなえた「ウッディーコンテンポラリーデザイン」</li>
                      <li>安らぎを感じられる「木の家」</li>
                      <li>自然の光や風を存分に感じられる気持ちのいい「半屋外空間」</li>
                    </ul>
                    <p class="desc2 sp-only3">どこにいても家族を感じられる、自分らしく、心からリラックスできる、自然がもたらす心地よさを感じられる、そんな暮らしを実現しました。</p>
                    <table class="table">
                      <tr>
                        <th>工法</th>
                        <td>木造枠組壁工法(プレミアム・モノコック構法)</td>
                      </tr>
                      <tr>
                        <th>お問い合わせ先</th>
                        <td>03-3247-3641</td>
                      </tr>
                    </table>
                    <div class="col2-item--tags sp-only3">
                      <span class="tag5">#システムキッチンがある</span>
                      <span class="tag5">#安心のセキュリティ</span>
                      <span class="tag5">#二重窓 #防音・断熱・通気性</span>
                      <span class="tag5">#IoT対応</span>
                      <span class="tag5">#アイランドキッチン</span>
                      <span class="tag5">#木のぬくもり</span>
                    </div>
                
                  </div>
                  <div class="col2-item">
                    <div class="subSlidersEP js-subSlidersEP">
                      <div class="subSlidersEP-item">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider05.jpg" alt="">
                      </div>
                      <div class="subSlidersEP-item">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider04.jpg" alt="">
                      </div>
                      <div class="subSlidersEP-item">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider01.png" alt="">
                      </div>
                      <div class="subSlidersEP-item">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider01.png" alt="">
                      </div>
                      <div class="subSlidersEP-item">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider01.png" alt="">
                      </div>
                    </div>
                    <div class="col2-item--tags pc-only3">
                      <span class="tag5">#システムキッチンがある</span>
                      <span class="tag5">#安心のセキュリティ</span>
                      <span class="tag5">#二重窓 #防音・断熱・通気性</span>
                      <span class="tag5">#IoT対応</span>
                      <span class="tag5">#アイランドキッチン</span>
                      <span class="tag5">#木のぬくもり</span>
                    </div>
                
                  </div>
                </div>
                <div class="list-categoryWrap">
                  <p class="ttl-cat">内観ギャラリー</p>
                  <ul class="list-category">
                    <li>
                      <a class="link" href="">
                        <div class="list-category-thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider04.jpg" alt="">
                        </div>
                        <p class="desc2">解放感あふれるウッディーコンテンポラリーデザインのリビング</p>
                      </a>
                    </li>
                    <li>
                      <a class="link" href="">
                        <div class="list-category-thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/category02.png" alt="">
                        </div>
                        <p class="desc2">半屋外空間のラナイ</p>
                      </a>
                    </li>
                    <li>
                      <a class="link" href="">
                        <div class="list-category-thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/category03.png" alt="">
                        </div>
                        <p class="desc2">キッチン脇の落ち着きのあるヌック</p>
                      </a>
                    </li>
                  </ul>
                  <div class="list-category-btn">
                    <a href="" class="btn-yellow"><span>見学予約に追加する</span></a>
                  </div>
                </div>
              </div>
            </div><!-- ./p-exhibition--sectionList-item -->
            <div class="p-exhibition--sectionList-item">
              <h4 class="p-exhibition--sectionList-item-label js-modelHouseAc active">
                <span class="jp">三井ホーム</span>
                LANGLEY
              </h4>
              <div class="p-exhibition--sectionList-item-inner js-modelHouseAcCnt">
                <div class="col2">
                  <div class="col2-item">
                    <p class="desc2 pc-only3">どこにいても家族を感じられる、自分らしく、心からリラックスできる、自然がもたらす心地よさを感じられる、そんな暮らしを実現しました。</p>
                    <p class="ttl-check">ポイント</p>
                    <ul class="list">
                      <li>温もりと上質を兼ねそなえた「ウッディーコンテンポラリーデザイン」</li>
                      <li>安らぎを感じられる「木の家」</li>
                      <li>自然の光や風を存分に感じられる気持ちのいい「半屋外空間」</li>
                    </ul>
                    <p class="desc2 sp-only3">どこにいても家族を感じられる、自分らしく、心からリラックスできる、自然がもたらす心地よさを感じられる、そんな暮らしを実現しました。</p>
                    <table class="table">
                      <tr>
                        <th>工法</th>
                        <td>木造枠組壁工法(プレミアム・モノコック構法)</td>
                      </tr>
                      <tr>
                        <th>お問い合わせ先</th>
                        <td>03-3247-3641</td>
                      </tr>
                    </table>
                    <div class="col2-item--tags sp-only3">
                      <span class="tag5">#システムキッチンがある</span>
                      <span class="tag5">#安心のセキュリティ</span>
                      <span class="tag5">#二重窓 #防音・断熱・通気性</span>
                      <span class="tag5">#IoT対応</span>
                      <span class="tag5">#アイランドキッチン</span>
                      <span class="tag5">#木のぬくもり</span>
                    </div>
                
                  </div>
                  <div class="col2-item">
                    <div class="subSlidersEP js-subSlidersEP">
                      <div class="subSlidersEP-item">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider05.jpg" alt="">
                      </div>
                      <div class="subSlidersEP-item">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider04.jpg" alt="">
                      </div>
                      <div class="subSlidersEP-item">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider01.png" alt="">
                      </div>
                      <div class="subSlidersEP-item">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider01.png" alt="">
                      </div>
                      <div class="subSlidersEP-item">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider01.png" alt="">
                      </div>
                    </div>
                    <div class="col2-item--tags pc-only3">
                      <span class="tag5">#システムキッチンがある</span>
                      <span class="tag5">#安心のセキュリティ</span>
                      <span class="tag5">#二重窓 #防音・断熱・通気性</span>
                      <span class="tag5">#IoT対応</span>
                      <span class="tag5">#アイランドキッチン</span>
                      <span class="tag5">#木のぬくもり</span>
                    </div>
                
                  </div>
                </div>
                <div class="list-categoryWrap">
                  <p class="ttl-cat">内観ギャラリー</p>
                  <ul class="list-category">
                    <li>
                      <a class="link" href="">
                        <div class="list-category-thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider04.jpg" alt="">
                        </div>
                        <p class="desc2">解放感あふれるウッディーコンテンポラリーデザインのリビング</p>
                      </a>
                    </li>
                    <li>
                      <a class="link" href="">
                        <div class="list-category-thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/category02.png" alt="">
                        </div>
                        <p class="desc2">半屋外空間のラナイ</p>
                      </a>
                    </li>
                    <li>
                      <a class="link" href="">
                        <div class="list-category-thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/category03.png" alt="">
                        </div>
                        <p class="desc2">キッチン脇の落ち着きのあるヌック</p>
                      </a>
                    </li>
                  </ul>
                  <div class="list-category-btn">
                    <a href="" class="btn-yellow"><span>見学予約に追加する</span></a>
                  </div>
                </div>
              </div>
            </div><!-- ./p-exhibition--sectionList-item -->
            <div class="p-exhibition--sectionList-item">
              <h4 class="p-exhibition--sectionList-item-label js-modelHouseAc active">
                <span class="jp">三井ホーム</span>
                LANGLEY
              </h4>
              <div class="p-exhibition--sectionList-item-inner js-modelHouseAcCnt">
                <div class="col2">
                  <div class="col2-item">
                    <p class="desc2 pc-only3">どこにいても家族を感じられる、自分らしく、心からリラックスできる、自然がもたらす心地よさを感じられる、そんな暮らしを実現しました。</p>
                    <p class="ttl-check">ポイント</p>
                    <ul class="list">
                      <li>温もりと上質を兼ねそなえた「ウッディーコンテンポラリーデザイン」</li>
                      <li>安らぎを感じられる「木の家」</li>
                      <li>自然の光や風を存分に感じられる気持ちのいい「半屋外空間」</li>
                    </ul>
                    <p class="desc2 sp-only3">どこにいても家族を感じられる、自分らしく、心からリラックスできる、自然がもたらす心地よさを感じられる、そんな暮らしを実現しました。</p>
                    <table class="table">
                      <tr>
                        <th>工法</th>
                        <td>木造枠組壁工法(プレミアム・モノコック構法)</td>
                      </tr>
                      <tr>
                        <th>お問い合わせ先</th>
                        <td>03-3247-3641</td>
                      </tr>
                    </table>
                    <div class="col2-item--tags sp-only3">
                      <span class="tag5">#システムキッチンがある</span>
                      <span class="tag5">#安心のセキュリティ</span>
                      <span class="tag5">#二重窓 #防音・断熱・通気性</span>
                      <span class="tag5">#IoT対応</span>
                      <span class="tag5">#アイランドキッチン</span>
                      <span class="tag5">#木のぬくもり</span>
                    </div>
                
                  </div>
                  <div class="col2-item">
                    <div class="subSlidersEP js-subSlidersEP">
                      <div class="subSlidersEP-item">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider05.jpg" alt="">
                      </div>
                      <div class="subSlidersEP-item">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider04.jpg" alt="">
                      </div>
                      <div class="subSlidersEP-item">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider01.png" alt="">
                      </div>
                      <div class="subSlidersEP-item">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider01.png" alt="">
                      </div>
                      <div class="subSlidersEP-item">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider01.png" alt="">
                      </div>
                    </div>
                    <div class="col2-item--tags pc-only3">
                      <span class="tag5">#システムキッチンがある</span>
                      <span class="tag5">#安心のセキュリティ</span>
                      <span class="tag5">#二重窓 #防音・断熱・通気性</span>
                      <span class="tag5">#IoT対応</span>
                      <span class="tag5">#アイランドキッチン</span>
                      <span class="tag5">#木のぬくもり</span>
                    </div>
                
                  </div>
                </div>
                <div class="list-categoryWrap">
                  <p class="ttl-cat">内観ギャラリー</p>
                  <ul class="list-category">
                    <li>
                      <a class="link" href="">
                        <div class="list-category-thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider04.jpg" alt="">
                        </div>
                        <p class="desc2">解放感あふれるウッディーコンテンポラリーデザインのリビング</p>
                      </a>
                    </li>
                    <li>
                      <a class="link" href="">
                        <div class="list-category-thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/category02.png" alt="">
                        </div>
                        <p class="desc2">半屋外空間のラナイ</p>
                      </a>
                    </li>
                    <li>
                      <a class="link" href="">
                        <div class="list-category-thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/category03.png" alt="">
                        </div>
                        <p class="desc2">キッチン脇の落ち着きのあるヌック</p>
                      </a>
                    </li>
                  </ul>
                  <div class="list-category-btn">
                    <a href="" class="btn-yellow"><span>見学予約に追加する</span></a>
                  </div>
                </div>
              </div>
            </div><!-- ./p-exhibition--sectionList-item -->
            <div class="p-exhibition--sectionList-item">
              <h4 class="p-exhibition--sectionList-item-label js-modelHouseAc active">
                <span class="jp">三井ホーム</span>
                LANGLEY
              </h4>
              <div class="p-exhibition--sectionList-item-inner js-modelHouseAcCnt">
                <div class="col2">
                  <div class="col2-item">
                    <p class="desc2 pc-only3">どこにいても家族を感じられる、自分らしく、心からリラックスできる、自然がもたらす心地よさを感じられる、そんな暮らしを実現しました。</p>
                    <p class="ttl-check">ポイント</p>
                    <ul class="list">
                      <li>温もりと上質を兼ねそなえた「ウッディーコンテンポラリーデザイン」</li>
                      <li>安らぎを感じられる「木の家」</li>
                      <li>自然の光や風を存分に感じられる気持ちのいい「半屋外空間」</li>
                    </ul>
                    <p class="desc2 sp-only3">どこにいても家族を感じられる、自分らしく、心からリラックスできる、自然がもたらす心地よさを感じられる、そんな暮らしを実現しました。</p>
                    <table class="table">
                      <tr>
                        <th>工法</th>
                        <td>木造枠組壁工法(プレミアム・モノコック構法)</td>
                      </tr>
                      <tr>
                        <th>お問い合わせ先</th>
                        <td>03-3247-3641</td>
                      </tr>
                    </table>
                    <div class="col2-item--tags sp-only3">
                      <span class="tag5">#システムキッチンがある</span>
                      <span class="tag5">#安心のセキュリティ</span>
                      <span class="tag5">#二重窓 #防音・断熱・通気性</span>
                      <span class="tag5">#IoT対応</span>
                      <span class="tag5">#アイランドキッチン</span>
                      <span class="tag5">#木のぬくもり</span>
                    </div>
                
                  </div>
                  <div class="col2-item">
                    <div class="subSlidersEP js-subSlidersEP">
                      <div class="subSlidersEP-item">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider05.jpg" alt="">
                      </div>
                      <div class="subSlidersEP-item">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider04.jpg" alt="">
                      </div>
                      <div class="subSlidersEP-item">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider01.png" alt="">
                      </div>
                      <div class="subSlidersEP-item">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider01.png" alt="">
                      </div>
                      <div class="subSlidersEP-item">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider01.png" alt="">
                      </div>
                    </div>
                    <div class="col2-item--tags pc-only3">
                      <span class="tag5">#システムキッチンがある</span>
                      <span class="tag5">#安心のセキュリティ</span>
                      <span class="tag5">#二重窓 #防音・断熱・通気性</span>
                      <span class="tag5">#IoT対応</span>
                      <span class="tag5">#アイランドキッチン</span>
                      <span class="tag5">#木のぬくもり</span>
                    </div>
                
                  </div>
                </div>
                <div class="list-categoryWrap">
                  <p class="ttl-cat">内観ギャラリー</p>
                  <ul class="list-category">
                    <li>
                      <a class="link" href="">
                        <div class="list-category-thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider04.jpg" alt="">
                        </div>
                        <p class="desc2">解放感あふれるウッディーコンテンポラリーデザインのリビング</p>
                      </a>
                    </li>
                    <li>
                      <a class="link" href="">
                        <div class="list-category-thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/category02.png" alt="">
                        </div>
                        <p class="desc2">半屋外空間のラナイ</p>
                      </a>
                    </li>
                    <li>
                      <a class="link" href="">
                        <div class="list-category-thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/category03.png" alt="">
                        </div>
                        <p class="desc2">キッチン脇の落ち着きのあるヌック</p>
                      </a>
                    </li>
                  </ul>
                  <div class="list-category-btn">
                    <a href="" class="btn-yellow"><span>見学予約に追加する</span></a>
                  </div>
                </div>
              </div>
            </div><!-- ./p-exhibition--sectionList-item -->
            <div class="p-exhibition--sectionList-item">
              <h4 class="p-exhibition--sectionList-item-label js-modelHouseAc active">
                <span class="jp">三井ホーム</span>
                LANGLEY
              </h4>
              <div class="p-exhibition--sectionList-item-inner js-modelHouseAcCnt">
                <div class="col2">
                  <div class="col2-item">
                    <p class="desc2 pc-only3">どこにいても家族を感じられる、自分らしく、心からリラックスできる、自然がもたらす心地よさを感じられる、そんな暮らしを実現しました。</p>
                    <p class="ttl-check">ポイント</p>
                    <ul class="list">
                      <li>温もりと上質を兼ねそなえた「ウッディーコンテンポラリーデザイン」</li>
                      <li>安らぎを感じられる「木の家」</li>
                      <li>自然の光や風を存分に感じられる気持ちのいい「半屋外空間」</li>
                    </ul>
                    <p class="desc2 sp-only3">どこにいても家族を感じられる、自分らしく、心からリラックスできる、自然がもたらす心地よさを感じられる、そんな暮らしを実現しました。</p>
                    <table class="table">
                      <tr>
                        <th>工法</th>
                        <td>木造枠組壁工法(プレミアム・モノコック構法)</td>
                      </tr>
                      <tr>
                        <th>お問い合わせ先</th>
                        <td>03-3247-3641</td>
                      </tr>
                    </table>
                    <div class="col2-item--tags sp-only3">
                      <span class="tag5">#システムキッチンがある</span>
                      <span class="tag5">#安心のセキュリティ</span>
                      <span class="tag5">#二重窓 #防音・断熱・通気性</span>
                      <span class="tag5">#IoT対応</span>
                      <span class="tag5">#アイランドキッチン</span>
                      <span class="tag5">#木のぬくもり</span>
                    </div>
                
                  </div>
                  <div class="col2-item">
                    <div class="subSlidersEP js-subSlidersEP">
                      <div class="subSlidersEP-item">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider05.jpg" alt="">
                      </div>
                      <div class="subSlidersEP-item">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider04.jpg" alt="">
                      </div>
                      <div class="subSlidersEP-item">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider01.png" alt="">
                      </div>
                      <div class="subSlidersEP-item">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider01.png" alt="">
                      </div>
                      <div class="subSlidersEP-item">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider01.png" alt="">
                      </div>
                    </div>
                    <div class="col2-item--tags pc-only3">
                      <span class="tag5">#システムキッチンがある</span>
                      <span class="tag5">#安心のセキュリティ</span>
                      <span class="tag5">#二重窓 #防音・断熱・通気性</span>
                      <span class="tag5">#IoT対応</span>
                      <span class="tag5">#アイランドキッチン</span>
                      <span class="tag5">#木のぬくもり</span>
                    </div>
                
                  </div>
                </div>
                <div class="list-categoryWrap">
                  <p class="ttl-cat">内観ギャラリー</p>
                  <ul class="list-category">
                    <li>
                      <a class="link" href="">
                        <div class="list-category-thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider04.jpg" alt="">
                        </div>
                        <p class="desc2">解放感あふれるウッディーコンテンポラリーデザインのリビング</p>
                      </a>
                    </li>
                    <li>
                      <a class="link" href="">
                        <div class="list-category-thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/category02.png" alt="">
                        </div>
                        <p class="desc2">半屋外空間のラナイ</p>
                      </a>
                    </li>
                    <li>
                      <a class="link" href="">
                        <div class="list-category-thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/category03.png" alt="">
                        </div>
                        <p class="desc2">キッチン脇の落ち着きのあるヌック</p>
                      </a>
                    </li>
                  </ul>
                  <div class="list-category-btn">
                    <a href="" class="btn-yellow"><span>見学予約に追加する</span></a>
                  </div>
                </div>
              </div>
            </div><!-- ./p-exhibition--sectionList-item -->
          </div>
        </div><!-- ./p-exhibition--sectionList-cnt -->
      </div><!-- ./p-exhibition--sectionList -->
      <div class="p-exhibition--sectionInfor" id="events">
        <div class="p-exhibition--sectionInfor-cnt">
          <div class="container">
            <h3 class="title-lv3">この展示場のイベント・相談会情報</h3>
          </div>
          <ul class="p-exhibition--sectionInfor-slider js-sectionInforSlider">
            <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/event/event05.png" alt="">
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3">イベント</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
            <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/event/event05.png" alt="">
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3">イベント</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
            <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/event/event06.png" alt="">
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3 type2">相談会</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
            <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/event/event05.png" alt="">
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3">イベント</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
            <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/event/event05.png" alt="">
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3">イベント</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
            <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/event/event06.png" alt="">
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3 type2">相談会</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
          </ul><!-- ./p-exhibition--sectionList-slider -->
          <p class="view-moreWrap align-center"><a href="" class="view-more">セミナー・イベント情報一覧を見る</a></p>
        </div>
      </div><!-- ./p-exhibition--sectionInfor -->
      <div class="p-exhibition--sectionAccess" id="access">
        <div class="container">
          <h3 class="title-lv3">交通アクセス</h3>
          <div class="p-exhibition--sectionAccess-cnt">
            <div class="p-exhibition--sectionAccess-map-iframe">
              <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12975.176605848541!2d139.511256!3d35.608144!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd28ae9dd5a068719!2zdHZr44OP44Km44K444Oz44Kw44OX44Op44K2IOaWsOeZvuWQiOODtuS4mA!5e0!3m2!1sja!2sjp!4v1615251654313!5m2!1sja!2sjp" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>
            <div class="p-exhibition--sectionAccess-map-link">
              <a href="https://goo.gl/maps/tWVVpXNbvboAqfUn6" target="_blank" class="link-icon blank">大きい地図で見る</a>
            </div>
            <div class="p-exhibition--sectionAccess-map-guide">
              <div class="p-exhibition--sectionAccess-map-infor">
                <p class="p-exhibition--sectionAccess-map-ttl">お車でお越しの方</p>
                <p class="p-exhibition--sectionAccess-map-des">東名高速道路 東名「川崎」ICから約25分<br>中央自動車道「稲城」ICから約20分</p>
              </div>
              <div class="p-exhibition--sectionAccess-map-infor">
                <p class="p-exhibition--sectionAccess-map-ttl">電車でお越しの方</p>
                <p class="p-exhibition--sectionAccess-map-des">小田急線「百合ヶ丘」駅 北口から徒歩6分<br>小田急線「新百合ヶ丘」駅 北口から徒歩10分</p>
              </div>
              
            </div>
          </div>
        </div>
      </div><!-- ./p-exhibition--sectionAccess -->
      <div class="p-exhibition--sectionGuide" id="guide">
        <div class="container">
          <h3 class="title-lv3">会場案内図</h3>
          <div class="p-exhibition--sectionGuide-cnt">
            <div class="p-exhibition--sectionGuide-cnt-img">
              <img src="<?php echo $PATH;?>/assets/images/exhibition/guide.png" alt="">
            </div>
            <ol class="p-exhibition--sectionGuide-cnt-detail">
              <li><span>1.</span>一条工務店 i-smart</li>
              <li><span>2.</span>日本ハウスHD(旧東日本ハウス)やまとシリーズ「館」</li>
              <li><span>3.</span>オープンハウス・アーキテクト ZEROENE PLAIN</li>
              <li><span>4.</span>アイ工務店 Ees(イエス)</li>
              <li><span>5.</span>桧家住宅 スマート・ワン・カスタム</li>
              <li><span>6.</span>一条工務店「夢の家」</li>
              <li><span>7.</span>アキュラホーム 住み心地のいい家</li>
              <li><span>8.</span>セキスイハイム デシオJX</li>
              <li><span>9.</span>住友不動産 新築そっくりさん</li>
              <li><span>10.</span>シルバニアホーム by古河林業 屋上庭園のある家</li>
              <li><span>11.</span>相陽建設 屋根に集う家</li>
              <li><span>12.</span>スウェーデンハウス グラン</li>
            </ol>
          </div>
        </div>
      </div><!-- ./p-exhibition--sectionGuide -->
      <div class="p-exhibition--sectionSuggest">
        <div class="container">
          <h3 class="title-lv3">同じエリアの展示場</h3>
          <ul class="list-result">
            <li>
              <a class="link" href="/exhibition/detail">
                <div class="list-result-thumb">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/subSlider05.jpg" alt="">
                </div>
                <div class="list-result-cnt">
                  <h4 class="list-result-cnt-ttl">Tvkハウジングプラザ新百合ヶ丘</h4>
                  <div class="list-result-cnt-table">
                    <table>
                      <tr>
                        <th>所在地</th>
                        <td>神奈川県秦野市平沢1381-1</td>
                      </tr>
                      <tr>
                        <th>電話番号</th>
                        <td>0463-82-6665</td>
                      </tr>
                      <tr>
                        <th>営業時間</th>
                        <td>10:00～18:00</td>
                      </tr>
                      <tr>
                        <th>展示棟数</th>
                        <td>全9棟</td>
                      </tr>
                    </table>
                  </div>
                  <span class="location">横浜北部・川崎エリア</span>
                </div>
              </a>
            </li>
            <li>
              <a class="link" href="/exhibition/detail">
                <div class="list-result-thumb">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/booking/booking02.png" alt="">
                </div>
                <div class="list-result-cnt">
                  <h4 class="list-result-cnt-ttl">Tvkハウジングプラザ新百合ヶ丘</h4>
                  <div class="list-result-cnt-table">
                    <table>
                      <tr>
                        <th>所在地</th>
                        <td>神奈川県秦野市平沢1381-1</td>
                      </tr>
                      <tr>
                        <th>電話番号</th>
                        <td>0463-82-6665</td>
                      </tr>
                      <tr>
                        <th>営業時間</th>
                        <td>10:00～18:00</td>
                      </tr>
                      <tr>
                        <th>展示棟数</th>
                        <td>全9棟</td>
                      </tr>
                    </table>
                  </div>
                  <span class="location">横浜北部・川崎エリア</span>
                </div>
              </a>
            </li>
            <li>
              <a class="link" href="/exhibition/detail">
                <div class="list-result-thumb">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/booking/booking03.png" alt="">
                </div>
                <div class="list-result-cnt">
                  <h4 class="list-result-cnt-ttl">Tvkハウジングプラザ新百合ヶ丘</h4>
                  <div class="list-result-cnt-table">
                    <table>
                      <tr>
                        <th>所在地</th>
                        <td>神奈川県秦野市平沢1381-1</td>
                      </tr>
                      <tr>
                        <th>電話番号</th>
                        <td>0463-82-6665</td>
                      </tr>
                      <tr>
                        <th>営業時間</th>
                        <td>10:00～18:00</td>
                      </tr>
                      <tr>
                        <th>展示棟数</th>
                        <td>全9棟</td>
                      </tr>
                    </table>
                  </div>
                  <span class="location">横浜北部・川崎エリア</span>
                </div>
              </a>
            </li>
            <li>
              <a class="link" href="/exhibition/detail">
                <div class="list-result-thumb">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/booking/booking04.png" alt="">
                </div>
                <div class="list-result-cnt">
                  <h4 class="list-result-cnt-ttl">Tvkハウジングプラザ新百合ヶ丘</h4>
                  <div class="list-result-cnt-table">
                    <table>
                      <tr>
                        <th>所在地</th>
                        <td>神奈川県秦野市平沢1381-1</td>
                      </tr>
                      <tr>
                        <th>電話番号</th>
                        <td>0463-82-6665</td>
                      </tr>
                      <tr>
                        <th>営業時間</th>
                        <td>10:00～18:00</td>
                      </tr>
                      <tr>
                        <th>展示棟数</th>
                        <td>全9棟</td>
                      </tr>
                    </table>
                  </div>
                  <span class="location">横浜北部・川崎エリア</span>
                </div>
              </a>
            </li>
            <li>
              <a class="link" href="/exhibition/detail">
                <div class="list-result-thumb">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/booking/booking05.png" alt="">
                </div>
                <div class="list-result-cnt">
                  <h4 class="list-result-cnt-ttl">Tvkハウジングプラザ新百合ヶ丘</h4>
                  <div class="list-result-cnt-table">
                    <table>
                      <tr>
                        <th>所在地</th>
                        <td>神奈川県秦野市平沢1381-1</td>
                      </tr>
                      <tr>
                        <th>電話番号</th>
                        <td>0463-82-6665</td>
                      </tr>
                      <tr>
                        <th>営業時間</th>
                        <td>10:00～18:00</td>
                      </tr>
                      <tr>
                        <th>展示棟数</th>
                        <td>全9棟</td>
                      </tr>
                    </table>
                  </div>
                  <span class="location">横浜北部・川崎エリア</span>
                </div>
              </a>
            </li>
            <li>
              <a class="link" href="/exhibition/detail">
                <div class="list-result-thumb">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/booking/booking06.png" alt="">
                </div>
                <div class="list-result-cnt">
                  <h4 class="list-result-cnt-ttl">Tvkハウジングプラザ新百合ヶ丘</h4>
                  <div class="list-result-cnt-table">
                    <table>
                      <tr>
                        <th>所在地</th>
                        <td>神奈川県秦野市平沢1381神奈川県秦野市平沢1381-1</td>
                      </tr>
                      <tr>
                        <th>電話番号</th>
                        <td>0463-82-6665</td>
                      </tr>
                      <tr>
                        <th>営業時間</th>
                        <td>10:00～18:00</td>
                      </tr>
                      <tr>
                        <th>展示棟数</th>
                        <td>全9棟</td>
                      </tr>
                    </table>
                  </div>
                  <span class="location">横浜北部・川崎エリア</span>
                </div>
              </a>
            </li>
          </ul><!-- ./list-result -->
          <p class="view-moreWrap align-center"><a href="" class="view-more">他の展示場を見る</a></p>
        </div>
      </div><!-- ./p-exhibition--sectionSuggest -->

      <div class="new__homeAddPx">
        <div class="new__homeAdd">
          <a href=""><span>来店のご予約はこちら</span></a>
        </div>
      </div>
      
    </div><!-- ./p-exhibition -->
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>