<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="ttl-epWrap">
    <div class="container">
      <h1 class="ttl-ep">展示場を探す</h1>
    </div>
  </div>
  <div class="breadcrumb">
    <div class="breadcrumb-inner">
      <ul>
        <li><a href="/"><span class="icon-home"></span></a></li>
        <li>展示場を探す</li>
      </ul>
    </div>
  </div><!-- ./breadcrumb -->
  <div class="p-end type2">
    <div class="container">
      <div class="p-exhibition">
        <div class="content-resultWrap">
          <p class="content-result--counter">該当の展示場:<span class="count">6</span><span class="unit">件</span></p>
          <div class="content-result--sidebar-condition sp-only3">
            <p class="content-result--sidebar-condition-ttl js-modelHouseAc">現在の検索条件</p>
            <ul class="js-modelHouseAcCnt">
              <li>横浜北部・川崎エリア</li>
              <li>川崎市</li>
              <li>横浜市(北部)</li>
              <li>横浜市(南部)</li>
              <li>葉山市</li>
              <li>横須賀市</li>
              <li>秦野市</li>
              <li>町田市(東京都)</li>
            </ul>
          </div>
          <div class="content-result">
            <div class="content-result--sidebar">
              <div class="content-result--sidebar-condition pc-only3">
                <p class="content-result--sidebar-condition-ttl js-modelHouseAc">現在の検索条件</p>
                <ul class="js-modelHouseAcCnt">
                  <li>横浜北部・川崎エリア</li>
                  <li>川崎市</li>
                  <li>横浜市(北部)</li>
                  <li>横浜市(南部)</li>
                  <li>葉山市</li>
                  <li>横須賀市</li>
                  <li>秦野市</li>
                  <li>町田市(東京都)</li>
                </ul>
              </div>
              <div class="form-area type2">
                <div class="form-area-labelWrap js-modelHouseAc">
                  <p class="form-area-label">エリアを選ぶ</p>
                </div>
                <form class="js-modelHouseAcCnt" action="">
                  <div class="form-area-cnt">
                    <div class="form-area-row">
                      <div class="form-area-row-inner">
                        <div class="form-area-ttl">
                          <input class="checkbox2" id="cbox01-1" type="checkbox" value="value1" checked="checked">
                          <label for="cbox01-1">横浜北部・川崎エリア</label>
                        </div>
                        <ul class="form-area-list">
                          <li>
                            <input class="checkbox2" id="cbox01-2" type="checkbox" value="value1" checked="checked">
                            <label for="cbox01-2">川崎市</label>
                          </li>
                          <li>
                            <input class="checkbox2" id="cbox01-3" type="checkbox" value="value1" checked="checked">
                            <label for="cbox01-3">横浜市(北部)</label>
                          </li>
                        </ul>
                      </div>
                    </div><!-- ./form-area-row -->
                    <div class="form-area-row">
                      <div class="form-area-row-inner">
                        <div class="form-area-ttl">
                          <input class="checkbox2" id="cbox02-1" type="checkbox" value="value1">
                          <label for="cbox02-1">横浜南部・横須賀エリア</label>
                        </div>
                        <ul class="form-area-list">
                          <li>
                            <input class="checkbox2" id="cbox02-2" type="checkbox" value="value1" checked="checked">
                            <label for="cbox02-2">横浜市(南部)</label>
                          </li>
                          <li>
                            <input class="checkbox2" id="cbox02-3" type="checkbox" value="value1">
                            <label for="cbox02-3">鎌倉市</label>
                          </li>
                          <li>
                            <input class="checkbox2" id="cbox02-4" type="checkbox" value="value1">
                            <label for="cbox02-4">逗子市</label>
                          </li>
                          <li>
                            <input class="checkbox2" id="cbox02-5" type="checkbox" value="value1" checked="checked">
                            <label for="cbox02-5">葉山市</label>
                          </li>
                          <li>
                            <input class="checkbox2" id="cbox02-6" type="checkbox" value="value1" checked="checked">
                            <label for="cbox02-6">横須賀市</label>
                          </li>
                        </ul>
                      </div>
                    </div><!-- ./form-area-row -->
                    <div class="form-area-row">
                      <div class="form-area-row-inner">
                        <div class="form-area-ttl">
                          <input class="checkbox2" id="cbox03-1" type="checkbox" value="value1">
                          <label for="cbox03-1">横浜南部・横須賀エリア</label>
                        </div>
                        <ul class="form-area-list">
                          <li>
                            <input class="checkbox2" id="cbox03-2" type="checkbox" value="value1">
                            <label for="cbox03-2">藤沢市</label>
                          </li>
                          <li>
                            <input class="checkbox2" id="cbox03-3" type="checkbox" value="value1">
                            <label for="cbox03-3">平塚市</label>
                          </li>
                        </ul>
                      </div>
                    </div><!-- ./form-area-row -->
                    <div class="form-area-row">
                      <div class="form-area-row-inner">
                        <div class="form-area-ttl">
                          <input class="checkbox2" id="cbox04-1" type="checkbox" value="value1">
                          <label for="cbox04-1">横浜南部・横須賀エリア</label>
                        </div>
                        <ul class="form-area-list">
                          <li>
                            <input class="checkbox2" id="cbox04-2" type="checkbox" value="value1">
                            <label for="cbox04-2">相模原市</label>
                          </li>
                          <li>
                            <input class="checkbox2" id="cbox04-3" type="checkbox" value="value1" checked="checked">
                            <label for="cbox04-3">秦野市</label>
                          </li>
                          <li>
                            <input class="checkbox2" id="cbox04-4" type="checkbox" value="value1">
                            <label for="cbox04-4">厚木市</label>
                          </li>
                          <li>
                            <input class="checkbox2" id="cbox04-5" type="checkbox" value="value1" checked="checked">
                            <label for="cbox04-5">町田市(東京都)</label>
                          </li>
                        </ul>
                      </div>
                    </div><!-- ./form-area-row -->
                  </div>
                  <div class="form-area-btn">
                    <a class="btn-reset" href=""><span>リセット</span></a>
                    <a class="btn-submit type2" href=""><span>再検索する</span></a>
                  </div>
                </form>
              </div>
            </div><!-- ./content-result--sidebar -->
            <div class="content-result--cnt">
              <ul class="list-result type2">
                <li>
                  <a class="link" href="/exhibition/detail">
                    <div class="list-result-thumb">
                      <img src="<?php echo $PATH;?>/assets/images/booking/booking01.png" alt="">
                    </div>
                    <div class="list-result-cnt">
                      <h4 class="list-result-cnt-ttl">Tvkハウジングプラザ新百合ヶ丘</h4>
                      <div class="list-result-cnt-table">
                        <table>
                          <tr>
                            <th>所在地</th>
                            <td>神奈川県秦野市平沢1381-1</td>
                          </tr>
                          <tr>
                            <th>電話番号</th>
                            <td>0463-82-6665</td>
                          </tr>
                          <tr>
                            <th>営業時間</th>
                            <td>10:00～18:00</td>
                          </tr>
                          <tr>
                            <th>展示棟数</th>
                            <td>全9棟</td>
                          </tr>
                        </table>
                      </div>
                      <span class="location">横浜北部・川崎エリア</span>
                    </div>
                  </a>
                </li>
                <li>
                  <a class="link" href="/exhibition/detail">
                    <div class="list-result-thumb">
                      <img src="<?php echo $PATH;?>/assets/images/booking/booking02.png" alt="">
                    </div>
                    <div class="list-result-cnt">
                      <h4 class="list-result-cnt-ttl">Tvkハウジングプラザ新百合ヶ丘</h4>
                      <div class="list-result-cnt-table">
                        <table>
                          <tr>
                            <th>所在地</th>
                            <td>神奈川県秦野市平沢1381-1</td>
                          </tr>
                          <tr>
                            <th>電話番号</th>
                            <td>0463-82-6665</td>
                          </tr>
                          <tr>
                            <th>営業時間</th>
                            <td>10:00～18:00</td>
                          </tr>
                          <tr>
                            <th>展示棟数</th>
                            <td>全9棟</td>
                          </tr>
                        </table>
                      </div>
                      <span class="location">横浜北部・川崎エリア</span>
                    </div>
                  </a>
                </li>
                <li>
                  <a class="link" href="/exhibition/detail">
                    <div class="list-result-thumb">
                      <img src="<?php echo $PATH;?>/assets/images/booking/booking03.png" alt="">
                    </div>
                    <div class="list-result-cnt">
                      <h4 class="list-result-cnt-ttl">Tvkハウジングプラザ新百合ヶ丘</h4>
                      <div class="list-result-cnt-table">
                        <table>
                          <tr>
                            <th>所在地</th>
                            <td>神奈川県秦野市平沢1381-1</td>
                          </tr>
                          <tr>
                            <th>電話番号</th>
                            <td>0463-82-6665</td>
                          </tr>
                          <tr>
                            <th>営業時間</th>
                            <td>10:00～18:00</td>
                          </tr>
                          <tr>
                            <th>展示棟数</th>
                            <td>全9棟</td>
                          </tr>
                        </table>
                      </div>
                      <span class="location">横浜北部・川崎エリア</span>
                    </div>
                  </a>
                </li>
                <li>
                  <a class="link" href="/exhibition/detail">
                    <div class="list-result-thumb">
                      <img src="<?php echo $PATH;?>/assets/images/booking/booking04.png" alt="">
                    </div>
                    <div class="list-result-cnt">
                      <h4 class="list-result-cnt-ttl">Tvkハウジングプラザ新百合ヶ丘</h4>
                      <div class="list-result-cnt-table">
                        <table>
                          <tr>
                            <th>所在地</th>
                            <td>神奈川県秦野市平沢1381-1</td>
                          </tr>
                          <tr>
                            <th>電話番号</th>
                            <td>0463-82-6665</td>
                          </tr>
                          <tr>
                            <th>営業時間</th>
                            <td>10:00～18:00</td>
                          </tr>
                          <tr>
                            <th>展示棟数</th>
                            <td>全9棟</td>
                          </tr>
                        </table>
                      </div>
                      <span class="location">横浜北部・川崎エリア</span>
                    </div>
                  </a>
                </li>
                <li>
                  <a class="link" href="/exhibition/detail">
                    <div class="list-result-thumb">
                      <img src="<?php echo $PATH;?>/assets/images/booking/booking05.png" alt="">
                    </div>
                    <div class="list-result-cnt">
                      <h4 class="list-result-cnt-ttl">Tvkハウジングプラザ新百合ヶ丘</h4>
                      <div class="list-result-cnt-table">
                        <table>
                          <tr>
                            <th>所在地</th>
                            <td>神奈川県秦野市平沢1381-1</td>
                          </tr>
                          <tr>
                            <th>電話番号</th>
                            <td>0463-82-6665</td>
                          </tr>
                          <tr>
                            <th>営業時間</th>
                            <td>10:00～18:00</td>
                          </tr>
                          <tr>
                            <th>展示棟数</th>
                            <td>全9棟</td>
                          </tr>
                        </table>
                      </div>
                      <span class="location">横浜北部・川崎エリア</span>
                    </div>
                  </a>
                </li>
                <li>
                  <a class="link" href="/exhibition/detail">
                    <div class="list-result-thumb">
                      <img src="<?php echo $PATH;?>/assets/images/booking/booking06.png" alt="">
                    </div>
                    <div class="list-result-cnt">
                      <h4 class="list-result-cnt-ttl">Tvkハウジングプラザ新百合ヶ丘</h4>
                      <div class="list-result-cnt-table">
                        <table>
                          <tr>
                            <th>所在地</th>
                            <td>神奈川県秦野市平沢1381神奈川県秦野市平沢1381-1</td>
                          </tr>
                          <tr>
                            <th>電話番号</th>
                            <td>0463-82-6665</td>
                          </tr>
                          <tr>
                            <th>営業時間</th>
                            <td>10:00～18:00</td>
                          </tr>
                          <tr>
                            <th>展示棟数</th>
                            <td>全9棟</td>
                          </tr>
                        </table>
                      </div>
                      <span class="location">横浜北部・川崎エリア</span>
                    </div>
                  </a>
                </li>
              </ul><!-- ./list-result -->
            </div>
          </div><!-- ./content-result -->
        </div><!-- ./content-resultWrap -->
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>