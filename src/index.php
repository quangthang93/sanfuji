<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="p-top">
    <div class="p-top--sliderWraper">
      <div class="p-top--slider js-sliderTop">
        <div class="p-top--slider-item type1">
          <a href="" class="p-top--slider-item-inner">
            <div class="p-top--slider-item-thumb type1">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/top/slider02.png" alt="">
            </div>
            <div class="p-top--slider-item-cnt type1">
              <p class="p-top--slider-item-time">JUNE 18 , 2019</p>
              <div class="p-top--slider-item-ttl">住宅ローンを利用して夢のマイホームを</div>
              <div class="p-top--slider-item-tag"><span class="cat2">コラム</span> <span class="space"></span> <span class="cat3"># タグ</span></div>
            </div>
          </a>
        </div><!-- ./p-top--item -->
        <div class="p-top--slider-item type1">
          <a href="" class="p-top--slider-item-inner">
            <div class="p-top--slider-item-thumb type1">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/top/slider03.png" alt="">
            </div>
            <div class="p-top--slider-item-cnt type1">
              <p class="p-top--slider-item-time">JUNE 18 , 2019</p>
              <div class="p-top--slider-item-ttl">住宅ローンを利用して夢のマイホームを</div>
              <div class="p-top--slider-item-tag"><span class="cat2">コラム</span> <span class="space"></span> <span class="cat3"># タグ</span></div>
            </div>
          </a>
        </div><!-- ./p-top--item -->
        <div class="p-top--slider-item type1">
          <a href="" class="p-top--slider-item-inner">
            <div class="p-top--slider-item-thumb type1">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/top/slider02.png" alt="">
            </div>
            <div class="p-top--slider-item-cnt type1">
              <p class="p-top--slider-item-time">JUNE 18 , 2019</p>
              <div class="p-top--slider-item-ttl">住宅ローンを利用して夢のマイホームを</div>
              <div class="p-top--slider-item-tag"><span class="cat2">コラム</span> <span class="space"></span> <span class="cat3"># タグ</span></div>
            </div>
          </a>
        </div><!-- ./p-top--item -->
      </div><!-- ./p-top--slider -->
    </div><!-- ./p-top--sliderWraper -->
    <div class="p-top--main">
      <div class="container2 fadeup">
        <div class="p-top--main-cnt">
          <div class="p-top--main-ttlWrap">
            <img class="img-flower" src="<?php echo $PATH;?>/assets/images/top/img-flower.png" alt="">
            <img class="img-house" src="<?php echo $PATH;?>/assets/images/top/img-house.png" alt="">
            <div class="section-ttlWrap">
              <h2 class="section-ttl">Live-rary</h2>
            </div>
            <div class="p-top--main-subTtlWrap">
              <p class="p-top--main-subTtl">〜「住む」こと、ぜんぶ。〜</p>
            </div>
          </div>
          <div class="p-top--main-pickup">
            <span class="p-top--main-pickup-label">PICK UP !</span>
            <a href="" class="p-top--main-pickup-inner">
              <div class="p-top--main-pickup-thumb">
                <img src="<?php echo $PATH;?>/assets/images/top/pickup.png" alt="">
              </div>
              <div class="p-top--main-pickup-cnt">
                <div class="p-top--main-pickup-tagWrap">
                  <div class="p-top--main-pickup-tag"><span class="cat2">コラム</span> <span class="space"></span> <span class="cat3"># タグ</span></div>
                  <p class="date sp-only3">JUNE 18 , 2019</p>
                </div>
                <h3 class="p-top--main-pickup-ttl">分譲住宅のメリット・デメリット</h3>
                <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                <p class="date pc-only3">JUNE 18 , 2019</p>
              </div>
            </a><!-- ./p-top--main-pickup-inner -->
          </div><!-- ./p-top--main-pickup -->
          <div class="p-top--main-posts">
            <ul>
              <li>
                <a href="">
                  <div class="p-top--main-posts-thumb">
                    <img src="<?php echo $PATH;?>/assets/images/top/post01.png" alt="">
                  </div>
                  <div class="p-top--main-posts-inforWrap">
                    <div class="p-top--main-posts-infor">
                      <div class="p-top--main-posts-tag"><span class="cat2 blue">知識</span> <span class="space pc-only4"></span> <span class="cat3 pc-only4"># タグ</span></div>
                      <p class="date">JUNE 18 , 2019</p>
                    </div>
                    <h3 class="p-top--main-posts-ttl">分譲住宅のメリット・デメリット</h3>
                    <p class="desc2 pc-only3">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                  </div>
                </a>
              </li>
              <li>
                <a href="">
                  <div class="p-top--main-posts-thumb">
                    <img src="<?php echo $PATH;?>/assets/images/top/post02.png" alt="">
                  </div>
                  <div class="p-top--main-posts-inforWrap">
                    <div class="p-top--main-posts-infor">
                      <div class="p-top--main-posts-tag"><span class="cat2 pink">知識</span> <span class="space pc-only4"></span> <span class="cat3 pc-only4"># タグ</span></div>
                      <p class="date">JUNE 18 , 2019</p>
                    </div>
                    <h3 class="p-top--main-posts-ttl">分譲住宅のメリット・デメリット</h3>
                    <p class="desc2 pc-only3">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                  </div>
                </a>
              </li>
              <li>
                <a href="">
                  <div class="p-top--main-posts-thumb">
                    <img src="<?php echo $PATH;?>/assets/images/top/post03.png" alt="">
                  </div>
                  <div class="p-top--main-posts-inforWrap">
                    <div class="p-top--main-posts-infor">
                      <div class="p-top--main-posts-tag"><span class="cat2">知識</span> <span class="space pc-only4"></span> <span class="cat3 pc-only4"># タグ</span></div>
                      <p class="date">JUNE 18 , 2019</p>
                    </div>
                    <h3 class="p-top--main-posts-ttl">分譲住宅のメリット・デメリット</h3>
                    <p class="desc2 pc-only3">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                  </div>
                </a>
              </li>
              <li>
                <a href="">
                  <div class="p-top--main-posts-thumb">
                    <img src="<?php echo $PATH;?>/assets/images/top/post04.png" alt="">
                  </div>
                  <div class="p-top--main-posts-inforWrap">
                    <div class="p-top--main-posts-infor">
                      <div class="p-top--main-posts-tag"><span class="cat2 blue">知識</span> <span class="space pc-only4"></span> <span class="cat3 pc-only4"># タグ</span></div>
                      <p class="date">JUNE 18 , 2019</p>
                    </div>
                    <h3 class="p-top--main-posts-ttl">分譲住宅のメリット・デメリット</h3>
                    <p class="desc2 pc-only3">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                  </div>
                </a>
              </li>
            </ul>
          </div><!-- ./p-top--main-posts -->
          <div class="p-top--main-tagList">
            <a href=""># ペットおもちゃ</a>
            <a href=""># 焼き物</a>
            <a href=""># 絵本</a>
            <a href=""># 簡単</a>
            <a href=""># スノードーム</a>
            <a href=""># 塗料　# 水耕栽培</a>
            <a href=""># テーブル</a>
            <a href=""># クリスマスリース</a>
            <a href=""># マリメッコ</a>
            <a href=""># 体重計</a>
            <a href=""># ラック</a>
            <a href=""># ペットおもちゃ</a>
            <a href=""># 焼き物</a>
            <a href=""># 絵本</a>
            <a href=""># 簡単</a>
            <a href=""># スノードーム</a>
            <a href=""># 塗料　# 水耕栽培</a>
            <a href=""># テーブル</a>
            <a href=""># クリスマスリース</a>
            <a href=""># マリメッコ</a>
            <a href=""># 体重計</a>
            <a href=""># ラック</a>
            <a href=""># ペットおもちゃ</a>
            <a href=""># 焼き物</a>
            <a href=""># 絵本</a>
            <a href=""># 簡単</a>
            <a href=""># スノードーム</a>
            <a href=""># 塗料　# 水耕栽培</a>
            <a href=""># テーブル</a>
            <a href=""># クリスマスリース</a>
            <a href=""># マリメッコ</a>
            <a href=""># 体重計</a>
            <a href=""># ラック</a>
          </div><!-- ./p-top--main-tagList -->
          <div class="btn-viewWrap">
            <a href="" class="btn-view3">もっと見る</a>
          </div>
        </div><!-- ./p-top--main-cnt -->
        <div class="p-top--main-sidebar pc-only">
          <h2 class="p-top--main-sidebar-label">RANKING</h2>
          <div class="p-top--main-sidebar-list">
            <ul>
              <li>
                <a href="">
                  <div class="p-top--main-sidebar-thumb">
                    <img src="<?php echo $PATH;?>/assets/images/top/post04.png" alt="">
                  </div>
                  <div class="p-top--main-sidebar-tag"><span class="cat2 blue">知識</span></div>
                  <h3 class="p-top--main-sidebar-ttl">住宅ローンを利用して夢のマイホームを</h3>
                  <p class="date">JUNE 18 , 2019</p>
                </a>
              </li>
              <li>
                <a href="">
                  <div class="p-top--main-sidebar-thumb">
                    <img src="<?php echo $PATH;?>/assets/images/top/post01.png" alt="">
                  </div>
                  <div class="p-top--main-sidebar-tag"><span class="cat2">コラム</span></div>
                  <h3 class="p-top--main-sidebar-ttl">住宅ローンを利用して夢のマイホームを</h3>
                  <p class="date">JUNE 18 , 2019</p>
                </a>
              </li>
              <li>
                <a href="">
                  <div class="p-top--main-sidebar-thumb">
                    <img src="<?php echo $PATH;?>/assets/images/top/post02.png" alt="">
                  </div>
                  <div class="p-top--main-sidebar-tag"><span class="cat2 pink">お役立ち</span></div>
                  <h3 class="p-top--main-sidebar-ttl">住宅ローンを利用して夢のマイホームを</h3>
                  <p class="date">JUNE 18 , 2019</p>
                </a>
              </li>
              <li>
                <a href="">
                  <div class="p-top--main-sidebar-thumb">
                    <img src="<?php echo $PATH;?>/assets/images/top/post03.png" alt="">
                  </div>
                  <div class="p-top--main-sidebar-tag"><span class="cat2 blue">知識</span></div>
                  <h3 class="p-top--main-sidebar-ttl">住宅ローンを利用して夢のマイホームを</h3>
                  <p class="date">JUNE 18 , 2019</p>
                </a>
              </li>
              <li>
                <a href="">
                  <div class="p-top--main-sidebar-thumb">
                    <img src="<?php echo $PATH;?>/assets/images/top/post04.png" alt="">
                  </div>
                  <div class="p-top--main-sidebar-tag"><span class="cat2">コラム</span></div>
                  <h3 class="p-top--main-sidebar-ttl">住宅ローンを利用して夢のマイホームを</h3>
                  <p class="date">JUNE 18 , 2019</p>
                </a>
              </li>
            </ul>
          </div><!-- ./p-top--main-sidebar-list -->
        </div><!-- ./p-top--main-sidebar -->
      </div>
    </div>
    <div class="section-event">
      <div class="container2 fadeup">
        <div class="section-ttl2Wrap">
          <h2 class="section-ttl2">イベント・相談会情報</h2>
          <div class="section-subTtlWrap">
            <p class="section-subTtl">週末のちょっと空いた時間に</p>
          </div>
        </div>
        <ul class="p-event--infor-tab">
          <li class="js-choice active">すべて</li>
          <li class="js-choice">横浜北部・川崎エリア</li>
          <li class="js-choice">横浜南部エリア</li>
          <li class="js-choice">湘南・西湘エリア</li>
          <li class="js-choice">相模原・県央エリア</li>
        </ul>
        <ul class="p-event--infor-list">
          <li class="p-event--infor-item">
            <a class="link" href="/event/detail">
              <div class="p-event--infor-item-thumb">
                <img src="<?php echo $PATH;?>/assets/images/event/event01.png" alt="">
              </div>
              <div class="p-event--infor-item-cnt">
                <div class="p-event--infor-item-tagWrap">
                  <span class="location">横浜南部エリア</span>
                  <span class="tag3">イベント</span>
                </div>
                <h3 class="p-event--infor-item-ttl">1組限定！平日モデルハウス見学ツアー</h3>
                <span class="cat4 pc-only3">ABCハウジング 新・川崎住宅公園</span>
                <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
              </div>
            </a>
          </li>
          <li class="p-event--infor-item">
            <a class="link" href="/event/detail">
              <div class="p-event--infor-item-thumb">
                <img src="<?php echo $PATH;?>/assets/images/event/event02.png" alt="">
              </div>
              <div class="p-event--infor-item-cnt">
                <div class="p-event--infor-item-tagWrap">
                  <span class="location">横浜南部エリア</span>
                  <span class="tag3">イベント</span>
                </div>
                <h3 class="p-event--infor-item-ttl">エネルギーは自給自足の時代へ！ダイワハウスの災害に備える家！</h3>
                <span class="cat4 pc-only3">ABCハウジング 新・川崎住宅公園</span>
                <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
              </div>
            </a>
          </li>
          <li class="p-event--infor-item">
            <a class="link" href="/event/detail">
              <div class="p-event--infor-item-thumb">
                <img src="<?php echo $PATH;?>/assets/images/event/event03.png" alt="">
              </div>
              <div class="p-event--infor-item-cnt">
                <div class="p-event--infor-item-tagWrap">
                  <span class="location">横浜南部エリア</span>
                  <span class="tag3 type2">相談会</span>
                </div>
                <h3 class="p-event--infor-item-ttl">1組限定！平日モデルハウス見学ツアー</h3>
                <span class="cat4 pc-only3">ABCハウジング 新・川崎住宅公園</span>
                <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
              </div>
            </a>
          </li>
        </ul>
        <div class="btn-viewWrap">
          <a href="" class="btn-view3">もっと見る</a>
        </div>
        <div class="p-event--search">
          <p class="p-event--search-ttl">EVENT SEARCH</p>
          <div class="p-event--search-form newFixEvenTop">
            <form action="">
              <div class="d-flex">
                <ul class="p-event--search-list">
                  <li>
                    <input class="checkbox2 type5 checkedAll" id="cbox01" type="checkbox" value="value1">
                    <label for="cbox01">すべて</label>
                  </li>
                  <li>
                    <input class="checkbox2 type5 checkSingle" id="cbox02" type="checkbox" value="value1">
                    <label for="cbox02">イベント</label>
                  </li>
                  <li>
                    <input class="checkbox2 type5 checkSingle" id="cbox03" type="checkbox" value="value1">
                    <label for="cbox03">相談会</label>
                  </li>
                  <li>
                    <input class="checkbox2 type5 checkSingle" id="cbox004" type="checkbox" value="value1">
                    <label for="cbox004">セミナー</label>
                  </li>
                </ul>
                <ul class="p-event--search-list">
                  <li>
                    <input class="checkbox2 type5 checkedAll02" id="cbox04" type="checkbox" value="value1">
                    <label for="cbox04">すべて</label>
                  </li>
                  <li>
                    <input class="checkbox2 type5 checkSingle02" id="cbox05" type="checkbox" value="value1">
                    <label for="cbox05">開催予定</label>
                  </li>
                  <li>
                    <input class="checkbox2 type5 checkSingle02" id="cbox06" type="checkbox" value="value1">
                    <label for="cbox06">開催中</label>
                  </li>
                  <li>
                    <input class="checkbox2 type5 checkSingle02" id="cbox07" type="checkbox" value="value1">
                    <label for="cbox07">終了</label>
                  </li>
                </ul>
              </div>
              <div class="p-event--search-fieldsWrap">
                <div class="p-event--search-fields">
                  <div class="p-event--search-fields-item item01">
                    <span>
                      <!-- <input readonly="" type="text" value="エリアを選択"> -->
                      <select class="select2" id="select01">
                        <option>横浜北部・川崎エリア</option>
                        <option>キーワード1</option>
                        <option>キーワード2</option>
                      </select>
                    </span>
                  </div>
                  <div class="p-event--search-fields-item item02">
                    <select class="select2" id="select01">
                      <option>会場を指定する</option>
                      <option>キーワード1</option>
                      <option>キーワード2</option>
                    </select>
                  </div>
                  <div class="p-event--search-fields-itemWrap">
                    <div class="p-event--search-fields-item item03 calendar"><span><input readonly="readonly" class="js-datepicker01" type="text" value="開催日時"></span></div>
                    <span class="p-event--search-fields-symbol">〜</span>
                    <div class="p-event--search-fields-item item04 calendar"><span><input readonly="" class="js-datepicker01" type="text" value="指定なし"></span></div>
                  </div>
                </div>
                <div class="p-event--search-btn">
                  <a class="btn-submit" href=""><span>この条件で検索する</span></a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div><!-- ./section-event -->
    <div class="section-search">
      <div class="container2 fadeup">
        <div class="section-ttl2Wrap">
          <h2 class="section-ttl2 type2">展示場を探す</h2>
          <div class="section-subTtlWrap">
            <p class="section-subTtl">お近くの展示場までお気軽に</p>
          </div>
        </div>
        <div class="section-search--form">
          <p class="section-search--form-label">エリアを選ぶ</p>
          <form action="">
            <div class="section-search--form-cnt">
              <div class="section-search--form-row">
                <div class="section-search--form-row-inner">
                  <div class="section-search--form-ttl">
                    <input class="checkbox2" id="cbox01-1" type="checkbox" value="value1">
                    <label for="cbox01-1">横浜北部・川崎エリア</label>
                  </div>
                  <ul class="section-search--form-list">
                    <li>
                      <input class="checkbox2" id="cbox01-2" type="checkbox" value="value1">
                      <label for="cbox01-2">川崎市</label>
                    </li>
                    <li>
                      <input class="checkbox2" id="cbox01-3" type="checkbox" value="value1">
                      <label for="cbox01-3">横浜市(北部)</label>
                    </li>
                  </ul>
                </div>
              </div><!-- ./section-search--form-row -->
              <div class="section-search--form-row">
                <div class="section-search--form-row-inner">
                  <div class="section-search--form-ttl">
                    <input class="checkbox2" id="cbox02-1" type="checkbox" value="value1">
                    <label for="cbox02-1">横浜南部・横須賀エリア</label>
                  </div>
                  <ul class="section-search--form-list">
                    <li>
                      <input class="checkbox2" id="cbox02-2" type="checkbox" value="value1">
                      <label for="cbox02-2">横浜市(南部)</label>
                    </li>
                    <li>
                      <input class="checkbox2" id="cbox02-3" type="checkbox" value="value1">
                      <label for="cbox02-3">鎌倉市</label>
                    </li>
                    <li>
                      <input class="checkbox2" id="cbox02-4" type="checkbox" value="value1">
                      <label for="cbox02-4">逗子市</label>
                    </li>
                    <li>
                      <input class="checkbox2" id="cbox02-5" type="checkbox" value="value1">
                      <label for="cbox02-5">葉山市</label>
                    </li>
                    <li>
                      <input class="checkbox2" id="cbox02-6" type="checkbox" value="value1">
                      <label for="cbox02-6">横須賀市</label>
                    </li>
                  </ul>
                </div>
              </div><!-- ./section-search--form-row -->
              <div class="section-search--form-row">
                <div class="section-search--form-row-inner">
                  <div class="section-search--form-ttl">
                    <input class="checkbox2" id="cbox03-1" type="checkbox" value="value1">
                    <label for="cbox03-1">横浜南部・横須賀エリア</label>
                  </div>
                  <ul class="section-search--form-list">
                    <li>
                      <input class="checkbox2" id="cbox03-2" type="checkbox" value="value1">
                      <label for="cbox03-2">藤沢市</label>
                    </li>
                    <li>
                      <input class="checkbox2" id="cbox03-3" type="checkbox" value="value1">
                      <label for="cbox03-3">平塚市</label>
                    </li>
                  </ul>
                </div>
              </div><!-- ./section-search--form-row -->
              <div class="section-search--form-row">
                <div class="section-search--form-row-inner">
                  <div class="section-search--form-ttl">
                    <input class="checkbox2" id="cbox04-1" type="checkbox" value="value1">
                    <label for="cbox04-1">横浜南部・横須賀エリア</label>
                  </div>
                  <ul class="section-search--form-list">
                    <li>
                      <input class="checkbox2" id="cbox04-2" type="checkbox" value="value1">
                      <label for="cbox04-2">相模原市</label>
                    </li>
                    <li>
                      <input class="checkbox2" id="cbox04-3" type="checkbox" value="value1">
                      <label for="cbox04-3">秦野市</label>
                    </li>
                    <li>
                      <input class="checkbox2" id="cbox04-4" type="checkbox" value="value1">
                      <label for="cbox04-4">厚木市</label>
                    </li>
                    <li>
                      <input class="checkbox2" id="cbox04-5" type="checkbox" value="value1">
                      <label for="cbox04-5">町田市(東京都)</label>
                    </li>
                  </ul>
                </div>
              </div><!-- ./section-search--form-row -->
            </div>
            <div class="section-search--btn">
              <a class="btn-submit type2" href=""><span>検索する</span></a>
              <a class="btn-reset" href=""><span>リセット</span></a>
            </div>
          </form>
        </div>
      </div>
    </div><!-- ./section-search -->
    <div class="section-search2 type2">
      <div class="container2 fadeup">
        <div class="section-ttl2Wrap">
          <h2 class="section-ttl2 type3">モデルハウスを探す</h2>
          <div class="section-subTtlWrap">
            <p class="section-subTtl">理想の住まいを見つけよう</p>
          </div>
        </div>
        <div class="section-search2--cnt">
          <div class="tabs">
            <div class="tabs-navWrapper">
              <ul class="tabs-nav js-tabsNav">
                <li class="tabs-item js-tabsItem active">ハウスメーカーから選ぶ</li>
                <li class="tabs-item js-tabsItem">条件から選ぶ</li>
              </ul>
            </div>
            <div class="tabs-cnt js-tabsCnt">
              <div class="tabs-panel type2 js-tabsPanel">
                <ul class="tabs2">
                  <li class="js-choice">あ</li>
                  <li class="js-choice">か</li>
                  <li class="js-choice">さ</li>
                  <li class="js-choice">た</li>
                  <li class="js-choice">な</li>
                  <li class="js-choice">は</li>
                  <li class="js-choice active">ま</li>
                  <li class="js-choice">や</li>
                  <li class="js-choice">ら</li>
                  <li class="js-choice">わ</li>
                </ul>
                <form action="">
                  <div class="checkbox-listWrap">
                    <ul class="checkbox-list">
                      <li>
                        <input class="checkbox2" id="cbox05-1" type="checkbox" value="value1">
                        <label for="cbox05-1">マツシタホーム</label>
                      </li>
                      <li>
                        <input class="checkbox2" id="cbox05-2" type="checkbox" value="value1">
                        <label for="cbox05-2">丸和住宅</label>
                      </li>
                      <li>
                        <input class="checkbox2" id="cbox05-3" type="checkbox" value="value1">
                        <label for="cbox05-3">マレアハウスデザイン</label>
                      </li>
                    
                      <li>
                        <input class="checkbox2" id="cbox05-4" type="checkbox" value="value1">
                        <label for="cbox05-4">ミサワホーム</label>
                      </li>
                      <li>
                        <input class="checkbox2" id="cbox05-5" type="checkbox" value="value1">
                        <label for="cbox05-5">三井ホーム</label>
                      </li>
                      <li>
                        <input class="checkbox2" id="cbox05-6" type="checkbox" value="value1">
                        <label for="cbox05-6">三菱地所ホーム</label>
                      </li>
                      <li>
                        <input class="checkbox2" id="cbox05-7" type="checkbox" value="value1">
                        <label for="cbox05-7">望月建業</label>
                      </li>
                      <li>
                        <input class="checkbox2" id="cbox05-8" type="checkbox" value="value1">
                        <label for="cbox05-8">もりぞう</label>
                      </li>
                    </ul>
                  </div>
                  <div class="section-search--btn">
                    <a class="btn-submit type2" href=""><span>検索する</span></a>
                    <a class="btn-reset" href=""><span>リセット</span></a>
                  </div>
                </form>
              </div>
              <div class="tabs-panel type2 js-tabsPanel">
                <form action="">
                  <div class="checkbox-listWrap">
                    <div class="checkbox-list--row">
                      <p class="checkbox-list--ttl">工法</p>
                      <ul class="checkbox-list type2">
                        <li>
                          <input class="checkbox2" id="cbox06-1" type="checkbox" value="value1">
                          <label for="cbox06-1">木造軸組工法</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox06-2" type="checkbox" value="value1">
                          <label for="cbox06-2">鉄骨系プレハブ工法</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox06-3" type="checkbox" value="value1">
                          <label for="cbox06-3">2×4工法</label>
                        </li>
                      
                        <li>
                          <input class="checkbox2" id="cbox06-4" type="checkbox" value="value1">
                          <label for="cbox06-4">2×6工法</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox06-5" type="checkbox" value="value1">
                          <label for="cbox06-5">鉄骨＋ALC工法</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox06-6" type="checkbox" value="value1">
                          <label for="cbox06-6">木質系プレハブ工法</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox06-7" type="checkbox" value="value1">
                          <label for="cbox06-7">コンクリート系プレハブ工法</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox06-8" type="checkbox" value="value1">
                          <label for="cbox06-8">ユニット系プレハブ工法</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox06-9" type="checkbox" value="value1">
                          <label for="cbox06-9">ユニット工法</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox06-10" type="checkbox" value="value1">
                          <label for="cbox06-10">その他</label>
                        </li>
                      </ul>
                    </div>
                    <div class="checkbox-list--row">
                      <p class="checkbox-list--ttl type2">仕様</p>
                      <ul class="checkbox-list type2">
                        <li>
                          <input class="checkbox2" id="cbox07-1" type="checkbox" value="value1">
                          <label for="cbox07-1">2階建て</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox07-2" type="checkbox" value="value1">
                          <label for="cbox07-2">3階建て以上</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox07-3" type="checkbox" value="value1">
                          <label for="cbox07-3">単世帯</label>
                        </li>
                      
                        <li>
                          <input class="checkbox2" id="cbox07-4" type="checkbox" value="value1">
                          <label for="cbox07-4">二世帯</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox07-5" type="checkbox" value="value1">
                          <label for="cbox07-5">長期優良住宅</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox07-6" type="checkbox" value="value1">
                          <label for="cbox07-6">ネットゼロエネルギーハウス</label>
                        </li>
                      </ul>
                    </div>
                    <div class="checkbox-list--row">
                      <p class="checkbox-list--ttl type3">こだわり</p>
                      <ul class="checkbox-list type2">
                        <li>
                          <input class="checkbox2" id="cbox08-1" type="checkbox" value="value1">
                          <label for="cbox08-1">システムキッチンがある</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-2" type="checkbox" value="value1">
                          <label for="cbox08-2">安心のセキュリティ</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-3" type="checkbox" value="value1">
                          <label for="cbox08-3">防音・断熱・通気性</label>
                        </li>
                      
                        <li>
                          <input class="checkbox2" id="cbox08-4" type="checkbox" value="value1">
                          <label for="cbox08-4">和室と洋室がある</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-5" type="checkbox" value="value1">
                          <label for="cbox08-5">書斎</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-6" type="checkbox" value="value1">
                          <label for="cbox08-6">収納スペース広々</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-7" type="checkbox" value="value1">
                          <label for="cbox08-7">カーポート設置可</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-8" type="checkbox" value="value1">
                          <label for="cbox08-8">吹き抜け空間</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-9" type="checkbox" value="value1">
                          <label for="cbox08-9">板張り天井</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-10" type="checkbox" value="value1">
                          <label for="cbox08-10">スキップフロア</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-11" type="checkbox" value="value1">
                          <label for="cbox08-11">IoT対応</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-12" type="checkbox" value="value1">
                          <label for="cbox08-12">シアタールーム</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-13" type="checkbox" value="value1">
                          <label for="cbox08-13">二重窓</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-14" type="checkbox" value="value1">
                          <label for="cbox08-14">アイランドキッチン</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-15" type="checkbox" value="value1">
                          <label for="cbox08-15">ホームエレベーター</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-16" type="checkbox" value="value1">
                          <label for="cbox08-16">地下収納</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-17" type="checkbox" value="value1">
                          <label for="cbox08-17">ウッドデッキ</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-18" type="checkbox" value="value1">
                          <label for="cbox08-18">太陽光発電</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-19" type="checkbox" value="value1">
                          <label for="cbox08-19">家庭用蓄電池</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-20" type="checkbox" value="value1">
                          <label for="cbox08-20">オール電化</label>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="section-search--btn">
                    <a class="btn-submit type2" href=""><span>検索する</span></a>
                    <a class="btn-reset" href=""><span>リセット</span></a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div><!-- ./section-event2 -->
    <div class="section-booking">
      <div class="container2 fadeup">
        <div class="section-booking--cnt">
          <div class="section-ttlWrap">
            <h2 class="section-ttl _home">モデルハウスを見学する</h2>
          </div>
          <p class="desc2">複数のハウスメーカーを選択して一括お申し込みいただけます。</p>
          <div class="btn-submitWrap">
            <a href="" class="btn-yellow2 type2">モデルハウス見学予約</a>
          </div>
        </div>
      </div>
    </div><!-- ./section-booking -->
    <div class="section-news">
      <div class="container2 fadeup">
        <div class="section-news--cnt">
          <div class="section-news--ttlWrap">
            <h2 class="section-news--ttl">お知らせ</h2>
            <div class="section-news--btn pc-only3">
              <a href="" class="btn-submit">一覧を見る</a>
            </div>
          </div>
          <div class="section-news--list">
            <ul>
              <li class="section-news--item">
                <a class="link" href="">
                  <div class="section-news--item-dateWrap">
                    <span class="date">2019.00.00</span>
                    <span class="tag">新着情報</span>
                  </div>
                  <p class="section-news--item-des">新着情報が掲載されます。</p>
                </a>
              </li>
              <li class="section-news--item">
                <a class="link" href="">
                  <div class="section-news--item-dateWrap">
                    <span class="date">2019.00.00</span>
                    <span class="tag">新着情報</span>
                  </div>
                  <p class="section-news--item-des">新着情報が掲載されます。</p>
                </a>
              </li>
              <li class="section-news--item">
                <a class="link" href="">
                  <div class="section-news--item-dateWrap">
                    <span class="date">2019.00.00</span>
                    <span class="tag">新着情報</span>
                  </div>
                  <p class="section-news--item-des">新着情報が掲載されます。</p>
                </a>
              </li>
              <li class="section-news--item">
                <a class="link" href="">
                  <div class="section-news--item-dateWrap">
                    <span class="date">2019.00.00</span>
                    <span class="tag">新着情報</span>
                  </div>
                  <p class="section-news--item-des">新着情報が掲載されます。</p>
                </a>
              </li>
              <li class="section-news--item">
                <a class="link" href="">
                  <div class="section-news--item-dateWrap">
                    <span class="date">2019.00.00</span>
                    <span class="tag">新着情報</span>
                  </div>
                  <p class="section-news--item-des">新着情報が掲載されます。</p>
                </a>
              </li>
            </ul>
          </div>
          <div class="section-news--btn sp-only3">
            <a href="" class="btn-submit">一覧を見る</a>
          </div>
        </div>
      </div>
    </div><!-- ./section-news -->
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>