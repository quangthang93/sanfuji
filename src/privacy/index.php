<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>

    <main class="main">
    	<div class="ttl-epWrap">
    		<div class="container">
	    		<h1 class="ttl-ep">プライバシーポリシー</h1>
    		</div>
    	</div>
      <div class="breadcrumb">
        <div class="breadcrumb-inner">
          <ul>
            <li><a href="/"><span class="icon-home"></span></a></li>
            <li>プライバシーポリシー</li>
          </ul>
        </div>
      </div><!-- ./breadcrumb -->
  		<div class="p-end p-privacy">
  				<div class="container">
  					<h2 class="p-privacy--ttl">個人情報保護方針</h2>
  					<p class="desc">弊社は、お客さまの個人情報(氏名、住所、電話番号、Eメールアドレス等)を大切に保護することを重大な責務として認識し、以下のとおり、個人情報保護に関する基本方針を定めております。</p>
  					<div class="p-privacy--list">
  						<div class="p-privacy--item">
  							<h3 class="p-privacy--item-ttl">法令規範の遵守および管理体制</h3>
  							<p class="p-privacy--item-cnt">弊社は個人情報に関して適用される法令、その他規範を遵守するとともに、個人情報を適切に管理するための責任と権限、手順、罰則等を規定した社内規定を定め、役員および従業員に周知徹底して、個人情報の取扱いを的確に実行し、さらなる改善に努めてまいります。</p>
  						</div>
  						<div class="p-privacy--item">
  							<h3 class="p-privacy--item-ttl">安全・管理</h3>
  							<p class="p-privacy--item-cnt">弊社は、お客さまから収集した個人情報については、情報の紛失、改ざん、漏洩を防止するため、社員に対する教育を実施し、合理的なセキュリティー対策を講じて、安全な環境下で取り扱うよう努めます。</p>
  						</div>
  						<div class="p-privacy--item">
  							<h3 class="p-privacy--item-ttl">個人情報の第三者への提供</h3>
  							<div class="p-privacy--item-cnt">
                  <p class="mgb-10">弊社は、お客さまから収集した個人情報は、次のいずれかに該当する場合を除き、第三者へ提供、開示しません。</p>
                  <ul class="list mgb-10">
                    <li>お客さまの同意がある場合</li>
                    <li>弊社の業務を遂行するために、弊社の委託先会社へ必要最小限の範囲で情報を提供する場合</li>
                    <li>お問い合わせ内容が、弊社の関係会社や代理店から回答させていただくことが適切と判断される場合</li>
                    <li>法令に基づく要求を受けた場合</li>
                  </ul>
                  <p>なお、お客さまから収集した個人情報を委託先会社、関係会社、代理店へ提供する場合には、適切な管理が行われるように努めます。</p>
                </div>
  						</div>
  						<div class="p-privacy--item">
  							<h3 class="p-privacy--item-ttl">取得、目的</h3>
  							<p class="p-privacy--item-cnt">弊社は、弊社の事業目的を遂行する範囲内でお客さまの個人情報を収集し、利用させて頂くことがあります。</p>
  						</div>
  						<div class="p-privacy--item">
  							<h3 class="p-privacy--item-ttl">苦情・相談、および開示訂正等の求めへの対応</h3>
  							<p class="p-privacy--item-cnt">弊社は、お客さまの個人情報について、お客さまご本人からの苦情・相談、および照会、修正、削除等を求められた場合には、弊社窓口にて適切に対応いたします。</p>
  						</div>
  						<div class="p-privacy--item">
  							<h3 class="p-privacy--item-ttl">クッキー(Cookies)の利用</h3>
  							<p class="p-privacy--item-cnt">弊社ウェブサイトの中には、お客さまがより便利に弊社ウェブサイト内のサービスをご利用いただくために、クッキー(Cookies)を使用する場合があります。クッキーは、個々の利用者を識別できるものではなく、お客さまのプライバシーを侵害するものではありません。お客さまは、ブラウザの設定により、クッキーの受け取りを拒否することも可能ですが、その場合でも弊社ウェブサイトの閲覧に支障をきたすことはありません。</p>
  						</div>
  						<div class="p-privacy--item">
  							<h3 class="p-privacy--item-ttl">お問い合わせ窓口</h3>
  							<p class="p-privacy--item-cnt">個人情報の取り扱いに関するお問い合わせは、下記の窓口までお願いいたします。<br><br> 株式会社サンフジ企画 個人情報保護推進担当 <br>〒151-0053 <br>東京都渋谷区代々木1-35-4 代々木クリスタルビル7F <br>TEL:03-3379-7171</p>
  						</div>
  					</div>
  				</div>
  		</div>
    </main><!-- ./main -->

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>