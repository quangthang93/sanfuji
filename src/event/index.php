<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="ttl-epWrap">
    <div class="container">
      <h1 class="ttl-ep">イベント・相談会情報</h1>
    </div>
  </div>
  <div class="breadcrumb">
  <div class="breadcrumb-inner">
    <ul>
      <li><a href="/"><span class="icon-home"></span></a></li>
      <li>イベント・相談会情報</li>
    </ul>
  </div>
</div><!-- ./breadcrumb -->
  <div class="p-event">
    <div class="p-event--sliderWraper">
      <div class="p-event--slider js-sliderEvent">
        <div class="p-event--slider-item">
          <div class="p-event--slider-item-inner">
            <span class="pickup-label">PICK UP !</span>
            <div class="p-event--slider-item-ttl">初めてのモデルハウス見学ミニセミナー＋モデルハウス見学ツアー</div>
            <div class="p-event--slider-item-tagWrap">
              <span class="location">横浜北部・川崎エリア</span>
              <span class="tag2">相談会</span>
            </div>
            <div class="p-event--slider-item-cntBox">
              <div class="p-event--slider-item-cnt">
                <p class="desc2 js-equaDesc">200文字くらいの簡単なイベント概要の説明が入ります。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミー。</p>
                <div class="p-event--slider-item-cnt-table">
                  <table>
                    <tr>
                      <th>会場</th>
                      <td>ABCハウジング 新・川崎住宅公園</td>
                    </tr>
                    <tr>
                      <th>開催予定日</th>
                      <td>2021.01.03(日) - 01.30(土)</td>
                    </tr>
                  </table>
                </div>
                <p class="p-event--slider-item-link">
                  <a class="link2" href="">このイベントを詳しく見る</a>
                </p>
              </div>
              <div class="p-event--slider-item-thumb">
                <img src="<?php echo $PATH;?>/assets/images/event/event09.png" alt="">
              </div>
            </div>
          </div>
        </div><!-- ./p-event--item -->
        <div class="p-event--slider-item">
          <div class="p-event--slider-item-inner">
            <span class="pickup-label">PICK UP !</span>
            <div class="p-event--slider-item-ttl">初めてのモデルハウス見学ミニセミナー＋モデルハウス見学ツアー</div>
            <div class="p-event--slider-item-tagWrap">
              <span class="location">横浜北部・川崎エリア</span>
              <span class="tag2">相談会</span>
            </div>
            <div class="p-event--slider-item-cntBox">
              <div class="p-event--slider-item-cnt">
                <p class="desc2 js-equaDesc">桜のリースづくりを楽しみながら、同時に社会貢献もできる！オリジナルチャームを付けることを通して、参加費の一部が日本赤十字社に寄付されます。</p>
                <div class="p-event--slider-item-cnt-table">
                  <table>
                    <tr>
                      <th>会場</th>
                      <td>ABCハウジング 新・川崎住宅公園</td>
                    </tr>
                    <tr>
                      <th>開催予定日</th>
                      <td>2021.01.03(日) - 01.30(土)</td>
                    </tr>
                  </table>
                </div>
                <p class="p-event--slider-item-link">
                  <a class="link2" href="">このイベントを詳しく見る</a>
                </p>
              </div>
              <div class="p-event--slider-item-thumb">
                <img src="<?php echo $PATH;?>/assets/images/event/event01.png" alt="">
              </div>
            </div>
          </div>
        </div><!-- ./p-event--item -->
        <div class="p-event--slider-item">
          <div class="p-event--slider-item-inner">
            <span class="pickup-label">PICK UP !</span>
            <div class="p-event--slider-item-ttl">初めてのモデルハウス見学ミニセミナー＋モデルハウス見学ツアー</div>
            <div class="p-event--slider-item-tagWrap">
              <span class="location">横浜北部・川崎エリア</span>
              <span class="tag2">相談会</span>
            </div>
            <div class="p-event--slider-item-cntBox">
              <div class="p-event--slider-item-cnt">
                <p class="desc2 js-equaDesc">桜のリースづくりを楽しみながら、同時に社会貢献もできる！オリジナルチャームを付けることを通して、参加費の一部が日本赤十字社に寄付されます。</p>
                <div class="p-event--slider-item-cnt-table">
                  <table>
                    <tr>
                      <th>会場</th>
                      <td>ABCハウジング 新・川崎住宅公園</td>
                    </tr>
                    <tr>
                      <th>開催予定日</th>
                      <td>2021.01.03(日) - 01.30(土)</td>
                    </tr>
                  </table>
                </div>
                <p class="p-event--slider-item-link">
                  <a class="link2" href="">このイベントを詳しく見る</a>
                </p>
              </div>
              <div class="p-event--slider-item-thumb">
                <img src="<?php echo $PATH;?>/assets/images/event/event01.png" alt="">
              </div>
            </div>
          </div>
        </div><!-- ./p-event--item -->
        <div class="p-event--slider-item">
          <div class="p-event--slider-item-inner">
            <span class="pickup-label">PICK UP !</span>
            <div class="p-event--slider-item-ttl">初めてのモデルハウス見学ミニセミナー＋モデルハウス見学ツアー</div>
            <div class="p-event--slider-item-tagWrap">
              <span class="location">横浜北部・川崎エリア</span>
              <span class="tag2">相談会</span>
            </div>
            <div class="p-event--slider-item-cntBox">
              <div class="p-event--slider-item-cnt">
                <p class="desc2 js-equaDesc">200文字くらいの簡単なイベント概要の説明が入ります。この文章はダミーです。</p>
                <div class="p-event--slider-item-cnt-table">
                  <table>
                    <tr>
                      <th>会場</th>
                      <td>ABCハウジング 新・川崎住宅公園</td>
                    </tr>
                    <tr>
                      <th>開催予定日</th>
                      <td>2021.01.03(日) - 01.30(土)</td>
                    </tr>
                  </table>
                </div>
                <p class="p-event--slider-item-link">
                  <a class="link2" href="">このイベントを詳しく見る</a>
                </p>
              </div>
              <div class="p-event--slider-item-thumb">
                <img src="<?php echo $PATH;?>/assets/images/event/event02.png" alt="">
              </div>
            </div>
          </div>
        </div><!-- ./p-event--item -->
        <div class="p-event--slider-item">
          <div class="p-event--slider-item-inner">
            <span class="pickup-label">PICK UP !</span>
            <div class="p-event--slider-item-ttl">初めてのモデルハウス見学ミニセミナー＋モデルハウス見学ツアー</div>
            <div class="p-event--slider-item-tagWrap">
              <span class="location">横浜北部・川崎エリア</span>
              <span class="tag2">相談会</span>
            </div>
            <div class="p-event--slider-item-cntBox">
              <div class="p-event--slider-item-cnt">
                <p class="desc2 js-equaDesc">200文字くらいの簡単なイベント概要の説明が入ります。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミー。</p>
                <div class="p-event--slider-item-cnt-table">
                  <table>
                    <tr>
                      <th>会場</th>
                      <td>ABCハウジング 新・川崎住宅公園</td>
                    </tr>
                    <tr>
                      <th>開催予定日</th>
                      <td>2021.01.03(日) - 01.30(土)</td>
                    </tr>
                  </table>
                </div>
                <p class="p-event--slider-item-link">
                  <a class="link2" href="">このイベントを詳しく見る</a>
                </p>
              </div>
              <div class="p-event--slider-item-thumb">
                <img src="<?php echo $PATH;?>/assets/images/event/event09.png" alt="">
              </div>
            </div>
          </div>
        </div><!-- ./p-event--item -->
        <div class="p-event--slider-item">
          <div class="p-event--slider-item-inner">
            <span class="pickup-label">PICK UP !</span>
            <div class="p-event--slider-item-ttl">初めてのモデルハウス見学ミニセミナー＋モデルハウス見学ツアー</div>
            <div class="p-event--slider-item-tagWrap">
              <span class="location">横浜北部・川崎エリア</span>
              <span class="tag2">相談会</span>
            </div>
            <div class="p-event--slider-item-cntBox">
              <div class="p-event--slider-item-cnt">
                <p class="desc2 js-equaDesc">桜のリースづくりを楽しみながら、同時に社会貢献もできる！オリジナルチャームを付けることを通して、参加費の一部が日本赤十字社に寄付されます。</p>
                <div class="p-event--slider-item-cnt-table">
                  <table>
                    <tr>
                      <th>会場</th>
                      <td>ABCハウジング 新・川崎住宅公園</td>
                    </tr>
                    <tr>
                      <th>開催予定日</th>
                      <td>2021.01.03(日) - 01.30(土)</td>
                    </tr>
                  </table>
                </div>
                <p class="p-event--slider-item-link">
                  <a class="link2" href="">このイベントを詳しく見る</a>
                </p>
              </div>
              <div class="p-event--slider-item-thumb">
                <img src="<?php echo $PATH;?>/assets/images/event/event01.png" alt="">
              </div>
            </div>
          </div>
        </div><!-- ./p-event--item -->
        <div class="p-event--slider-item">
          <div class="p-event--slider-item-inner">
            <span class="pickup-label">PICK UP !</span>
            <div class="p-event--slider-item-ttl">初めてのモデルハウス見学ミニセミナー＋モデルハウス見学ツアー</div>
            <div class="p-event--slider-item-tagWrap">
              <span class="location">横浜北部・川崎エリア</span>
              <span class="tag2">相談会</span>
            </div>
            <div class="p-event--slider-item-cntBox">
              <div class="p-event--slider-item-cnt">
                <p class="desc2 js-equaDesc">200文字くらいの簡単なイベント概要の説明が入ります。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミー。</p>
                <div class="p-event--slider-item-cnt-table">
                  <table>
                    <tr>
                      <th>会場</th>
                      <td>ABCハウジング 新・川崎住宅公園</td>
                    </tr>
                    <tr>
                      <th>開催予定日</th>
                      <td>2021.01.03(日) - 01.30(土)</td>
                    </tr>
                  </table>
                </div>
                <p class="p-event--slider-item-link">
                  <a class="link2" href="">このイベントを詳しく見る</a>
                </p>
              </div>
              <div class="p-event--slider-item-thumb">
                <img src="<?php echo $PATH;?>/assets/images/event/event02.png" alt="">
              </div>
            </div>
          </div>
        </div><!-- ./p-event--item -->
      </div><!-- ./p-event--slider -->
    </div><!-- ./p-event--sliderWraper -->
    <div class="p-event--cnt">
      <div class="container">
        <div class="p-event--search">
          <p class="p-event--search-ttl">EVENT SEARCH</p>
          <div class="p-event--search-form addNewFix newFixEvenTop ">
            <form action="">
              <div class="d-flex">
                <ul class="p-event--search-list">
                  <li>
                    <input class="checkbox2 type4 checkedAll" id="cbox01" type="checkbox" value="value1">
                    <label for="cbox01">すべて</label>
                  </li>
                  <li>
                    <input class="checkbox2 type4 checkSingle" id="cbox02" type="checkbox" value="value1">
                    <label for="cbox02">イベント</label>
                  </li>
                  <li>
                    <input class="checkbox2 type4 checkSingle" id="cbox03" type="checkbox" value="value1">
                    <label for="cbox03">相談会</label>
                  </li>
                  <li>
                    <input class="checkbox2 type4 checkSingle" id="cbox004" type="checkbox" value="value1">
                    <label for="cbox004">セミナー</label>
                  </li>
                </ul>
                <ul class="p-event--search-list">
                  <li>
                    <input class="checkbox2 type4 checkedAll02" id="cbox04" type="checkbox" value="value1">
                    <label for="cbox04">すべて</label>
                  </li>
                  <li>
                    <input class="checkbox2 type4 checkSingle02" id="cbox05" type="checkbox" value="value1">
                    <label for="cbox05">開催予定</label>
                  </li>
                  <li>
                    <input class="checkbox2 type4 checkSingle02" id="cbox06" type="checkbox" value="value1">
                    <label for="cbox06">開催中</label>
                  </li>
                  <li>
                    <input class="checkbox2 type4 checkSingle02" id="cbox07" type="checkbox" value="value1">
                    <label for="cbox07">終了</label>
                  </li>
                </ul>
              </div>
              <div class="p-event--search-fields">
                <div class="p-event--search-fields-item item01">
                  <span>
                    <!-- <input type="text" value="横浜北部・川崎エリア"> -->
                    <select class="select2" id="select01">
                      <option>エリアを指定する</option>
                      <option>キーワード1</option>
                      <option>キーワード2</option>
                    </select>
                  </span>
                </div>
                <!-- add news label 20210531 --> 
                <div class="p-event--search-fields-item item02">
                  <select class="select2" id="select01">
                    <option>会場を指定する</option>
                    <option>キーワード1</option>
                    <option>キーワード2</option>
                  </select>
                </div>
                <div class="p-event--search-fields-itemWrap">
                  <div class="p-event--search-fields-item calendar"><span><input readonly="readonly" class="js-datepicker01" type="text" value="指定なし"></span></div>
                  <span class="p-event--search-fields-symbol">〜</span>
                  <div class="p-event--search-fields-item calendar"><span><input readonly class="js-datepicker01" type="text" value="指定なし"></span></div>
                </div>
              </div>
              <div class="p-event--search-btn">
                <a class="btn-submit" href=""><span>この条件で検索する</span></a>
              </div>
            </form>
          </div>
        </div><!-- ./p-event--search -->
        <div class="p-event--infor">
          <h2 class="p-event--infor-ttl">新着イベント・相談会情報</h2>
          <ul class="p-event--infor-tab">
            <li class="js-choice active">すべて</li>
            <li class="js-choice">横浜北部・川崎エリア</li>
            <li class="js-choice">横浜南部エリア</li>
            <li class="js-choice">湘南・西湘エリア</li>
            <li class="js-choice">相模原・県央エリア</li>
          </ul>
          <ul class="p-event--infor-list">
            <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <span class="p-event--infor-item-thumb-label">開催中</span>
                  <img src="<?php echo $PATH;?>/assets/images/event/event01.png" alt="">
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3 type3">セミナー</span>
                    <span class="tag3">イベント</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
             <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <span class="p-event--infor-item-thumb-label type2">終了</span>
                  <img src="<?php echo $PATH;?>/assets/images/event/event02.png" alt="">
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3">イベント</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
             <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <span class="p-event--infor-item-thumb-label type3">開催予定</span>
                  <img src="<?php echo $PATH;?>/assets/images/event/event03.png" alt="">
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3 type2">相談会</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
             <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/event/event04.png" alt="">
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3">イベント</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
            <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/event/event05.png" alt="">
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3">イベント</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
            <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/event/event06.png" alt="">
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3">イベント</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
            <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/event/event07.png" alt="">
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3">イベント</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
            <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/event/event08.png" alt="">
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3">イベント</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
            <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/event/event09.png" alt="">
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3">イベント</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
            <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/event/event01.png" alt="">
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3">イベント</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
            <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/event/event02.png" alt="">
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3">イベント</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
            <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/event/event03.png" alt="">
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3">イベント</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
          </ul>
          <div class="pagination2">
            <div class="pagination2-list">
              <a class="ctrl prev" href="">
                <svg xmlns="http://www.w3.org/2000/svg" width="6.121" height="9.414" viewBox="0 0 6.121 9.414">
                  <g id="_1" data-name=" 1" transform="translate(1267.414 519.938) rotate(180)">
                    <path class="a" id="_1" data-name="&gt;" d="M814.332,1939.086l4,4-4,4" transform="translate(447.668 -1427.855)" fill="none" stroke="#fff" stroke-width="2" />
                  </g>
                </svg>
              </a>
              <a href="">1</a>
              <a href="">2</a>
              <a class="active" href="">3</a>
              <a href="">4</a>
              <a href="">5</a>
              <a class="ctrl next" href="">
                <svg xmlns="http://www.w3.org/2000/svg" width="6.121" height="9.414" viewBox="0 0 6.121 9.414">
                  <g id="シンボル_82" data-name="シンボル 82" transform="translate(-1259.293 -510.062)">
                    <path class="a" id="_" data-name="&gt;" d="M814.332,1939.086l4,4-4,4" transform="translate(445.668 -1428.316)" fill="none" stroke="#fff" stroke-width="2" />
                  </g>
                </svg>
              </a>
            </div>
          </div>
        </div>
      </div><!-- ./container -->
    </div><!-- ./p-event--cnt -->
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>