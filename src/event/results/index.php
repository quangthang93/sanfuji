<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="ttl-epWrap">
    <div class="container">
      <h1 class="ttl-ep">イベント・相談会情報</h1>
    </div>
  </div>
  <div class="breadcrumb">
    <div class="breadcrumb-inner">
      <ul>
        <li><a href="/"><span class="icon-home"></span></a></li>
        <li>イベント・相談会情報</li>
      </ul>
    </div>
  </div><!-- ./breadcrumb -->
  <div class="p-event">
    <div class="p-event--cnt">
      <div class="container">
        <div class="p-event--search type2 js-modalSearch">
          <div class="p-event--search-inner">
            <span class="p-event--search-inner-close js-closeModalSearch"></span>
            <p class="p-event--search-ttl">EVENT SEARCH</p>
            <div class="p-event--search-form newFixEvenTop">
              <form action="">
                <div class="p-event--search-form-row">
                  <ul class="p-event--search-list">
                    <li>
                      <input class="checkbox2 type4 white checkedAll" id="cbox01" type="checkbox" value="value1">
                      <label for="cbox01">すべて</label>
                    </li>
                    <li>
                      <input class="checkbox2 type4 white checkSingle" id="cbox02" type="checkbox" value="value1">
                      <label for="cbox02">イベント</label>
                    </li>
                    <li>
                      <input class="checkbox2 type4 white checkSingle" id="cbox004" type="checkbox" value="value1">
                      <label for="cbox004">セミナー</label>
                    </li>
                    <li>
                      <input class="checkbox2 type4 white checkSingle" id="cbox03" type="checkbox" value="value1">
                      <label for="cbox03">相談会</label>
                    </li>
                  </ul>
                  <div class="p-event--search-fields-itemWrap">
                    <div class="p-event--search-fields-item item01">
                      <span>
                        <select class="select2" id="select01">
                          <option>エリアを指定する</option>
                          <option>キーワード1</option>
                          <option>キーワード2</option>
                        </select>
                      </span>
                    </div>
                    <div class="p-event--search-fields-item item05">
                      <select class="select2" id="select01">
                        <option>会場を指定する</option>
                        <option>キーワード1</option>
                        <option>キーワード2</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="p-event--search-form-row">
                  <ul class="p-event--search-list">
                    <li>
                      <input class="checkbox2 type4 white checkedAll02" id="cbox04" type="checkbox" value="value1">
                      <label for="cbox04">すべて</label>
                    </li>
                    <li>
                      <input class="checkbox2 type4 white checkSingle02" id="cbox05" type="checkbox" value="value1">
                      <label for="cbox05">開催予定</label>
                    </li>
                    <li>
                      <input class="checkbox2 type4 white checkSingle02" id="cbox06" type="checkbox" value="value1">
                      <label for="cbox06">開催中</label>
                    </li>
                    <li>
                      <input class="checkbox2 type4 white checkSingle02" id="cbox07" type="checkbox" value="value1">
                      <label for="cbox07">終了</label>
                    </li>
                  </ul>
                  <div class="p-event--search-fields-itemWrap">
                    <div class="p-event--search-fields-item item06 calendar"><span><input readonly="readonly" class="js-datepicker01" type="text" value="指定なし"></span></div>
                    <span class="p-event--search-fields-symbol">〜</span>
                    <div class="p-event--search-fields-item item06 calendar"><span><input readonly class="js-datepicker01" type="text" value="指定なし"></span></div>
                  </div>
                </div>
                <div class="p-event--search-btn">
                  <a class="btn-submit" href=""><span>この条件で検索する</span></a>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- ./p-event--search -->
        <div class="p-event--searchBox">
          <div class="p-event--searchBox-infor">
            <table>
              <tr>
                <th class="area">エリア</th>
                <td>横浜北部・川崎エリア</td>
              </tr>
              <tr>
                <th class="map">会場</th>
                <td>新百合ヶ丘ハウジングギャラリー</td>
              </tr>
              <tr>
                <th class="time">開催日時</th>
                <td>
                  <div class="Adgxe">
                    2021/02/01 ～ 指定なし
                    <ul class="d-flex">
                      <li class="addStant">ステータス</li>
                      <li>開催中</li>
                    </ul>
                  </div>
                </td>
              </tr>
              <tr>
                <th class="name">種類</th>
                <td>イベント</td>
              </tr>
            </table>
          </div>
          <div class="p-event--searchBox-btn">
            <a class="btn-submit js-openModalSearch" href="javascript:void(0)"><span>条件を変更する</span></a>
          </div>
        </div>
        <div class="p-event--infor">
          <p class="p-event--counter">85<span>件</span></p>
          <ul class="p-event--infor-list">
            <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <span class="p-event--infor-item-thumb-label">開催中</span>
                  <img src="<?php echo $PATH;?>/assets/images/event/event01.png" alt="">
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3">イベント</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
            <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <span class="p-event--infor-item-thumb-label type2">終了</span>
                  <img src="<?php echo $PATH;?>/assets/images/event/event02.png" alt="">
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3">イベント</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
            <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <span class="p-event--infor-item-thumb-label type3">開催予定</span>
                  <img src="<?php echo $PATH;?>/assets/images/event/event03.png" alt="">
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3 type2">相談会</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
            <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/event/event04.png" alt="">
                  <span class="event private">開催予定</span>
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3">イベント</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
            <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/event/event05.png" alt="">
                  <span class="event puplic">開催中</span>
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3">イベント</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
            <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/event/event06.png" alt="">
                  <span class="event finished">終了</span>
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3">イベント</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
            <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/event/event07.png" alt="">
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3">イベント</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
            <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/event/event08.png" alt="">
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3">イベント</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
            <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/event/event09.png" alt="">
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3">イベント</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
            <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/event/event01.png" alt="">
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3">イベント</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
            <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/event/event02.png" alt="">
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3">イベント</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
            <li class="p-event--infor-item">
              <a class="link" href="/event/detail">
                <div class="p-event--infor-item-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/event/event03.png" alt="">
                </div>
                <div class="p-event--infor-item-cnt">
                  <div class="p-event--infor-item-tagWrap">
                    <span class="location">横浜南部エリア</span>
                    <span class="tag3">イベント</span>
                  </div>
                  <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                  <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                  <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                </div>
              </a>
            </li>
          </ul>
          <div class="pagination2">
            <div class="pagination2-list">
              <a class="ctrl prev" href="">
                <svg xmlns="http://www.w3.org/2000/svg" width="6.121" height="9.414" viewBox="0 0 6.121 9.414">
                  <g id="_1" data-name=" 1" transform="translate(1267.414 519.938) rotate(180)">
                    <path class="a" id="_1" data-name="&gt;" d="M814.332,1939.086l4,4-4,4" transform="translate(447.668 -1427.855)" fill="none" stroke="#fff" stroke-width="2" />
                  </g>
                </svg>
              </a>
              <a href="">1</a>
              <a href="">2</a>
              <a class="active" href="">3</a>
              <a href="">4</a>
              <a href="">5</a>
              <a class="ctrl next" href="">
                <svg xmlns="http://www.w3.org/2000/svg" width="6.121" height="9.414" viewBox="0 0 6.121 9.414">
                  <g id="シンボル_82" data-name="シンボル 82" transform="translate(-1259.293 -510.062)">
                    <path class="a" id="_" data-name="&gt;" d="M814.332,1939.086l4,4-4,4" transform="translate(445.668 -1428.316)" fill="none" stroke="#fff" stroke-width="2" />
                  </g>
                </svg>
              </a>
            </div>
          </div>
        </div>
      </div><!-- ./container -->
    </div><!-- ./p-event--cnt -->
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>