<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="ttl-epWrap">
    <div class="container">
      <h1 class="ttl-ep">イベント・相談会情報</h1>
    </div>
  </div>
  <div class="breadcrumb">
  <div class="breadcrumb-inner">
    <ul>
      <li><a href="/"><span class="icon-home"></span></a></li>
      <li><a href="/">イベント・相談会情報</a></li>
      <li>新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</li>
    </ul>
  </div>
</div><!-- ./breadcrumb -->
  <div class="p-event">
    <div class="p-event--detail">
      <div class="container">
        <div class="p-event--detail-tag">
          <span class="tag3 type2">相談会</span>
        </div>
        <h2 class="title-lv2">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h2>
        <div class="p-event--detail-location">
          <span class="location">湘南・西湘エリア</span>
          <p class="date3">2021年02月27日(土)～03月06日(土) 11:30～14:00</p>
        </div>
        <div class="p-event--detail-top">
          <div class="p-event--detail-top-detail">
            <h3 class="title-lv3">イベント・相談会概要</h3>
            <table class="table">
              <tr>
                <th>開催場所</th>
                <td>
                  <a href="" class="link-icon arrow">新百合ヶ丘ハウジングギャラリー</a>
                  <p>〒215-0004<br>神奈川県川崎市麻生区万福寺2-2-1</p>
                </td>
              </tr>
              <tr>
                <th>開催予定日</th>
                <td>2021年02月27日(土)～2021年03月06日(土)</td>
              </tr>
              <tr>
                <th>開催時間</th>
                <td>【月～金】10:00　～　18:00 <br>【土日祝】9:30　～　18:30<br>※最終日は17:30(最終来場17:00)閉場になります。</td>
              </tr>
              <tr>
                <th>備考</th>
                <td>駐車場の収容数には限りがございますので、 <br>お車でお越しの方は、あらかじめご来場のご予約を<br>お願いいたします。</td>
              </tr>
            </table>
          </div>
          <div class="p-event--detail-top-thumb">
            <img src="<?php echo $PATH;?>/assets/images/event/event10.png" alt="">
          </div>
        </div><!-- ./p-event--detail-top -->
        <div class="p-event--detail-body">
          <div class="p-event--detail-body-summary">
            <h3 class="title-lv3 mgb-30">イベント・相談会内容</h3>
            <p class="desc2">200文字くらいの簡単なイベント概要の説明が入ります。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミー。</p>
          </div>
          <div class="p-event--detail-body-item">
            <h4 class="title-lv4 type2 mgb-10">住まいづくり安心サポート相談員によるアドバイス。</h4>
            <p class="desc2 mgb-10">最初のお悩み相談から工事会社選びまで、住まいづくり安心サポート相談員が新設丁寧にアドバイスいたします。 <br>「何から始めたらいいか？」「建替えとリフォームどちらがいいか」「土地や中古住宅の選び方」など、どんなご質問でも大丈夫です。</p>
            <p class="desc2 mgb-10">初めてご相談される方でも安心できるようどんなご相談もお応えいたします。</p>
            <p class="desc2">また、新築・リフォームをご検討する際にあらかじめ知っておいた方や、お困りごとの解決の糸口、施工業者の選び方など、自社で培ったノウハウと知識で、お客様それぞれにあったベストを導き出します。その他、家づくりに関するどんなご相談ごとでも対応いたします。</p>
          </div>
          <div class="p-event--detail-body-item">
            <h4 class="title-lv4 type2 mgb-10">LINE相談はじめました！</h4>
            <p class="desc2 mgb-20">LINEでもご相談いただけるようになりました。 <br>ご自宅からゆっくりとご相談ください。</p>
            <p class="desc2 mgb-10">LINEでのご相談は下記のID、又はQRコードより友だち追加をしてください。<br> ID:housquare_soudan</p>
            <div class="p-event--detail-body-qrCode">
              <img src="<?php echo $PATH;?>/assets/images/common/qrcode.png" alt="">
            </div>
          </div>
          <div class="p-event--detail-body-item">
            <h4 class="title-lv4 type2 mgb-10">お電話でのご相談はこちら</h4>
            <p class="desc2">お電話からでもご相談いただけます。<br> 下記の電話番号までご連絡ください。※通話料はご負担になります<br><a class="tel" href="tel:045-507-4177">TEL: 045-507-4177</a></p>
          </div>
          <div class="p-event--detail-body-item">
            <h4 class="title-lv4 type2 mgb-10">オンライン（Zoom）でのご相談はこちら</h4>
            <p class="desc2 mgb-20">毎週金土日・祝日限定でオンライン（Zoom）でのご相談も受け付けております。<br> 上記LINEかお電話にて、ご希望の相談日時をご連絡ください。<br>ご相談日時前にZoomミーティングへ招待するURLをメールでお送りします。</p>
          </div>
          <div class="p-event--detail-body-item">
            <ul class="list">
              <li>Zoomのダウンロード等の設定は相談日時までに、お客様にてお願いします。</li>
            </ul>
            <a href="" class="link-icon blank" target="_blank">Zoomのダウンロードはこちら</a>
          </div>
        </div><!-- ./p-event--detail-body -->
      </div><!-- ./container -->
      <div class="p-event--detail-signupBox">
        <div class="p-event--detail-signup">
          <p class="p-event--detail-signup-ttl"><span>お申込みはこちらから！</span></p>
          <p class="desc2">ご予約お申し込みはこちらから受け付けております。どなたでもお気軽にお申し込みください。</p>
          <div class="p-event--detail-signup-direct">
            <a href="" class="btn-blank" target="_blank"><span>来場予約サイトへ</span></a>
          </div>
        </div>
      </div><!-- ./p-event--detail-signup -->
      <div class="p-event--detail-otherPost">
        <div class="container">
          <div class="p-event--detail-otherPost-item">
            <h3 class="title-lv3">同じ展示場のイベント・相談会情報</h3>
            <ul class="p-event--infor-list">
               <li class="p-event--infor-item">
                <a class="link" href="/event/detail">
                  <div class="p-event--infor-item-thumb">
                    <img src="<?php echo $PATH;?>/assets/images/event/event04.png" alt="">
                  </div>
                  <div class="p-event--infor-item-cnt">
                    <div class="p-event--infor-item-tagWrap">
                      <span class="location">横浜南部エリア</span>
                      <span class="tag3">イベント</span>
                    </div>
                    <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                    <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                    <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                  </div>
                </a>
              </li>
              <li class="p-event--infor-item">
                <a class="link" href="/event/detail">
                  <div class="p-event--infor-item-thumb">
                    <img src="<?php echo $PATH;?>/assets/images/event/event05.png" alt="">
                  </div>
                  <div class="p-event--infor-item-cnt">
                    <div class="p-event--infor-item-tagWrap">
                      <span class="location">横浜南部エリア</span>
                      <span class="tag3">イベント</span>
                    </div>
                    <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                    <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                    <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                  </div>
                </a>
              </li>
              <li class="p-event--infor-item">
                <a class="link" href="/event/detail">
                  <div class="p-event--infor-item-thumb">
                    <img src="<?php echo $PATH;?>/assets/images/event/event06.png" alt="">
                  </div>
                  <div class="p-event--infor-item-cnt">
                    <div class="p-event--infor-item-tagWrap">
                      <span class="location">横浜南部エリア</span>
                      <span class="tag3 type2">相談会</span>
                    </div>
                    <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                    <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                    <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                  </div>
                </a>
              </li>
            </ul>
          </div>
          <div class="p-event--detail-otherPost-item">
            <h3 class="title-lv3">同じ展示場のイベント・相談会情報</h3>
            <ul class="p-event--infor-list">
              <li class="p-event--infor-item">
                <a class="link" href="/event/detail">
                  <div class="p-event--infor-item-thumb">
                    <img src="<?php echo $PATH;?>/assets/images/event/event07.png" alt="">
                  </div>
                  <div class="p-event--infor-item-cnt">
                    <div class="p-event--infor-item-tagWrap">
                      <span class="location">横浜南部エリア</span>
                      <span class="tag3">イベント</span>
                    </div>
                    <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                    <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                    <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                  </div>
                </a>
              </li>
              <li class="p-event--infor-item">
                <a class="link" href="/event/detail">
                  <div class="p-event--infor-item-thumb">
                    <img src="<?php echo $PATH;?>/assets/images/event/event08.png" alt="">
                  </div>
                  <div class="p-event--infor-item-cnt">
                    <div class="p-event--infor-item-tagWrap">
                      <span class="location">横浜南部エリア</span>
                      <span class="tag3">イベント</span>
                    </div>
                    <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                    <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                    <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                  </div>
                </a>
              </li>
              <li class="p-event--infor-item">
                <a class="link" href="/event/detail">
                  <div class="p-event--infor-item-thumb">
                    <img src="<?php echo $PATH;?>/assets/images/event/event09.png" alt="">
                  </div>
                  <div class="p-event--infor-item-cnt">
                    <div class="p-event--infor-item-tagWrap">
                      <span class="location">横浜南部エリア</span>
                      <span class="tag3 type2">相談会</span>
                    </div>
                    <h3 class="p-event--infor-item-ttl">新築・リフォーム相談【1月】＜LINE・電話・オンライン相談可＞</h3>
                    <span class="cat">ABCハウジング 新・川崎住宅公園</span>
                    <p class="date2">開催予定日：2021年02月27日(土)～2021年03月06日(土)</p>
                  </div>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div><!-- ./p-event--detail-otherPost -->

      <p class="view-moreWrap align-center"><a href="/news" class="view-more">セミナー・イベント情報一覧を見る</a></p>
      
    </div><!-- ./p-event--cnt -->
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>