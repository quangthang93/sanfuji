<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-blog.php'; ?>
    <div class="blog-popup--overlay js-blogOverlay"></div>
    <div class="blog-popup js-blogPopup">
      <div class="blog-popup--inner">
        <div class="blog-popup--cnt">
          <div class="blog-popup--ctrl js-blogPopupCtrl">
            <img src="<?php echo $PATH;?>/assets/images/common/btn-close.png" alt="">
          </div>
          <a class="link" href="">
            <div class="blog-popup--cnt-thumb">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/notice-slide.png" alt="">
            </div>
            <div class="blog-popup--cnt-infor">
              <h3 class="notice-slider--item-ttl">プレゼントキャンペーン実施​</h3>
              <p class="txt-blue">パナソニックホームズ</p>
              <p class="txt-blue2">カサート</p>
              <p class="desc4">住宅展示場ご来場予約のお客様を対象に、選べるプレゼントをご用意いただきました。</p>
              <p class="date4">2021.05.25 更新</p>
            </div>
          </a>
        </div>
      </div>
    </div>

    <div class="blog-banner">
      <img src="<?php echo $PATH;?>/assets/images/blog/blog-banner.png" alt="">
    </div>
    <div class="blog-nav">
      <!-- <a href="/" class="blog-nav-gotop sp-only">家ーるTOP</a> -->
      <div class="blog-nav-ctrl js-blogNavCtrl sp-only">
        <span></span>
        <span></span>
        <span></span>
      </div>
      <div class="blog-nav-close js-blogNavClose sp-only"></div>
      <div class="blog-nav--directWrap">
        <div class="blog-nav--directInner js-blogNavDirect">
          <p class="blog-nav--direct-ttl sp-only">コラムのカテゴリー</p>
          <ul class="blog-nav--direct">
            <li><a class="link" href="">家づくりのノウハウ</a></li>
            <li><a class="link" href="">ライフスタイル・暮らし</a></li>
            <li><a class="link" href="">インテリア</a></li>
            <li><a class="link" href="">神奈川の街</a></li>
            <li><a class="link" href="">コラム・インタビュー</a></li>
            <li><a class="link" href="">イベント</a></li>
          </ul>
          <form class="blog-nav--form type2 sp-only" action="/search">
            <input type="text" name="q" class="js-search-boxInput search-boxInput" placeholder="フリーワードを記入してください">
            <input type="submit" value="検索">
          </form>
        </div>
        <div class="blog-nav--snsWrap">
          <div class="blog-nav--sns">
            <a class="blog-nav--sns-search link js-btnBlogSearch" href="javascript:void(0)"></a>
            <a class="blog-nav--sns-twitter link" href=""></a>
          </div>
        </div>
        <form class="blog-nav--form js-formBlogSearch" action="/search">
          <input type="text" name="q" class="js-search-boxInput search-boxInput" placeholder="フリーワードを記入してください">
          <input type="submit" value="検索">
        </form>
      </div>
      <div class="overlay js-overlay sp-only"></div>
    </div><!-- ./blog-nav -->
    <nav class="breadcrumbs ts-contain" id="breadcrumb">
      <div class="inner wrap" breadcrumbs ts-contain><span><a href="http://sanfuji-media.f-demo.com/"><span>TOP</span></a></span> <span class="delim"><i class="tsi tsi-angle-right"></i></span> <span><a href="http://sanfuji-media.f-demo.com/?cat=8"><span>イベント</span></a></span> <span class="delim"><i class="tsi tsi-angle-right"></i></span> <span class="current">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</span></div>
    </nav>
    <div class="main wrap">
      <div class="ts-row cf">
        <aside class="col-4 sidebar" data-sticky="1">
          <div class="inner  theiaStickySidebar">
            <ul>
              <li id="search-2" class="widget widget_search">
                <form method="get" class="search-form" action="http://sanfuji-media.f-demo.com/">
                  <label>
                    <span class="screen-reader-text">Search for:</span>
                    <input type="search" class="search-field" placeholder="フリーワードを記入してください" value="" name="s" title="Search for:" />
                  </label>
                  <button type="submit" class="search-submit"><i class="tsi tsi-search"></i></button>
                </form>
              </li>
              <li id="wpp-2" class="widget popular-posts">
                <h5 class="widget-title block-head-widget has-style"><span class="title">RANKING</span></h5><!-- cached -->
                <!-- WordPress Popular Posts -->
                <ul class="wpp-list wpp-cards">
                  <li><img src="http://sanfuji-media.f-demo.com/wp-content/uploads/wordpress-popular-posts/35-featured-124x83.png" width="124" height="83" alt="" class="wpp-thumbnail wpp_featured wpp_cached_thumb" loading="lazy" />
                    <div class="wpp-item-data">
                      <div class="taxonomies"></div><a href="http://sanfuji-media.f-demo.com/?p=35" class="wpp-post-title" target="_self">分譲住宅のメリット・デメリット</a>
                      <p class="wpp-excerpt">JUNE 18 , 2019</p>
                    </div>
                  </li>
                  <li><img src="http://sanfuji-media.f-demo.com/wp-content/uploads/wordpress-popular-posts/31-featured-124x83.png" width="124" height="83" alt="" class="wpp-thumbnail wpp_featured wpp_cached_thumb" loading="lazy" />
                    <div class="wpp-item-data">
                      <div class="taxonomies"></div><a href="http://sanfuji-media.f-demo.com/?p=31" class="wpp-post-title" target="_self">住宅ローンを利用して夢のマイホームを</a>
                      <p class="wpp-excerpt">JUNE 18 , 2019</p>
                    </div>
                  </li>
                  <li><img src="http://sanfuji-media.f-demo.com/wp-content/uploads/wordpress-popular-posts/32-featured-124x83.png" width="124" height="83" alt="" class="wpp-thumbnail wpp_featured wpp_cached_thumb" loading="lazy" />
                    <div class="wpp-item-data">
                      <div class="taxonomies"></div><a href="http://sanfuji-media.f-demo.com/?p=32" class="wpp-post-title" target="_self">分譲住宅のメリット・デメリット</a>
                      <p class="wpp-excerpt">JUNE 18 , 2019</p>
                    </div>
                  </li>
                  <li><img src="http://sanfuji-media.f-demo.com/wp-content/uploads/wordpress-popular-posts/33-featured-124x83.png" width="124" height="83" alt="" class="wpp-thumbnail wpp_featured wpp_cached_thumb" loading="lazy" />
                    <div class="wpp-item-data">
                      <div class="taxonomies"></div><a href="http://sanfuji-media.f-demo.com/?p=33" class="wpp-post-title" target="_self">都内のタワーマンション人気ランキング</a>
                      <p class="wpp-excerpt">JUNE 18 , 2019</p>
                    </div>
                  </li>
                  <li><img src="http://sanfuji-media.f-demo.com/wp-content/uploads/wordpress-popular-posts/34-featured-124x83.png" width="124" height="83" alt="" class="wpp-thumbnail wpp_featured wpp_cached_thumb" loading="lazy" />
                    <div class="wpp-item-data">
                      <div class="taxonomies"></div><a href="http://sanfuji-media.f-demo.com/?p=34" class="wpp-post-title" target="_self">住宅ローンを利用して夢のマイホームを</a>
                      <p class="wpp-excerpt">JUNE 18 , 2019</p>
                    </div>
                  </li>
                </ul>
              </li>
              <li id="bunyad-posts-widget-2" class="widget widget-posts">
                <h5 class="widget-title block-head-widget has-style"><span class="title">latest article</span></h5>
                <ul class="posts cf meta-below">
                  <li class="post cf">
                    <div class="post-thumb">
                      <a href="http://sanfuji-media.f-demo.com/?p=35" class="image-link media-ratio ar-cheerup-thumb"><span data-bgsrc="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png" class="img bg-cover wp-post-image attachment-large size-large lazyload" data-bgset="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png 762w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-300x200.png 300w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-175x117.png 175w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-450x301.png 450w" data-sizes="(max-width: 87px) 100vw, 87px"></span></a> </div>
                    <div class="content">
                      <div class="post-meta post-meta-b post-meta-left has-below">
                        <h4 class="is-title post-title"><a href="http://sanfuji-media.f-demo.com/?p=35">都内のタワーマンション人気ランキング</a></h4>
                        <div class="below meta-below"><a href="http://sanfuji-media.f-demo.com/?p=35" class="meta-item date-link">
                            <time class="post-date" datetime="2021-02-09T03:31:45+09:00">JUNE 18 , 2019</time>
                          </a></div>
                      </div>
                    </div>
                  </li>
                  <li class="post cf">
                    <div class="post-thumb">
                      <a href="http://sanfuji-media.f-demo.com/?p=34" class="image-link media-ratio ar-cheerup-thumb"><span data-bgsrc="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png" class="img bg-cover wp-post-image attachment-large size-large lazyload" data-bgset="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png 762w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-300x200.png 300w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-175x117.png 175w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-450x301.png 450w" data-sizes="(max-width: 87px) 100vw, 87px"></span></a> </div>
                    <div class="content">
                      <div class="post-meta post-meta-b post-meta-left has-below">
                        <h4 class="is-title post-title"><a href="http://sanfuji-media.f-demo.com/?p=34">分譲住宅のメリット・デメリット</a></h4>
                        <div class="below meta-below"><a href="http://sanfuji-media.f-demo.com/?p=34" class="meta-item date-link">
                            <time class="post-date" datetime="2021-02-09T03:31:43+09:00">JUNE 18 , 2019</time>
                          </a></div>
                      </div>
                    </div>
                  </li>
                  <li class="post cf">
                    <div class="post-thumb">
                      <a href="http://sanfuji-media.f-demo.com/?p=33" class="image-link media-ratio ar-cheerup-thumb"><span data-bgsrc="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png" class="img bg-cover wp-post-image attachment-large size-large lazyload" data-bgset="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png 762w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-300x200.png 300w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-175x117.png 175w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-450x301.png 450w" data-sizes="(max-width: 87px) 100vw, 87px"></span></a> </div>
                    <div class="content">
                      <div class="post-meta post-meta-b post-meta-left has-below">
                        <h4 class="is-title post-title"><a href="http://sanfuji-media.f-demo.com/?p=33">住宅ローンを利用して夢のマイホームを</a></h4>
                        <div class="below meta-below"><a href="http://sanfuji-media.f-demo.com/?p=33" class="meta-item date-link">
                            <time class="post-date" datetime="2021-02-09T03:31:41+09:00">JUNE 18 , 2019</time>
                          </a></div>
                      </div>
                    </div>
                  </li>
                </ul>
              </li>
              <li id="bunyad-posts-widget-2" class="widget widget-posts">
                <h5 class="widget-title block-head-widget has-style"><span class="title">Recommend</span></h5>
                <ul class="posts cf meta-below">
                  <li class="post cf">
                    <div class="post-thumb">
                      <a href="http://sanfuji-media.f-demo.com/?p=35" class="image-link media-ratio ar-cheerup-thumb"><span data-bgsrc="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png" class="img bg-cover wp-post-image attachment-large size-large lazyload" data-bgset="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png 762w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-300x200.png 300w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-175x117.png 175w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-450x301.png 450w" data-sizes="(max-width: 87px) 100vw, 87px"></span></a> </div>
                    <div class="content">
                      <div class="post-meta post-meta-b post-meta-left has-below">
                        <h4 class="is-title post-title"><a href="http://sanfuji-media.f-demo.com/?p=35">都内のタワーマンション人気ランキング</a></h4>
                        <div class="below meta-below"><a href="http://sanfuji-media.f-demo.com/?p=35" class="meta-item date-link">
                            <time class="post-date" datetime="2021-02-09T03:31:45+09:00">JUNE 18 , 2019</time>
                          </a></div>
                      </div>
                    </div>
                  </li>
                  <li class="post cf">
                    <div class="post-thumb">
                      <a href="http://sanfuji-media.f-demo.com/?p=34" class="image-link media-ratio ar-cheerup-thumb"><span data-bgsrc="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png" class="img bg-cover wp-post-image attachment-large size-large lazyload" data-bgset="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png 762w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-300x200.png 300w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-175x117.png 175w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-450x301.png 450w" data-sizes="(max-width: 87px) 100vw, 87px"></span></a> </div>
                    <div class="content">
                      <div class="post-meta post-meta-b post-meta-left has-below">
                        <h4 class="is-title post-title"><a href="http://sanfuji-media.f-demo.com/?p=34">住宅ローンを利用して夢のマイホームを</a></h4>
                        <div class="below meta-below"><a href="http://sanfuji-media.f-demo.com/?p=34" class="meta-item date-link">
                            <time class="post-date" datetime="2021-02-09T03:31:43+09:00">JUNE 18 , 2019</time>
                          </a></div>
                      </div>
                    </div>
                  </li>
                  <li class="post cf">
                    <div class="post-thumb">
                      <a href="http://sanfuji-media.f-demo.com/?p=33" class="image-link media-ratio ar-cheerup-thumb"><span data-bgsrc="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png" class="img bg-cover wp-post-image attachment-large size-large lazyload" data-bgset="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png 762w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-300x200.png 300w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-175x117.png 175w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-450x301.png 450w" data-sizes="(max-width: 87px) 100vw, 87px"></span></a> </div>
                    <div class="content">
                      <div class="post-meta post-meta-b post-meta-left has-below">
                        <h4 class="is-title post-title"><a href="http://sanfuji-media.f-demo.com/?p=33">都内のタワーマンション人気ランキング</a></h4>
                        <div class="below meta-below"><a href="http://sanfuji-media.f-demo.com/?p=33" class="meta-item date-link">
                            <time class="post-date" datetime="2021-02-09T03:31:41+09:00">JUNE 18 , 2019</time>
                          </a></div>
                      </div>
                    </div>
                  </li>
                </ul>
              </li>
              <li id="categories-3" class="widget widget_categories">
                <h5 class="widget-title block-head-widget has-style"><span class="title">CATEGORY</span></h5>
                <ul>
                  <li class="cat-item cat-item-8"><a href="http://sanfuji-media.f-demo.com/?cat=8">イベント</a>
                  </li>
                  <li class="cat-item cat-item-5"><a href="http://sanfuji-media.f-demo.com/?cat=5">インテリア</a>
                  </li>
                  <li class="cat-item cat-item-4"><a href="http://sanfuji-media.f-demo.com/?cat=4">ライフスタイル・暮らし</a>
                  </li>
                </ul>
              </li>
              <li id="tag_cloud-2" class="widget widget_tag_cloud">
                <h5 class="widget-title block-head-widget has-style"><span class="title">#TAG</span></h5>
                <div class="tagcloud"><a href="http://sanfuji-media.f-demo.com/?tag=%e3%82%b9%e3%83%8e%e3%83%bc%e3%83%89%e3%83%bc%e3%83%a0" class="tag-cloud-link tag-link-15 tag-link-position-1" style="font-size: 8pt;" aria-label="スノードーム (1個の項目)">#スノードーム</a>
                  <a href="http://sanfuji-media.f-demo.com/?tag=%e3%83%9a%e3%83%83%e3%83%88%e3%81%8a%e3%82%82%e3%81%a1%e3%82%83" class="tag-cloud-link tag-link-9 tag-link-position-2" style="font-size: 22pt;" aria-label="ペットおもちゃ (3個の項目)">#ペットおもちゃ</a></div>
              </li>
              <li id="custom_html-2" class="widget_text widget widget_custom_html">
                <div class="textwidget custom-html-widget">
                  <div>
                    <div class="blog-advers">
                      <a href=""><img src="<?php echo $PATH;?>/assets/images/blog/advers01.png" alt=""></a>
                    </div>

                    <div class="blog-sns">
                      <h5 class="widget-title block-head-widget has-style"><span class="title">FOLLOW ME !</span></h5>
                      <div class="blog-sns--labelWrap">
                        <p class="blog-sns--label">TWITTER</p>
                      </div>
                      <img src="<?php echo $PATH;?>/assets/images/blog/twitter.png" alt="">
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </aside>
        <div class="col-8 main-content cf">
          <article id="post-35" class="the-post single-default post-35 post type-post status-publish format-standard has-post-thumbnail category-8 tag-15">
            <header class="post-header the-post-header cf">
              <h1 class="sfuji-title align-left mgb-25">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</h1>
              <div class="featured">
                <a href="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png" class="image-link"><img width="762" height="509" src="data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PScwIDAgMTAyNCAxMDI0JyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnPjwvc3ZnPg==" class="attachment-large size-large lazyload wp-post-image" alt="" loading="lazy" sizes="(max-width: 768px) 100vw, 768px" title="この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています" data-srcset="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png 762w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-300x200.png 300w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-175x117.png 175w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-450x301.png 450w" data-src="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png" /></a>
              </div>
            </header><!-- .post-header -->
            <div class="row type2" style="padding: 20px 30px 50px;">
              <div class="col-6">
                <ul class="post-categories">
                  <li><a href="http://sanfuji-media.f-demo.com/?cat=8" rel="category">イベント</a></li>
                </ul> 2月 9,2021
              </div>
              <div class="col-6">
                <div class="post-share">
                  <div class="post-share-icons cf">
                    <span class="counters">
                    </span>
                    <a href="https://www.facebook.com/sharer.php?u=http%3A%2F%2Fsanfuji-media.f-demo.com%2F%3Fp%3D35" class="link facebook" target="_blank" title="Share on Facebook"><i class="tsi tsi-facebook"></i></a>
                    <a href="https://twitter.com/intent/tweet?url=http%3A%2F%2Fsanfuji-media.f-demo.com%2F%3Fp%3D35&#038;text=%E3%81%93%E3%81%AE%E6%96%87%E7%AB%A0%E3%81%AF%E3%83%80%E3%83%9F%E3%83%BC%E3%81%A7%E3%81%99%E3%80%82%E6%96%87%E5%AD%97%E3%81%AE%E5%A4%A7%E3%81%8D%E3%81%95%E3%80%81%E9%87%8F%E3%80%81%E5%AD%97%E9%96%93%E3%80%81%E8%A1%8C%E9%96%93%E7%AD%89%E3%82%92%E7%A2%BA%E8%AA%8D%E3%81%99%E3%82%8B%E3%81%9F%E3%82%81%E3%81%AB%E5%85%A5%E3%82%8C%E3%81%A6%E3%81%84%E3%81%BE%E3%81%99" class="link twitter" target="_blank" title="Share on Twitter"><i class="tsi tsi-twitter"></i></a>
                    <a href="https://plus.google.com/share?url=http%3A%2F%2Fsanfuji-media.f-demo.com%2F%3Fp%3D35" class="link google" target="_blank" title="Share on Google"><i class="tsi tsi-google"></i></a>
                    <a href="https://social-plugins.line.me/lineit/share?url=http%3A%2F%2Fsanfuji-media.f-demo.com%2F%3Fp%3D35" class="link line" target="_blank" title="Share on Line"><i class="tsi tsi-line"></i></a>
                    <a href="https://pinterest.com/pin/create/button/?url=http%3A%2F%2Fsanfuji-media.f-demo.com%2F%3Fp%3D35&#038;media=http%3A%2F%2Fsanfuji-media.f-demo.com%2Fwp-content%2Fuploads%2F2021%2F02%2Fimg.png&#038;description=%E3%81%93%E3%81%AE%E6%96%87%E7%AB%A0%E3%81%AF%E3%83%80%E3%83%9F%E3%83%BC%E3%81%A7%E3%81%99%E3%80%82%E6%96%87%E5%AD%97%E3%81%AE%E5%A4%A7%E3%81%8D%E3%81%95%E3%80%81%E9%87%8F%E3%80%81%E5%AD%97%E9%96%93%E3%80%81%E8%A1%8C%E9%96%93%E7%AD%89%E3%82%92%E7%A2%BA%E8%AA%8D%E3%81%99%E3%82%8B%E3%81%9F%E3%82%81%E3%81%AB%E5%85%A5%E3%82%8C%E3%81%A6%E3%81%84%E3%81%BE%E3%81%99" class="link pinterest" target="_blank" title="Pinterest"><i class="tsi tsi-pinterest-p"></i></a>
                    <a href="https://b.hatena.ne.jp/entry/http%3A%2F%2Fsanfuji-media.f-demo.com%2F%3Fp%3D35" class="link hatena" target="_blank" title="Share on Google"><i class="tsi tsi-bing"></i></a>
                  </div>
                </div>
              </div>
              <div style="clear: both;"></div>
            </div>
            <div class="post-content description cf entry-content content-spacious">
              <p>この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。</p>
              <div id="toc_container" class="no_bullets">
                <p class="toc_title">この記事の目次</p>
                <ul class="toc_list">
                  <li><a href="#i"><span class="toc_number toc_depth_1">1</span> 中見出しテキスト</a></li>
                  <li><a href="#i-2"><span class="toc_number toc_depth_1">2</span> 編集アイテム一覧</a>
                    <ul>
                      <li><a href="#i-3"><span class="toc_number toc_depth_2">2.1</span> 吹き出しアイテム</a></li>
                      <li><a href="#2"><span class="toc_number toc_depth_2">2.2</span> 写真を横に2枚(横長・縦長・正方形)</a></li>
                      <li><a href="#i-4"><span class="toc_number toc_depth_2">2.3</span> テキスト背景色あり</a></li>
                    </ul>
                  </li>
                  <li><a href="#i-5"><span class="toc_number toc_depth_1">3</span> テキスト背景色あり</a></li>
                  <li><a href="#i-6"><span class="toc_number toc_depth_1">4</span> テキスト背景色あり</a></li>
                  <li><a href="#i-7"><span class="toc_number toc_depth_1">5</span> テキスト背景色あり</a></li>
                  <li><a href="#i-8"><span class="toc_number toc_depth_1">6</span> テキスト背景色あり</a></li>
                  <li><a href="#i-9"><span class="toc_number toc_depth_1">7</span> まとめ</a></li>
                </ul>
              </div>
              <h2><span id="i">中見出しテキスト</span></h2>
              <p>この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。この文章はダミーです。</p>
              <h2><span id="i-2">編集アイテム一覧</span></h2>
              <h3><span id="i-3">吹き出しアイテム</span></h3>

              <div class="blog-balloon">
                <div class="blog-balloon--item">
                  <div class="blog-balloon--item-thumbWrap">
                    <div class="blog-balloon--item-thumb">
                      <img src="<?php echo $PATH;?>/assets/images/blog/ball0on01.png" alt="">
                    </div>
                    <p class="blog-balloon--item-name">ご質問者様</p>
                  </div>
                  <div class="blog-balloon--item-cnt">吹き出し内コメント入ります。吹き出し内コメント入ります。吹き出し内コメント入ります。吹き出し内コメント入ります。吹き出し内コメント入ります。吹き出し内コメント入ります。</div>
                </div>
                <div class="blog-balloon--item">
                  <div class="blog-balloon--item-thumbWrap">
                    <div class="blog-balloon--item-thumb">
                      <img src="<?php echo $PATH;?>/assets/images/blog/ball0on02.png" alt="">
                    </div>
                    <p class="blog-balloon--item-name">ご質問者様</p>
                  </div>
                  <div class="blog-balloon--item-cnt">吹き出し内コメント入ります。吹き出し内コメント入ります。吹き出し内コメント入ります。吹き出し内コメント入ります。吹き出し内コメント入ります。吹き出し内コメント入ります。</div>
                </div>
              </div><!-- ./blog-balloon -->

              <br>
              <h3><span id="2">写真を横に2枚(横長・縦長・正方形)</span></h3>
              <div class="wp-block-columns">
                <div class="wp-block-column">
                  <figure class="wp-block-image size-large"><img loading="lazy" width="345" height="230" src="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/1-2img.png" alt="" class="wp-image-58" srcset="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/1-2img.png 345w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/1-2img-300x200.png 300w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/1-2img-175x117.png 175w" sizes="(max-width: 345px) 100vw, 345px" /></figure>
                </div>
                <div class="wp-block-column">
                  <figure class="wp-block-image size-large"><img loading="lazy" width="345" height="230" src="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/1-2img.png" alt="" class="wp-image-58" srcset="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/1-2img.png 345w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/1-2img-300x200.png 300w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/1-2img-175x117.png 175w" sizes="(max-width: 345px) 100vw, 345px" /></figure>
                </div>
              </div>
              <div class="wp-block-columns">
                <div class="wp-block-column">
                  <figure class="wp-block-image size-large"><img loading="lazy" width="345" height="460" src="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/1-2img-1.png" alt="" class="wp-image-61" srcset="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/1-2img-1.png 345w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/1-2img-1-225x300.png 225w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/1-2img-1-175x233.png 175w" sizes="(max-width: 345px) 100vw, 345px" /></figure>
                </div>
                <div class="wp-block-column">
                  <figure class="wp-block-image size-large"><img loading="lazy" width="345" height="460" src="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/1-2img-1.png" alt="" class="wp-image-61" srcset="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/1-2img-1.png 345w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/1-2img-1-225x300.png 225w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/1-2img-1-175x233.png 175w" sizes="(max-width: 345px) 100vw, 345px" /></figure>
                </div>
              </div>
              <div class="wp-block-columns">
                <div class="wp-block-column">
                  <figure class="wp-block-image size-large"><img loading="lazy" width="345" height="230" src="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/1-2img.png" alt="" class="wp-image-58" srcset="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/1-2img.png 345w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/1-2img-300x200.png 300w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/1-2img-175x117.png 175w" sizes="(max-width: 345px) 100vw, 345px" /></figure>
                </div>
                <div class="wp-block-column">
                  <figure class="wp-block-image size-large"><img loading="lazy" width="345" height="230" src="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/1-2img.png" alt="" class="wp-image-58" srcset="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/1-2img.png 345w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/1-2img-300x200.png 300w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/1-2img-175x117.png 175w" sizes="(max-width: 345px) 100vw, 345px" /></figure>
                </div>
              </div>
              <br>
              <h3><span id="i-4">テキスト背景色あり</span></h3>
              <p class="has-background" style="background-color:#f1f3f5">背景ありのテキストです。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。</p>

              <br>
              <h3><span id="i-4">テキスト囲いあり</span></h3>
              <p class="has-border" style="background-color:#fff">囲みありのテキストです。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。</p>

              <br>
              <h3><span id="i-4">引用テキスト</span></h3>
              <p class="has-quote" style="background-color:#fff">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。この文章はダミーです。</p>
              <br>
              <br>
              <a href="" class="link3">テキストリンク</a>
              <br>
              <br>
              <a href="" class="link-icon arrow">テキストリンク</a>
              <br>
              <a href="" class="link-icon blank">テキストリンク</a>
              <br>
              <a href="" class="link-icon pdf">テキストリンク</a>

              <br>
              <br>

              <blockquote class="instagram-media" data-instgrm-captioned="" data-instgrm-permalink="https://www.instagram.com/p/CIP6YFzFbMV/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="13" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);">
                <div style="padding:16px;"> <a href="https://www.instagram.com/p/CIP6YFzFbMV/?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank" rel="noopener">
                    <div style=" display: flex; flex-direction: row; align-items: center;">
                      <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div>
                      <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;">
                        <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div>
                        <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div>
                      </div>
                    </div>
                    <div style="padding: 19% 0;"></div>
                    <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <g transform="translate(-511.000000, -20.000000)" fill="#000000">
                            <g>
                              <path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path>
                            </g>
                          </g>
                        </g>
                      </svg></div>
                    <div style="padding-top: 8px;">
                      <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;"> View this post on Instagram</div>
                    </div>
                    <div style="padding: 12.5% 0;"></div>
                    <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;">
                      <div>
                        <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div>
                        <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div>
                        <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div>
                      </div>
                      <div style="margin-left: 8px;">
                        <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div>
                        <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div>
                      </div>
                      <div style="margin-left: auto;">
                        <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div>
                        <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div>
                        <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div>
                      </div>
                    </div>
                    <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;">
                      <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div>
                      <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div>
                    </div>
                  </a>
                  <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/CIP6YFzFbMV/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank" rel="noopener">A post shared by Man Demo (@man_demo_japan)</a></p>
                </div>
              </blockquote>
              <script async="" src="//www.instagram.com/embed.js"></script>
              <h2><span id="i-5">テキスト背景色あり</span></h2>
              <h2><span id="i-6">テキスト背景色あり</span></h2>
              <h2><span id="i-7">テキスト背景色あり</span></h2>
              <h2><span id="i-8">テキスト背景色あり</span></h2>
              <h2><span id="i-9">まとめ</span></h2>
              <p>この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。この文章はダミーです。</p>

              <nav class="main-pagination number">
                <div class="inner">
                  <span class="page-numbers label-prev"><span class="disabled"><i class="tsi tsi-long-arrow-left"></i>Previous</span></span><span aria-current="page" class="page-numbers current">1</span>
                  <a class="page-numbers" href="http://sanfuji-media.f-demo.com/?paged=2">2</a><span class="page-numbers label-next"><a href="http://sanfuji-media.f-demo.com/?paged=2">Next<i class="tsi tsi-long-arrow-right"></i></a></span> 
                </div>
              </nav>
              <br>
              <br>
              <div>
                <a href="" class="tag4"># マンション</a>
                <a href="" class="tag4"># クリスマスリース</a>
              </div>
            </div><!-- .post-content -->
           <!--  <div class="the-post-foot cf">
              <div class="tag-share cf">
                <div class="post-tags"><a href="http://sanfuji-media.f-demo.com/?tag=%e3%82%b9%e3%83%8e%e3%83%bc%e3%83%89%e3%83%bc%e3%83%a0" rel="tag">スノードーム</a></div>
              </div>
            </div> -->
            <div class="post-nav">
              <div class="post previous cf">
                <a href="http://sanfuji-media.f-demo.com/?p=34" title="Prev Post" class="nav-icon">
                  <i class="tsi tsi-angle-left"></i>
                </a>
                <span class="content">
                  <a href="http://sanfuji-media.f-demo.com/?p=34" class="image-link">
                    <img width="150" height="150" src="data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PScwIDAgMTUwIDE1MCcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJz48L3N2Zz4=" class="attachment-thumbnail size-thumbnail lazyload wp-post-image" alt="" loading="lazy" data-src="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-150x150.png" /> </a>
                  <div class="post-meta">
                    <span class="label">Prev Post</span>
                    <div class="post-meta post-meta-b post-meta-left has-below">
                      <h2 class="is-title post-title"><a href="http://sanfuji-media.f-demo.com/?p=34">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</a></h2>
                      <div class="below meta-below"><a href="http://sanfuji-media.f-demo.com/?p=34" class="meta-item date-link">
                          <time class="post-date" datetime="2021-02-09T03:31:43+09:00">2月 9,2021</time>
                        </a></div>
                    </div>
                  </div>
                </span>
              </div>
            </div>
            <section class="related-posts grid-2">
              <h4 class="section-head"><span class="title">この記事に関連する記事</span></h4>
              <div class="ts-row posts cf">
                <article class="post col-4">
                  <a href="http://sanfuji-media.f-demo.com/?p=34" class="image-link media-ratio ratio-4-3"><span data-bgsrc="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png" class="img bg-cover wp-post-image attachment-large size-large lazyload" data-bgset="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png 762w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-450x301.png 450w" data-sizes="(max-width: 370px) 100vw, 370px"></span></a>
                  <div class="content">
                    <span class="post-cat2">
                      <a href="http://sanfuji-media.f-demo.com/?cat=8" class="category" rel="category">家づくりのノウハウ</a>
                    </span>
                    <h3 class="post-title"><a href="http://sanfuji-media.f-demo.com/?p=34" class="post-link">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</a></h3>
                    <div class="post-meta post-meta-b has-below">
                      <div class="below meta-below"><a href="http://sanfuji-media.f-demo.com/?p=34" class="meta-item date-link">
                          <time class="post-date" datetime="2021-02-09T03:31:43+09:00">2月 9,2021</time>
                        </a></div>
                    </div>
                  </div>
                </article>
                <article class="post col-4">
                  <a href="http://sanfuji-media.f-demo.com/?p=33" class="image-link media-ratio ratio-4-3"><span data-bgsrc="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png" class="img bg-cover wp-post-image attachment-large size-large lazyload" data-bgset="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png 762w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-450x301.png 450w" data-sizes="(max-width: 370px) 100vw, 370px"></span></a>
                  <div class="content">
                    <span class="post-cat2">
                      <a href="http://sanfuji-media.f-demo.com/?cat=8" class="category" rel="category">ライフスタイル・暮らし</a>
                    </span>
                    <h3 class="post-title"><a href="http://sanfuji-media.f-demo.com/?p=33" class="post-link">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</a></h3>
                    <div class="post-meta post-meta-b has-below">
                      <div class="below meta-below"><a href="http://sanfuji-media.f-demo.com/?p=33" class="meta-item date-link">
                          <time class="post-date" datetime="2021-02-09T03:31:41+09:00">2月 9,2021</time>
                        </a></div>
                    </div>
                  </div>
                </article>
              </div>
            </section>
          </article> <!-- .the-post -->
        </div>
      </div> <!-- .ts-row -->
    </div> <!-- .main -->
    <div class="blog-contact">
      <a class="link" href="/contact">CONTACT</a>
    </div>
 
 <!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer-blog.php'; ?>