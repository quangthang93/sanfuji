<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-blog.php'; ?>
<div class="blog-banner">
  <img src="<?php echo $PATH;?>/assets/images/blog/blog-banner.png" alt="">
</div>
<div class="blog-nav">
  <div class="blog-nav-ctrl js-blogNavCtrl sp-only">
    <span></span>
    <span></span>
    <span></span>
  </div>
  <div class="blog-nav-close js-blogNavClose sp-only"></div>
  <div class="blog-nav--directWrap">
    <ul class="blog-nav--direct js-blogNavDirect">
      <li><a class="link" href="">家づくりのノウハウ</a></li>
      <li><a class="link" href="">ライフスタイル・暮らし</a></li>
      <li><a class="link" href="">インテリア</a></li>
      <li><a class="link" href="">神奈川の街</a></li>
      <li><a class="link" href="">コラム・インタビュー</a></li>
      <li><a class="link" href="">イベント</a></li>
    </ul>
    <div class="blog-nav--snsWrap">
      <div class="blog-nav--sns">
        <a class="blog-nav--sns-search link" href=""></a>
        <a class="blog-nav--sns-twitter link" href=""></a>
      </div>
    </div>
  </div>
  <div class="overlay js-overlay sp-only"></div>
</div><!-- ./blog-nav -->
<nav class="breadcrumbs ts-contain" id="breadcrumb">
  <div class="inner wrap" breadcrumbs ts-contain>
    <span>
      <a href="http://sanfuji-media.f-demo.com/">
        <span>TOP</span>
      </a>
    </span> 
    <span class="delim"><i class="tsi tsi-angle-right"></i></span> 
    <span>
      <a href="http://sanfuji-media.f-demo.com/">
        <span>カテゴリ</span>
      </a>
    </span> 
    <span class="delim"><i class="tsi tsi-angle-right"></i></span> 
    <span class="current">記事タイトル</span>
  </div>
</nav>
<div class="main wrap">
  <div class="blocks">
  </div>
  <div class="ts-row cf p-notfoundWrap">
    <aside class="col-4 sidebar" data-sticky="1">
      <div class="inner  theiaStickySidebar">
        <ul>
          <li id="search-2" class="widget widget_search">
            <form method="get" class="search-form" action="http://sanfuji-media.f-demo.com/">
              <label>
                <span class="screen-reader-text">Search for:</span>
                <input type="search" class="search-field" placeholder="フリーワードを記入してください" value="" name="s" title="Search for:" />
              </label>
              <button type="submit" class="search-submit"><i class="tsi tsi-search"></i></button>
            </form>
          </li>
          <li id="wpp-2" class="widget popular-posts">
            <h5 class="widget-title block-head-widget has-style"><span class="title">RANKING</span></h5><!-- cached -->
            <!-- WordPress Popular Posts -->
            <ul class="wpp-list wpp-cards">
              <li><img src="http://sanfuji-media.f-demo.com/wp-content/uploads/wordpress-popular-posts/35-featured-124x83.png" width="124" height="83" alt="" class="wpp-thumbnail wpp_featured wpp_cached_thumb" loading="lazy" />
                <div class="wpp-item-data">
                  <div class="taxonomies"></div><a href="http://sanfuji-media.f-demo.com/?p=35" class="wpp-post-title" target="_self">分譲住宅のメリット・デメリット</a>
                  <p class="wpp-excerpt">JUNE 18 , 2019</p>
                </div>
              </li>
              <li><img src="http://sanfuji-media.f-demo.com/wp-content/uploads/wordpress-popular-posts/31-featured-124x83.png" width="124" height="83" alt="" class="wpp-thumbnail wpp_featured wpp_cached_thumb" loading="lazy" />
                <div class="wpp-item-data">
                  <div class="taxonomies"></div><a href="http://sanfuji-media.f-demo.com/?p=31" class="wpp-post-title" target="_self">住宅ローンを利用して夢のマイホームを</a>
                  <p class="wpp-excerpt">JUNE 18 , 2019</p>
                </div>
              </li>
              <li><img src="http://sanfuji-media.f-demo.com/wp-content/uploads/wordpress-popular-posts/32-featured-124x83.png" width="124" height="83" alt="" class="wpp-thumbnail wpp_featured wpp_cached_thumb" loading="lazy" />
                <div class="wpp-item-data">
                  <div class="taxonomies"></div><a href="http://sanfuji-media.f-demo.com/?p=32" class="wpp-post-title" target="_self">分譲住宅のメリット・デメリット</a>
                  <p class="wpp-excerpt">JUNE 18 , 2019</p>
                </div>
              </li>
              <li><img src="http://sanfuji-media.f-demo.com/wp-content/uploads/wordpress-popular-posts/33-featured-124x83.png" width="124" height="83" alt="" class="wpp-thumbnail wpp_featured wpp_cached_thumb" loading="lazy" />
                <div class="wpp-item-data">
                  <div class="taxonomies"></div><a href="http://sanfuji-media.f-demo.com/?p=33" class="wpp-post-title" target="_self">都内のタワーマンション人気ランキング</a>
                  <p class="wpp-excerpt">JUNE 18 , 2019</p>
                </div>
              </li>
              <li><img src="http://sanfuji-media.f-demo.com/wp-content/uploads/wordpress-popular-posts/34-featured-124x83.png" width="124" height="83" alt="" class="wpp-thumbnail wpp_featured wpp_cached_thumb" loading="lazy" />
                <div class="wpp-item-data">
                  <div class="taxonomies"></div><a href="http://sanfuji-media.f-demo.com/?p=34" class="wpp-post-title" target="_self">住宅ローンを利用して夢のマイホームを</a>
                  <p class="wpp-excerpt">JUNE 18 , 2019</p>
                </div>
              </li>
            </ul>
          </li>
          <li id="bunyad-posts-widget-2" class="widget widget-posts">
            <h5 class="widget-title block-head-widget has-style"><span class="title">latest article</span></h5>
            <ul class="posts cf meta-below">
              <li class="post cf">
                <div class="post-thumb">
                  <a href="http://sanfuji-media.f-demo.com/?p=35" class="image-link media-ratio ar-cheerup-thumb"><span data-bgsrc="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png" class="img bg-cover wp-post-image attachment-large size-large lazyload" data-bgset="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png 762w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-300x200.png 300w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-175x117.png 175w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-450x301.png 450w" data-sizes="(max-width: 87px) 100vw, 87px"></span></a> </div>
                <div class="content">
                  <div class="post-meta post-meta-b post-meta-left has-below">
                    <h4 class="is-title post-title"><a href="http://sanfuji-media.f-demo.com/?p=35">都内のタワーマンション人気ランキング</a></h4>
                    <div class="below meta-below"><a href="http://sanfuji-media.f-demo.com/?p=35" class="meta-item date-link">
                        <time class="post-date" datetime="2021-02-09T03:31:45+09:00">JUNE 18 , 2019</time>
                      </a></div>
                  </div>
                </div>
              </li>
              <li class="post cf">
                <div class="post-thumb">
                  <a href="http://sanfuji-media.f-demo.com/?p=34" class="image-link media-ratio ar-cheerup-thumb"><span data-bgsrc="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png" class="img bg-cover wp-post-image attachment-large size-large lazyload" data-bgset="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png 762w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-300x200.png 300w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-175x117.png 175w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-450x301.png 450w" data-sizes="(max-width: 87px) 100vw, 87px"></span></a> </div>
                <div class="content">
                  <div class="post-meta post-meta-b post-meta-left has-below">
                    <h4 class="is-title post-title"><a href="http://sanfuji-media.f-demo.com/?p=34">分譲住宅のメリット・デメリット</a></h4>
                    <div class="below meta-below"><a href="http://sanfuji-media.f-demo.com/?p=34" class="meta-item date-link">
                        <time class="post-date" datetime="2021-02-09T03:31:43+09:00">JUNE 18 , 2019</time>
                      </a></div>
                  </div>
                </div>
              </li>
              <li class="post cf">
                <div class="post-thumb">
                  <a href="http://sanfuji-media.f-demo.com/?p=33" class="image-link media-ratio ar-cheerup-thumb"><span data-bgsrc="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png" class="img bg-cover wp-post-image attachment-large size-large lazyload" data-bgset="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png 762w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-300x200.png 300w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-175x117.png 175w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-450x301.png 450w" data-sizes="(max-width: 87px) 100vw, 87px"></span></a> </div>
                <div class="content">
                  <div class="post-meta post-meta-b post-meta-left has-below">
                    <h4 class="is-title post-title"><a href="http://sanfuji-media.f-demo.com/?p=33">住宅ローンを利用して夢のマイホームを</a></h4>
                    <div class="below meta-below"><a href="http://sanfuji-media.f-demo.com/?p=33" class="meta-item date-link">
                        <time class="post-date" datetime="2021-02-09T03:31:41+09:00">JUNE 18 , 2019</time>
                      </a></div>
                  </div>
                </div>
              </li>
            </ul>
          </li>
          <li id="bunyad-posts-widget-2" class="widget widget-posts">
            <h5 class="widget-title block-head-widget has-style"><span class="title">Recommend</span></h5>
            <ul class="posts cf meta-below">
              <li class="post cf">
                <div class="post-thumb">
                  <a href="http://sanfuji-media.f-demo.com/?p=35" class="image-link media-ratio ar-cheerup-thumb"><span data-bgsrc="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png" class="img bg-cover wp-post-image attachment-large size-large lazyload" data-bgset="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png 762w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-300x200.png 300w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-175x117.png 175w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-450x301.png 450w" data-sizes="(max-width: 87px) 100vw, 87px"></span></a> </div>
                <div class="content">
                  <div class="post-meta post-meta-b post-meta-left has-below">
                    <h4 class="is-title post-title"><a href="http://sanfuji-media.f-demo.com/?p=35">都内のタワーマンション人気ランキング</a></h4>
                    <div class="below meta-below"><a href="http://sanfuji-media.f-demo.com/?p=35" class="meta-item date-link">
                        <time class="post-date" datetime="2021-02-09T03:31:45+09:00">JUNE 18 , 2019</time>
                      </a></div>
                  </div>
                </div>
              </li>
              <li class="post cf">
                <div class="post-thumb">
                  <a href="http://sanfuji-media.f-demo.com/?p=34" class="image-link media-ratio ar-cheerup-thumb"><span data-bgsrc="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png" class="img bg-cover wp-post-image attachment-large size-large lazyload" data-bgset="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png 762w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-300x200.png 300w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-175x117.png 175w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-450x301.png 450w" data-sizes="(max-width: 87px) 100vw, 87px"></span></a> </div>
                <div class="content">
                  <div class="post-meta post-meta-b post-meta-left has-below">
                    <h4 class="is-title post-title"><a href="http://sanfuji-media.f-demo.com/?p=34">住宅ローンを利用して夢のマイホームを</a></h4>
                    <div class="below meta-below"><a href="http://sanfuji-media.f-demo.com/?p=34" class="meta-item date-link">
                        <time class="post-date" datetime="2021-02-09T03:31:43+09:00">JUNE 18 , 2019</time>
                      </a></div>
                  </div>
                </div>
              </li>
              <li class="post cf">
                <div class="post-thumb">
                  <a href="http://sanfuji-media.f-demo.com/?p=33" class="image-link media-ratio ar-cheerup-thumb"><span data-bgsrc="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png" class="img bg-cover wp-post-image attachment-large size-large lazyload" data-bgset="http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img.png 762w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-300x200.png 300w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-175x117.png 175w, http://sanfuji-media.f-demo.com/wp-content/uploads/2021/02/img-450x301.png 450w" data-sizes="(max-width: 87px) 100vw, 87px"></span></a> </div>
                <div class="content">
                  <div class="post-meta post-meta-b post-meta-left has-below">
                    <h4 class="is-title post-title"><a href="http://sanfuji-media.f-demo.com/?p=33">都内のタワーマンション人気ランキング</a></h4>
                    <div class="below meta-below"><a href="http://sanfuji-media.f-demo.com/?p=33" class="meta-item date-link">
                        <time class="post-date" datetime="2021-02-09T03:31:41+09:00">JUNE 18 , 2019</time>
                      </a></div>
                  </div>
                </div>
              </li>
            </ul>
          </li>
          <li id="categories-3" class="widget widget_categories">
            <h5 class="widget-title block-head-widget has-style"><span class="title">CATEGORY</span></h5>
            <ul>
              <li class="cat-item cat-item-8"><a href="http://sanfuji-media.f-demo.com/?cat=8">イベント</a>
              </li>
              <li class="cat-item cat-item-5"><a href="http://sanfuji-media.f-demo.com/?cat=5">インテリア</a>
              </li>
              <li class="cat-item cat-item-4"><a href="http://sanfuji-media.f-demo.com/?cat=4">ライフスタイル・暮らし</a>
              </li>
            </ul>
          </li>
          <li id="tag_cloud-2" class="widget widget_tag_cloud">
            <h5 class="widget-title block-head-widget has-style"><span class="title">#TAG</span></h5>
            <div class="tagcloud"><a href="http://sanfuji-media.f-demo.com/?tag=%e3%82%b9%e3%83%8e%e3%83%bc%e3%83%89%e3%83%bc%e3%83%a0" class="tag-cloud-link tag-link-15 tag-link-position-1" style="font-size: 8pt;" aria-label="スノードーム (1個の項目)">#スノードーム</a>
              <a href="http://sanfuji-media.f-demo.com/?tag=%e3%83%9a%e3%83%83%e3%83%88%e3%81%8a%e3%82%82%e3%81%a1%e3%82%83" class="tag-cloud-link tag-link-9 tag-link-position-2" style="font-size: 22pt;" aria-label="ペットおもちゃ (3個の項目)">#ペットおもちゃ</a></div>
          </li>
          <li id="custom_html-2" class="widget_text widget widget_custom_html">
            <div class="textwidget custom-html-widget">
              <div>
                <div class="blog-advers">
                  <a href=""><img src="<?php echo $PATH;?>/assets/images/blog/advers01.png" alt=""></a>
                </div>
                <div class="blog-sns">
                  <h5 class="widget-title block-head-widget has-style"><span class="title">FOLLOW ME !</span></h5>
                  <div class="blog-sns--labelWrap">
                    <p class="blog-sns--label">TWITTER</p>
                  </div>
                  <img src="<?php echo $PATH;?>/assets/images/blog/twitter.png" alt="">
                </div>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </aside>
    <div class="col-8 main-content cf">
      <div class="p-notfound">
        <p class="p-notfound--ttl">お探しの記事は見つかりません</p>
        <p class="desc2">申し訳ありません。お探しの記事は見つかりませんでした。<br> 一時的にアクセスできない状況にあるか移動もしくは削除された可能性があります。<br> 下記の検索フォームにてお求めの記事をお探しください。</p>
        <div class="p-notfound--form">
          <form action="">
            <input type="text" placeholder="フリーワードを記入してください">
            <input type="submit" value="検索">
          </form>
        </div>
      </div>
    </div>
  </div>
</div> <!-- .main -->
<div class="blog-contact">
  <a class="link" href="/contact">CONTACT</a>
</div>
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer-blog.php'; ?>