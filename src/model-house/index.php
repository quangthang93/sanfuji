<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="ttl-epWrap">
    <div class="container">
      <h1 class="ttl-ep">モデルハウスを探す</h1>
    </div>
  </div>
  <div class="breadcrumb">
    <div class="breadcrumb-inner">
      <ul>
        <li><a href="/"><span class="icon-home"></span></a></li>
        <li><a href="/">モデルハウスを探す</a></li>
        <li>条件から選ぶ</li>
      </ul>
    </div>
  </div><!-- ./breadcrumb -->
  <div class="p-end">
    <div class="container">
      <div class="p-modelHouse">
        <div class="section-search2--cnt">
          <div class="tabs">
            <div class="tabs-navWrapper">
              <ul class="tabs-nav type2 js-tabsNav">
                <li class="tabs-item type2 js-tabsItem active">ハウスメーカーから選ぶ</li>
                <li class="tabs-item type2 js-tabsItem">条件から選ぶ</li>
              </ul>
            </div>
            <div class="tabs-cnt js-tabsCnt">
              <div class="tabs-panel type2 js-tabsPanel">
                <ul class="tabs2">
                  <li class="js-choice">あ</li>
                  <li class="js-choice">か</li>
                  <li class="js-choice">さ</li>
                  <li class="js-choice">た</li>
                  <li class="js-choice">な</li>
                  <li class="js-choice">は</li>
                  <li class="js-choice active">ま</li>
                  <li class="js-choice">や</li>
                  <li class="js-choice">ら</li>
                  <li class="js-choice">わ</li>
                </ul>
                <form action="">
                  <div class="checkbox-listWrap">
                    <ul class="checkbox-list">
                      <li>
                        <input class="checkbox2" id="cbox05-1" type="checkbox" value="value1">
                        <label for="cbox05-1">マツシタホーム</label>
                      </li>
                      <li>
                        <input class="checkbox2" id="cbox05-2" type="checkbox" value="value1">
                        <label for="cbox05-2">丸和住宅</label>
                      </li>
                      <li>
                        <input class="checkbox2" id="cbox05-3" type="checkbox" value="value1">
                        <label for="cbox05-3">マレアハウスデザイン</label>
                      </li>
                    
                      <li>
                        <input class="checkbox2" id="cbox05-4" type="checkbox" value="value1">
                        <label for="cbox05-4">ミサワホーム</label>
                      </li>
                      <li>
                        <input class="checkbox2" id="cbox05-5" type="checkbox" value="value1">
                        <label for="cbox05-5">三井ホーム</label>
                      </li>
                      <li>
                        <input class="checkbox2" id="cbox05-6" type="checkbox" value="value1">
                        <label for="cbox05-6">三菱地所ホーム</label>
                      </li>
                      <li>
                        <input class="checkbox2" id="cbox05-7" type="checkbox" value="value1">
                        <label for="cbox05-7">望月建業</label>
                      </li>
                      <li>
                        <input class="checkbox2" id="cbox05-8" type="checkbox" value="value1">
                        <label for="cbox05-8">もりぞう</label>
                      </li>
                    </ul>
                  </div>
                  <div class="p-modelHouse--btnGroup">
                    <a class="btn-reset" href=""><span>リセット</span></a>
                    <a class="btn-submit" href=""><span>検索する</span></a>
                  </div>
                </form>
              </div>
              <div class="tabs-panel type3 js-tabsPanel">
                <form action="">
                  <div class="checkbox-listWrap">
                    <div class="checkbox-list--row">
                      <p class="checkbox-list--ttl">工法</p>
                      <ul class="checkbox-list type2 type4">
                        <li>
                          <input class="checkbox2" id="cbox06-1" type="checkbox" value="value1">
                          <label for="cbox06-1">木造軸組工法</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox06-2" type="checkbox" value="value1">
                          <label for="cbox06-2">鉄骨系プレハブ工法</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox06-3" type="checkbox" value="value1">
                          <label for="cbox06-3">2×4工法</label>
                        </li>
                      
                        <li>
                          <input class="checkbox2" id="cbox06-4" type="checkbox" value="value1">
                          <label for="cbox06-4">2×6工法</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox06-5" type="checkbox" value="value1">
                          <label for="cbox06-5">鉄骨＋ALC工法</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox06-6" type="checkbox" value="value1">
                          <label for="cbox06-6">木質系プレハブ工法</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox06-7" type="checkbox" value="value1">
                          <label for="cbox06-7">コンクリート系プレハブ工法</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox06-8" type="checkbox" value="value1">
                          <label for="cbox06-8">ユニット系プレハブ工法</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox06-9" type="checkbox" value="value1">
                          <label for="cbox06-9">ユニット工法</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox06-10" type="checkbox" value="value1">
                          <label for="cbox06-10">その他</label>
                        </li>
                      </ul>
                    </div>
                    <div class="checkbox-list--row">
                      <p class="checkbox-list--ttl type2">仕様</p>
                      <ul class="checkbox-list type2 type4">
                        <li>
                          <input class="checkbox2" id="cbox07-1" type="checkbox" value="value1">
                          <label for="cbox07-1">2階建て</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox07-2" type="checkbox" value="value1">
                          <label for="cbox07-2">3階建て以上</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox07-3" type="checkbox" value="value1">
                          <label for="cbox07-3">単世帯</label>
                        </li>
                      
                        <li>
                          <input class="checkbox2" id="cbox07-4" type="checkbox" value="value1">
                          <label for="cbox07-4">二世帯</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox07-5" type="checkbox" value="value1">
                          <label for="cbox07-5">長期優良住宅</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox07-6" type="checkbox" value="value1">
                          <label for="cbox07-6">ネットゼロエネルギーハウス</label>
                        </li>
                      </ul>
                    </div>
                    <div class="checkbox-list--row">
                      <p class="checkbox-list--ttl type3">こだわり</p>
                      <ul class="checkbox-list type2 type4">
                        <li>
                          <input class="checkbox2" id="cbox08-1" type="checkbox" value="value1">
                          <label for="cbox08-1">システムキッチンがある</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-2" type="checkbox" value="value1">
                          <label for="cbox08-2">安心のセキュリティ</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-3" type="checkbox" value="value1">
                          <label for="cbox08-3">防音・断熱・通気性</label>
                        </li>
                      
                        <li>
                          <input class="checkbox2" id="cbox08-4" type="checkbox" value="value1">
                          <label for="cbox08-4">和室と洋室がある</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-5" type="checkbox" value="value1">
                          <label for="cbox08-5">書斎</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-6" type="checkbox" value="value1">
                          <label for="cbox08-6">収納スペース広々</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-7" type="checkbox" value="value1">
                          <label for="cbox08-7">カーポート設置可</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-8" type="checkbox" value="value1">
                          <label for="cbox08-8">吹き抜け空間</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-9" type="checkbox" value="value1">
                          <label for="cbox08-9">板張り天井</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-10" type="checkbox" value="value1">
                          <label for="cbox08-10">スキップフロア</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-11" type="checkbox" value="value1">
                          <label for="cbox08-11">IoT対応</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-12" type="checkbox" value="value1">
                          <label for="cbox08-12">シアタールーム</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-13" type="checkbox" value="value1">
                          <label for="cbox08-13">二重窓</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-14" type="checkbox" value="value1">
                          <label for="cbox08-14">アイランドキッチン</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-15" type="checkbox" value="value1">
                          <label for="cbox08-15">ホームエレベーター</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-16" type="checkbox" value="value1">
                          <label for="cbox08-16">地下収納</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-17" type="checkbox" value="value1">
                          <label for="cbox08-17">ウッドデッキ</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-18" type="checkbox" value="value1">
                          <label for="cbox08-18">太陽光発電</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-19" type="checkbox" value="value1">
                          <label for="cbox08-19">家庭用蓄電池</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox08-20" type="checkbox" value="value1">
                          <label for="cbox08-20">オール電化</label>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="p-modelHouse--btnGroup">
                    <a class="btn-reset" href=""><span>リセット</span></a>
                    <a class="btn-submit" href=""><span>検索する</span></a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>