<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="ttl-epWrap">
    <div class="container">
      <h1 class="ttl-ep">モデルハウスを探す</h1>
    </div>
  </div>
  <div class="breadcrumb">
    <div class="breadcrumb-inner">
      <ul>
        <li><a href="/"><span class="icon-home"></span></a></li>
        <li><a href="/">モデルハウスを探す</a></li>
        <li>検索結果</li>
      </ul>
    </div>
  </div><!-- ./breadcrumb -->
  <div class="p-end type2">
    <div class="container">
      <div class="p-modelHouse">
        <div class="content-resultWrap">
          <p class="content-result--counter">該当のモデルハウス:<span class="count">12</span><span class="unit">件</span></p>
          <div class="content-result--sidebar-condition sp-only3">
            <p class="content-result--sidebar-condition-ttl js-modelHouseAc">現在の検索条件</p>
            <ul  class="js-modelHouseAcCnt">
              <li>横浜北部・川崎エリア</li>
              <li>三井ホーム</li>
              <li>コンクリート系プレハブ工法</li>
              <li>長期優良住宅</li>
              <li>システムキッチンがある</li>
            </ul>
          </div>
          <div class="content-result">
            <div class="content-result--sidebar">
              <form action="">
              <div class="content-result--sidebar-condition pc-only3">
                <p class="content-result--sidebar-condition-ttl js-modelHouseAc">現在の検索条件</p>
                <ul>
                  <li>横浜北部・川崎エリア</li>
                  <li>三井ホーム</li>
                  <li>コンクリート系プレハブ工法</li>
                  <li>長期優良住宅</li>
                  <li>システムキッチンがある</li>
                </ul>
              </div>
              <div class="form-area type2">
                <div class="form-area-labelWrap js-modelHouseAc">
                  <p class="form-area-label">モデルハウスを探す</p>
                </div>
                <div class="js-modelHouseAcCn">
                  <div class="p-modelHouse--row">
                    <p class="p-modelHouse--row-ttl link js-modelHouseAc">エリアを選ぶ</p>
                    <div class="p-modelHouse--row-cnt js-modelHouseAcCnt">
                      <ul class="checkbox-list type2 type3">
                        <li>
                          <input class="checkbox2" id="cbox00-1" type="checkbox" value="value1" checked="checked">
                          <label for="cbox00-1">横浜北部・川崎エリア</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox00-2" type="checkbox" value="value1">
                          <label for="cbox00-2">横浜南部・横須賀エリア</label>
                        </li>
                        <li>
                          <input class="checkbox2" id="cbox00-3" type="checkbox" value="value1">
                          <label for="cbox00-3">湘南・西湘エリア</label>
                        </li>
                      
                        <li>
                          <input class="checkbox2" id="cbox00-4" type="checkbox" value="value1">
                          <label for="cbox00-4">相模原・県央エリア</label>
                        </li>
                      </ul>
                    </div>
                  </div><!-- ./p-modelHouse--row -->
                  <div class="p-modelHouse--row">
                    <a href="javascript:void(0)" class="p-modelHouse--row-ttl link type2 js-openModalHouse" data-remodal-target="modal">ハウスメーカーから選ぶ</a>
                  </div><!-- ./p-modelHouse--row -->
                  
                  <div class="p-modelHouse--row">
                    <p class="p-modelHouse--row-ttl type3 link js-modelHouseAc">条件から選ぶ</p>
                    <div class="p-modelHouse--row-cnt js-modelHouseAcCnt">
                      <div class="checkbox-listWrap">
                        <div class="checkbox-list--row">
                          <p class="checkbox-list--ttl">工法</p>
                          <ul class="checkbox-list type2 type3">
                            <li>
                              <input class="checkbox2" id="cbox06-1" type="checkbox" value="value1">
                              <label for="cbox06-1">木造軸組工法</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox06-2" type="checkbox" value="value1">
                              <label for="cbox06-2">鉄骨系プレハブ工法</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox06-3" type="checkbox" value="value1">
                              <label for="cbox06-3">2×4工法</label>
                            </li>
                          
                            <li>
                              <input class="checkbox2" id="cbox06-4" type="checkbox" value="value1">
                              <label for="cbox06-4">2×6工法</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox06-5" type="checkbox" value="value1">
                              <label for="cbox06-5">鉄骨＋ALC工法</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox06-6" type="checkbox" value="value1">
                              <label for="cbox06-6">木質系プレハブ工法</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox06-7" type="checkbox" value="value1" checked="checked">
                              <label for="cbox06-7">コンクリート系プレハブ工法</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox06-8" type="checkbox" value="value1">
                              <label for="cbox06-8">ユニット系プレハブ工法</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox06-9" type="checkbox" value="value1">
                              <label for="cbox06-9">ユニット工法</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox06-10" type="checkbox" value="value1">
                              <label for="cbox06-10">その他</label>
                            </li>
                          </ul>
                        </div>
                        <div class="checkbox-list--row">
                          <p class="checkbox-list--ttl type2">仕様</p>
                          <ul class="checkbox-list type2 type3">
                            <li>
                              <input class="checkbox2" id="cbox07-1" type="checkbox" value="value1">
                              <label for="cbox07-1">2階建て</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox07-2" type="checkbox" value="value1">
                              <label for="cbox07-2">3階建て以上</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox07-3" type="checkbox" value="value1">
                              <label for="cbox07-3">単世帯</label>
                            </li>
                          
                            <li>
                              <input class="checkbox2" id="cbox07-4" type="checkbox" value="value1">
                              <label for="cbox07-4">二世帯</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox07-5" type="checkbox" value="value1" checked="checked">
                              <label for="cbox07-5">長期優良住宅</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox07-6" type="checkbox" value="value1">
                              <label for="cbox07-6">ネットゼロエネルギーハウス</label>
                            </li>
                          </ul>
                        </div>
                        <div class="checkbox-list--row">
                          <p class="checkbox-list--ttl type3">こだわり</p>
                          <ul class="checkbox-list type2 type3">
                            <li>
                              <input class="checkbox2" id="cbox08-1" type="checkbox" value="value1" checked="checked">
                              <label for="cbox08-1">システムキッチンがある</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox08-2" type="checkbox" value="value1">
                              <label for="cbox08-2">安心のセキュリティ</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox08-3" type="checkbox" value="value1">
                              <label for="cbox08-3">防音・断熱・通気性</label>
                            </li>
                          
                            <li>
                              <input class="checkbox2" id="cbox08-4" type="checkbox" value="value1">
                              <label for="cbox08-4">和室と洋室がある</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox08-5" type="checkbox" value="value1">
                              <label for="cbox08-5">書斎</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox08-6" type="checkbox" value="value1">
                              <label for="cbox08-6">収納スペース広々</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox08-7" type="checkbox" value="value1">
                              <label for="cbox08-7">カーポート設置可</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox08-8" type="checkbox" value="value1">
                              <label for="cbox08-8">吹き抜け空間</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox08-9" type="checkbox" value="value1">
                              <label for="cbox08-9">板張り天井</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox08-10" type="checkbox" value="value1">
                              <label for="cbox08-10">スキップフロア</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox08-11" type="checkbox" value="value1">
                              <label for="cbox08-11">IoT対応</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox08-12" type="checkbox" value="value1">
                              <label for="cbox08-12">シアタールーム</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox08-13" type="checkbox" value="value1">
                              <label for="cbox08-13">二重窓</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox08-14" type="checkbox" value="value1">
                              <label for="cbox08-14">アイランドキッチン</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox08-15" type="checkbox" value="value1">
                              <label for="cbox08-15">ホームエレベーター</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox08-16" type="checkbox" value="value1">
                              <label for="cbox08-16">地下収納</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox08-17" type="checkbox" value="value1">
                              <label for="cbox08-17">ウッドデッキ</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox08-18" type="checkbox" value="value1">
                              <label for="cbox08-18">太陽光発電</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox08-19" type="checkbox" value="value1">
                              <label for="cbox08-19">家庭用蓄電池</label>
                            </li>
                            <li>
                              <input class="checkbox2" id="cbox08-20" type="checkbox" value="value1">
                              <label for="cbox08-20">オール電化</label>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div><!-- ./p-modelHouse--row -->
                  <div class="form-area-btn">
                    <!-- <a class="btn-reset" href=""><span>リセット</span></a> -->
                    <a href="" class="btn-reset2">選択したすべてのチェックをリセットする</a>
                    <a class="btn-submit type2" href=""><span>検索する</span></a>
                  </div>
                </div>
              </div>
              </form>
            </div><!-- ./content-result--sidebar -->
            <div class="content-result--cnt">
              <ul class="list-result type2">
                <li>
                  <a class="link" href="/model-house/detail">
                    <div class="list-result-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/booking/booking01.png" alt="">
                    </div>
                    <div class="list-result-cnt">
                      <p class="cat5">三井ホーム</p>  
                      <h4 class="list-result-cnt-ttl">LANGLEY</h4>
                      <div class="list-result-cnt-table">
                        <table>
                          <tr>
                            <th>電話番号</th>
                            <td>03-3247-3641</td>
                          </tr>
                          <tr>
                            <th>展示場</th>
                            <td>新百合ヶ丘ハウジングギャラリー</td>
                          </tr>
                        </table>
                      </div>
                    </div>
                  </a>
                </li>
                <li>
                  <a class="link" href="/model-house/detail">
                    <div class="list-result-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/booking/booking02.png" alt="">
                    </div>
                    <div class="list-result-cnt">
                      <p class="cat5">三井ホーム</p>  
                      <h4 class="list-result-cnt-ttl">Avan-Corte</h4>
                      <div class="list-result-cnt-table">
                        <table>
                          <tr>
                            <th>電話番号</th>
                            <td>03-3247-3641</td>
                          </tr>
                          <tr>
                            <th>展示場</th>
                            <td>武蔵小杉住宅展示場</td>
                          </tr>
                        </table>
                      </div>
                    </div>
                  </a>
                </li>
                <li>
                  <a class="link" href="/model-house/detail">
                    <div class="list-result-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/booking/booking03.png" alt="">
                    </div>
                    <div class="list-result-cnt">
                      <p class="cat5">三井ホーム</p>  
                      <h4 class="list-result-cnt-ttl">GranFree</h4>
                      <div class="list-result-cnt-table">
                        <table>
                          <tr>
                            <th>電話番号</th>
                            <td>03-3247-3641</td>
                          </tr>
                          <tr>
                            <th>展示場</th>
                            <td>tvkハウジングプラザ新百合ヶ丘</td>
                          </tr>
                        </table>
                      </div>
                    </div>
                  </a>
                </li>
                <li>
                  <a class="link" href="/model-house/detail">
                    <div class="list-result-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/booking/booking04.png" alt="">
                    </div>
                    <div class="list-result-cnt">
                      <p class="cat5">三井ホーム</p>  
                      <h4 class="list-result-cnt-ttl">Lucas</h4>
                      <div class="list-result-cnt-table">
                        <table>
                          <tr>
                            <th>電話番号</th>
                            <td>03-3247-3641</td>
                          </tr>
                          <tr>
                            <th>展示場</th>
                            <td>ABCハウジング 新・川崎住宅公園</td>
                          </tr>
                        </table>
                      </div>
                    </div>
                  </a>
                </li>
                <li>
                  <a class="link" href="/model-house/detail">
                    <div class="list-result-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/booking/booking01.png" alt="">
                    </div>
                    <div class="list-result-cnt">
                      <p class="cat5">三井ホーム</p>  
                      <h4 class="list-result-cnt-ttl">LANGLEY</h4>
                      <div class="list-result-cnt-table">
                        <table>
                          <tr>
                            <th>電話番号</th>
                            <td>03-3247-3641</td>
                          </tr>
                          <tr>
                            <th>展示場</th>
                            <td>新百合ヶ丘ハウジングギャラリー</td>
                          </tr>
                        </table>
                      </div>
                    </div>
                  </a>
                </li>
                <li>
                  <a class="link" href="/model-house/detail">
                    <div class="list-result-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/booking/booking02.png" alt="">
                    </div>
                    <div class="list-result-cnt">
                      <p class="cat5">三井ホーム</p>  
                      <h4 class="list-result-cnt-ttl">Avan-Corte</h4>
                      <div class="list-result-cnt-table">
                        <table>
                          <tr>
                            <th>電話番号</th>
                            <td>03-3247-3641</td>
                          </tr>
                          <tr>
                            <th>展示場</th>
                            <td>武蔵小杉住宅展示場</td>
                          </tr>
                        </table>
                      </div>
                    </div>
                  </a>
                </li>
                <li>
                  <a class="link" href="/model-house/detail">
                    <div class="list-result-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/booking/booking03.png" alt="">
                    </div>
                    <div class="list-result-cnt">
                      <p class="cat5">三井ホーム</p>  
                      <h4 class="list-result-cnt-ttl">GranFree</h4>
                      <div class="list-result-cnt-table">
                        <table>
                          <tr>
                            <th>電話番号</th>
                            <td>03-3247-3641</td>
                          </tr>
                          <tr>
                            <th>展示場</th>
                            <td>tvkハウジングプラザ新百合ヶ丘</td>
                          </tr>
                        </table>
                      </div>
                    </div>
                  </a>
                </li>
                <li>
                  <a class="link" href="/model-house/detail">
                    <div class="list-result-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/booking/booking04.png" alt="">
                    </div>
                    <div class="list-result-cnt">
                      <p class="cat5">三井ホーム</p>  
                      <h4 class="list-result-cnt-ttl">Lucas</h4>
                      <div class="list-result-cnt-table">
                        <table>
                          <tr>
                            <th>電話番号</th>
                            <td>03-3247-3641</td>
                          </tr>
                          <tr>
                            <th>展示場</th>
                            <td>ABCハウジング 新・川崎住宅公園</td>
                          </tr>
                        </table>
                      </div>
                    </div>
                  </a>
                </li>
                <li>
                  <a class="link" href="/model-house/detail">
                    <div class="list-result-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/booking/booking01.png" alt="">
                    </div>
                    <div class="list-result-cnt">
                      <p class="cat5">三井ホーム</p>  
                      <h4 class="list-result-cnt-ttl">LANGLEY</h4>
                      <div class="list-result-cnt-table">
                        <table>
                          <tr>
                            <th>電話番号</th>
                            <td>03-3247-3641</td>
                          </tr>
                          <tr>
                            <th>展示場</th>
                            <td>新百合ヶ丘ハウジングギャラリー</td>
                          </tr>
                        </table>
                      </div>
                    </div>
                  </a>
                </li>
                <li>
                  <a class="link" href="/model-house/detail">
                    <div class="list-result-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/booking/booking02.png" alt="">
                    </div>
                    <div class="list-result-cnt">
                      <p class="cat5">三井ホーム</p>  
                      <h4 class="list-result-cnt-ttl">Avan-Corte</h4>
                      <div class="list-result-cnt-table">
                        <table>
                          <tr>
                            <th>電話番号</th>
                            <td>03-3247-3641</td>
                          </tr>
                          <tr>
                            <th>展示場</th>
                            <td>武蔵小杉住宅展示場</td>
                          </tr>
                        </table>
                      </div>
                    </div>
                  </a>
                </li>
                <li>
                  <a class="link" href="/model-house/detail">
                    <div class="list-result-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/booking/booking03.png" alt="">
                    </div>
                    <div class="list-result-cnt">
                      <p class="cat5">三井ホーム</p>  
                      <h4 class="list-result-cnt-ttl">GranFree</h4>
                      <div class="list-result-cnt-table">
                        <table>
                          <tr>
                            <th>電話番号</th>
                            <td>03-3247-3641</td>
                          </tr>
                          <tr>
                            <th>展示場</th>
                            <td>tvkハウジングプラザ新百合ヶ丘</td>
                          </tr>
                        </table>
                      </div>
                    </div>
                  </a>
                </li>
                <li>
                  <a class="link" href="/model-house/detail">
                    <div class="list-result-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/booking/booking04.png" alt="">
                    </div>
                    <div class="list-result-cnt">
                      <p class="cat5">三井ホーム</p>  
                      <h4 class="list-result-cnt-ttl">Lucas</h4>
                      <div class="list-result-cnt-table">
                        <table>
                          <tr>
                            <th>電話番号</th>
                            <td>03-3247-3641</td>
                          </tr>
                          <tr>
                            <th>展示場</th>
                            <td>ABCハウジング 新・川崎住宅公園</td>
                          </tr>
                        </table>
                      </div>
                    </div>
                  </a>
                </li>
              </ul><!-- ./list-result -->
            </div>
          </div><!-- ./content-result -->
        </div><!-- ./content-resultWrap -->

        <div class="p-event--search type3 js-modalHouse" data-remodal-id="modal">
          <div class="p-event--search-inner">
            <!-- <span class="p-event--search-inner-close js-closeModalHouse"></span> -->
            <button data-remodal-action="close" class="remodal-close"></button>
            <p class="p-modelHouse--modal-ttl">ハウスメーカーから選ぶ</p>
            <div class="p-event--search-form">
              <ul class="tabs2">
                <li class="js-choice">あ</li>
                <li class="js-choice">か</li>
                <li class="js-choice">さ</li>
                <li class="js-choice">た</li>
                <li class="js-choice">な</li>
                <li class="js-choice">は</li>
                <li class="js-choice active">ま</li>
                <li class="js-choice">や</li>
                <li class="js-choice">ら</li>
                <li class="js-choice">わ</li>
              </ul>
              <form action="">
                <div class="checkbox-listWrap">
                  <ul class="checkbox-list">
                    <li>
                      <input class="checkbox2" id="cbox05-1" type="checkbox" value="value1">
                      <label for="cbox05-1">アイ工務店</label>
                    </li>
                    <li>
                      <input class="checkbox2" id="cbox05-2" type="checkbox" value="value1">
                      <label for="cbox05-2">アキュラホーム</label>
                    </li>
                    <li>
                      <input class="checkbox2" id="cbox05-3" type="checkbox" value="value1">
                      <label for="cbox05-3">旭化成ホームズ(HEBEL HAUS)</label>
                    </li>
                  
                    <li>
                      <input class="checkbox2" id="cbox05-4" type="checkbox" value="value1">
                      <label for="cbox05-4">一条工務店</label>
                    </li>
                    <li>
                      <input class="checkbox2" id="cbox05-5" type="checkbox" value="value1">
                      <label for="cbox05-5">ウィザースホーム</label>
                    </li>
                    <li>
                      <input class="checkbox2" id="cbox05-6" type="checkbox" value="value1">
                      <label for="cbox05-6">オープンハウス・アーキテクト</label>
                    </li>
                  </ul>
                </div>
                <div class="p-modelHouse--btnGroup">
                  <a class="btn-reset" href=""><span>リセット</span></a>
                  <a class="btn-submit" href=""><span>検索する</span></a>
                </div>
              </form>
            </div>
          </div>
        </div><!-- ./model-house modal -->
        <!-- <div class="p-modelHouse--modal-overlay js-modalOverlay"></div> -->
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>