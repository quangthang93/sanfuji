<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="ttl-ep2Wrap">
    <div class="container">
      <h1 class="ttl-ep3"><span>三井ホーム</span>LANGLEY</h1>
    </div>
  </div>
  <div class="breadcrumb">
    <div class="breadcrumb-inner">
      <ul>
        <li><a href="/"><span class="icon-home"></span></a></li>
        <li><a href="//model-house/">モデルハウスを探す</a></li>
        <li>LANGLEY</li>
      </ul>
    </div>
  </div><!-- ./breadcrumb -->
  <div class="slidersEP-wrapper">
    <div class="slidersEP-head">
      <div class="container">
        <div class="slidersEP-head--name">
          このモデルハウスがある展示場:<a href="" class="link2">tvkハウジングプラザ新百合ヶ丘</a>
        </div>
      </div>
    </div><!-- ./slidersEP-head -->
    <div class="slidersEP js-slidersEP">
      <div class="slidersEP-item">
        <div class="slidersEP-item-thumb">
          <img src="<?php echo $PATH;?>/assets/images/exhibition/pict01.png" alt="">
        </div>
      </div><!-- ./slidersEP--item -->
      <div class="slidersEP-item">
        <div class="slidersEP-item-thumb">
          <img src="<?php echo $PATH;?>/assets/images/exhibition/pict02.png" alt="">
        </div>
      </div><!-- ./slidersEP--item -->
      <div class="slidersEP-item">
        <div class="slidersEP-item-thumb">
          <img src="<?php echo $PATH;?>/assets/images/exhibition/pict03.png" alt="">
        </div>
      </div><!-- ./slidersEP--item -->
      <div class="slidersEP-item">
        <div class="slidersEP-item-thumb">
          <img src="<?php echo $PATH;?>/assets/images/exhibition/pict01.png" alt="">
        </div>
      </div><!-- ./slidersEP--item -->
      <div class="slidersEP-item">
        <div class="slidersEP-item-thumb">
          <img src="<?php echo $PATH;?>/assets/images/exhibition/pict02.png" alt="">
        </div>
      </div><!-- ./slidersEP--item -->
      <div class="slidersEP-item">
        <div class="slidersEP-item-thumb">
          <img src="<?php echo $PATH;?>/assets/images/exhibition/pict03.png" alt="">
        </div>
      </div><!-- ./slidersEP--item -->
      <div class="slidersEP-item">
        <div class="slidersEP-item-thumb">
          <img src="<?php echo $PATH;?>/assets/images/exhibition/pict01.png" alt="">
        </div>
      </div><!-- ./slidersEP--item -->
      <div class="slidersEP-item">
        <div class="slidersEP-item-thumb">
          <img src="<?php echo $PATH;?>/assets/images/exhibition/pict02.png" alt="">
        </div>
      </div><!-- ./slidersEP--item -->
      <div class="slidersEP-item">
        <div class="slidersEP-item-thumb">
          <img src="<?php echo $PATH;?>/assets/images/exhibition/pict03.png" alt="">
        </div>
      </div><!-- ./slidersEP--item -->
      <div class="slidersEP-item">
        <div class="slidersEP-item-thumb">
          <img src="<?php echo $PATH;?>/assets/images/exhibition/pict02.png" alt="">
        </div>
      </div><!-- ./slidersEP--item -->
    </div><!-- ./slidersEP -->
    <div class="slidersEP-navWrap">
      <div class="container">
        <div class="slidersEP-nav js-slidersEP-nav">
          <div class="slidersEP-nav--item">
            <div class="slidersEP-nav--item-thumb">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/slider01.png" alt="">
            </div>
          </div><!-- ./slidersEP--item -->
          <div class="slidersEP-nav--item">
            <div class="slidersEP-nav--item-thumb">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/slider03.png" alt="">
            </div>
          </div><!-- ./slidersEP--item -->
          <div class="slidersEP-nav--item">
            <div class="slidersEP-nav--item-thumb">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/slider01.png" alt="">
            </div>
          </div><!-- ./slidersEP--item -->
          <div class="slidersEP-nav--item">
            <div class="slidersEP-nav--item-thumb">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/slider02.png" alt="">
            </div>
          </div><!-- ./slidersEP--item -->
          <div class="slidersEP-nav--item">
            <div class="slidersEP-nav--item-thumb">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/slider01.png" alt="">
            </div>
          </div><!-- ./slidersEP--item -->
          <div class="slidersEP-nav--item">
            <div class="slidersEP-nav--item-thumb">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/slider02.png" alt="">
            </div>
          </div><!-- ./slidersEP--item -->
          <div class="slidersEP-nav--item">
            <div class="slidersEP-nav--item-thumb">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/slider01.png" alt="">
            </div>
          </div><!-- ./slidersEP--item -->
          <div class="slidersEP-nav--item">
            <div class="slidersEP-nav--item-thumb">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/slider02.png" alt="">
            </div>
          </div><!-- ./slidersEP--item -->
          <div class="slidersEP-nav--item">
            <div class="slidersEP-nav--item-thumb">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/slider01.png" alt="">
            </div>
          </div><!-- ./slidersEP--item -->
          <div class="slidersEP-nav--item">
            <div class="slidersEP-nav--item-thumb">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/slider02.png" alt="">
            </div>
          </div><!-- ./slidersEP--item -->
        </div><!-- ./slidersEP -->
      </div>
    </div><!-- ./slidersEP-navWap -->
  </div><!-- ./slidersEP--wrapper -->
  <div class="p-end type2">
    <div class="p-exhibition house">
      <div class="container">
        <div class="p-modelHouse">
          <div class="p-exhibition--overview">
            <h3 class="title-lv3">モデルハウス概要</h3>
            <div class="col2-table">
              <div class="col2-table-item">
                <table class="table">
                  <tr>
                    <th>ハウスメーカー</th>
                    <td>三井ホーム</td>
                  </tr>
                  <tr>
                    <th>電話番号</th>
                    <td>03-3247-3641</td>
                  </tr>
                  <tr>
                    <th>工法</th>
                    <td>木造枠組壁工法(プレミアム・モノコック構法)</td>
                  </tr>
                </table>
              </div>
              <div class="col2-table-item">
                <p class="desc2">どこにいても家族を感じられる、自分らしく、心からリラックスできる、自然がもたらす心地よさを感じられる、そんな暮らしを実現しました。</p>
                <div class="col2-table-item--tags">
                  <span class="tag5">#システムキッチンがある</span>
                  <span class="tag5">#安心のセキュリティ</span>
                  <span class="tag5">#二重窓 #防音・断熱・通気性</span>
                  <span class="tag5">#IoT対応</span>
                  <span class="tag5">#アイランドキッチン</span>
                  <span class="tag5">#木のぬくもり</span>
                </div>
              </div>
            </div>
          </div><!-- ./p-exhibition--overview -->
          <div class="p-modelHouse--category">
            <div class="list-categoryWrap">
              <p class="ttl-cat">内観ギャラリー</p>
              <ul class="list-category">
                <li>
                  <a class="link" href="">
                    <div class="list-category-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/category01.png" alt="">
                    </div>
                    <p class="desc2">解放感あふれるウッディーコンテンポラリーデザインのリビング</p>
                  </a>
                </li>
                <li>
                  <a class="link" href="">
                    <div class="list-category-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/category02.png" alt="">
                    </div>
                    <p class="desc2">半屋外空間のラナイ</p>
                  </a>
                </li>
                <li>
                  <a class="link" href="">
                    <div class="list-category-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/category03.png" alt="">
                    </div>
                    <p class="desc2">キッチン脇の落ち着きのあるヌック</p>
                  </a>
                </li>
              </ul>
              <div class="list-category-btn">
                <a href="" class="btn-yellow"><span>見学予約に追加する</span></a>
              </div>
            </div>
            <p class="view-moreWrap align-center"><a href="" class="link2">展示場を見る</a></p>

            <div class="inforWrap">
              <p class="ttl-notice2">モデルハウスINFORMATION​</p>
              <div class="inforWrap-inner">
                <p class="desc2">ダミーテキストです。内容とは関係ありませんのでお読みにならないでください。ダミーテキストです。ダミーテキストです。内容とは関係ありませんの,,,</p>
                <div class="inforWrap-inner--banner">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/banner-infor.png" alt="">
                </div>
                <div class="inforWrap-inner--table">
                  <div class="inforWrap-inner--table-row">
                    <p class="inforWrap-inner--table-label">情報の有効期限</p>
                    <p class="inforWrap-inner--table-desc">2021.10.27</p>
                  </div>
                  <div class="inforWrap-inner--table-row">
                    <p class="inforWrap-inner--table-label">お問い合わせ先</p>
                    <p class="inforWrap-inner--table-desc">三井ホームLANGLEY　<br class="sp-only2">TEL：042-521-7802</p>
                  </div>
                  <div class="inforWrap-inner--table-row">
                    <p class="inforWrap-inner--table-label">URL</p>
                    <p class="inforWrap-inner--table-desc"><a class="link" target="_blank" href="https://www.mitsuihome.co.jp/home/product/langley/">https://www.mitsuihome.co.jp/home/product/langley/</a></p>
                  </div>
                </div>
              </div>
              <div class="inforWrap-inner">
                <p class="desc2">ダミーテキストです。内容とは関係ありませんのでお読みにならないでください。ダミーテキストです。ダミーテキストです。内容とは関係ありませんの,,,</p>
                <div class="inforWrap-inner--banner">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/exhibition/banner-infor.png" alt="">
                </div>
                <div class="inforWrap-inner--table">
                  <div class="inforWrap-inner--table-row">
                    <p class="inforWrap-inner--table-label">情報の有効期限</p>
                    <p class="inforWrap-inner--table-desc">2021.10.27</p>
                  </div>
                  <div class="inforWrap-inner--table-row">
                    <p class="inforWrap-inner--table-label">お問い合わせ先</p>
                    <p class="inforWrap-inner--table-desc">三井ホームLANGLEY　<br class="sp-only2">TEL：042-521-7802</p>
                  </div>
                  <div class="inforWrap-inner--table-row">
                    <p class="inforWrap-inner--table-label">URL</p>
                    <p class="inforWrap-inner--table-desc"><a class="link" target="_blank" href="https://www.mitsuihome.co.jp/home/product/langley/">https://www.mitsuihome.co.jp/home/product/langley/</a></p>
                  </div>
                </div>
              </div>
            </div>

          </div>

          <div class="p-exhibition--cnt">
          <h3 class="title-lv3">こちらのモデルハウスもおすすめ</h3>
          <ul class="list-result">
            <li>
              <a class="link" href="/exhibition/detail">
                <div class="list-result-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/booking/booking01.png" alt="">
                </div>
                <div class="list-result-cnt">
                  <p class="cat5">ダイワハウス</p>
                  <h4 class="list-result-cnt-ttl">xevoΣ PREMIUM</h4>
                  <div class="list-result-cnt-table">
                    <table>
                      <tr>
                        <th>電話番号</th>
                        <td>03-3247-3641</td>
                      </tr>
                      <tr>
                        <th>展示場</th>
                        <td>tvkハウジングプラザ新百合ヶ丘</td>
                      </tr>
                    </table>
                  </div>
                </div>
              </a>
            </li>
            <li>
              <a class="link" href="/exhibition/detail">
                <div class="list-result-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/booking/booking01.png" alt="">
                </div>
                <div class="list-result-cnt">
                  <p class="cat5">ダイワハウス</p>
                  <h4 class="list-result-cnt-ttl">xevoΣ PREMIUM</h4>
                  <div class="list-result-cnt-table">
                    <table>
                      <tr>
                        <th>電話番号</th>
                        <td>03-3247-3641</td>
                      </tr>
                      <tr>
                        <th>展示場</th>
                        <td>tvkハウジングプラザ新百合ヶ丘</td>
                      </tr>
                    </table>
                  </div>
                </div>
              </a>
            </li>
            <li>
              <a class="link" href="/exhibition/detail">
                <div class="list-result-thumb">
                  <img src="<?php echo $PATH;?>/assets/images/booking/booking01.png" alt="">
                </div>
                <div class="list-result-cnt">
                  <p class="cat5">ダイワハウス</p>
                  <h4 class="list-result-cnt-ttl">xevoΣ PREMIUM</h4>
                  <div class="list-result-cnt-table">
                    <table>
                      <tr>
                        <th>電話番号</th>
                        <td>03-3247-3641</td>
                      </tr>
                      <tr>
                        <th>展示場</th>
                        <td>tvkハウジングプラザ新百合ヶ丘</td>
                      </tr>
                    </table>
                  </div>
                </div>
              </a>
            </li>
          </ul><!-- ./list-result -->
          <p class="view-moreWrap align-center"><a href="" class="view-more">他のモデルハウスを見る</a></p>
        </div>
        </div>
      </div>
    </div><!-- ./p-exhibition -->
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>